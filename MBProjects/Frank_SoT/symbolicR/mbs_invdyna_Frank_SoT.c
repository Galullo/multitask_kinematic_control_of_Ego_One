//
//-------------------------------------------------------------
//
//	ROBOTRAN - Version 6.6 (build : february 22, 2008)
//
//	Copyright 
//	Universite catholique de Louvain 
//	Departement de Mecanique 
//	Unite de Production Mecanique et Machines 
//	2, Place du Levant 
//	1348 Louvain-la-Neuve 
//	http://www.robotran.be// 
//
//	==> Generation Date : Mon Oct 16 10:08:06 2017
//
//	==> Project name : Frank_SoT
//	==> using XML input file 
//
//	==> Number of joints : 20
//
//	==> Function : F 2 : Inverse Dynamics : RNEA
//	==> Flops complexity : 1903
//
//	==> Generation Time :  0.020 seconds
//	==> Post-Processing :  0.020 seconds
//
//-------------------------------------------------------------
//
 
#include <math.h> 

#include "mbs_data.h"
#include "mbs_project_interface.h"
 
void mbs_invdyna(double *Qq,
MbsData *s, double tsim)

// double Qq[20];
{ 
 
#include "mbs_invdyna_Frank_SoT.h" 
#define q s->q 
#define qd s->qd 
#define qdd s->qdd 
 
 

// === begin imp_aux === 

// === end imp_aux === 

// ===== BEGIN task 0 ===== 

// = = Block_0_0_0_0_0_1 = = 
 
// Trigonometric Variables  

  C4 = cos(q[4]);
  S4 = sin(q[4]);
  C5 = cos(q[5]);
  S5 = sin(q[5]);
  C6 = cos(q[6]);
  S6 = sin(q[6]);

// = = Block_0_0_0_0_0_2 = = 
 
// Trigonometric Variables  

  C7 = cos(q[7]);
  S7 = sin(q[7]);

// = = Block_0_0_0_0_0_3 = = 
 
// Trigonometric Variables  

  C8 = cos(q[8]);
  S8 = sin(q[8]);

// = = Block_0_0_0_0_0_4 = = 
 
// Trigonometric Variables  

  C9 = cos(q[9]);
  S9 = sin(q[9]);
  C10 = cos(q[10]);
  S10 = sin(q[10]);
  C11 = cos(q[11]);
  S11 = sin(q[11]);
  C12 = cos(q[12]);
  S12 = sin(q[12]);
  C13 = cos(q[13]);
  S13 = sin(q[13]);

// = = Block_0_0_0_0_0_5 = = 
 
// Trigonometric Variables  

  C14 = cos(q[14]);
  S14 = sin(q[14]);
  C15 = cos(q[15]);
  S15 = sin(q[15]);

// = = Block_0_0_0_0_0_6 = = 
 
// Trigonometric Variables  

  C16 = cos(q[16]);
  S16 = sin(q[16]);
  C17 = cos(q[17]);
  S17 = sin(q[17]);
  C18 = cos(q[18]);
  S18 = sin(q[18]);
  C19 = cos(q[19]);
  S19 = sin(q[19]);
  C20 = cos(q[20]);
  S20 = sin(q[20]);

// = = Block_0_1_0_0_0_0 = = 
 
// Forward Kinematics 

  ALPHA33 = qdd[3]-s->g[3];
  ALPHA14 = qdd[1]*C4+qdd[2]*S4;
  ALPHA24 = -(qdd[1]*S4-qdd[2]*C4);
  OM35 = qd[4]*C5;
  OMp35 = -(qd[4]*qd[5]*S5-qdd[4]*C5);
  ALPHA15 = ALPHA14*C5-ALPHA33*S5;
  ALPHA35 = ALPHA14*S5+ALPHA33*C5;
  OM16 = qd[6]-qd[4]*S5;
  OM26 = qd[5]*C6+OM35*S6;
  OM36 = -(qd[5]*S6-OM35*C6);
  OMp16 = qdd[6]-qd[4]*qd[5]*C5-qdd[4]*S5;
  OMp26 = C6*(qdd[5]+qd[6]*OM35)+S6*(OMp35-qd[5]*qd[6]);
  OMp36 = C6*(OMp35-qd[5]*qd[6])-S6*(qdd[5]+qd[6]*OM35);
  BS26 = OM16*OM26;
  BS36 = OM16*OM36;
  BS56 = -(OM16*OM16+OM36*OM36);
  BS66 = OM26*OM36;
  BS96 = -(OM16*OM16+OM26*OM26);
  BETA26 = BS26-OMp36;
  BETA36 = BS36+OMp26;
  BETA66 = BS66-OMp16;
  BETA86 = BS66+OMp16;
  ALPHA26 = ALPHA24*C6+ALPHA35*S6;
  ALPHA36 = -(ALPHA24*S6-ALPHA35*C6);
  OM17 = OM16*C7-OM36*S7;
  OM27 = qd[7]+OM26;
  OM37 = OM16*S7+OM36*C7;
  OM18 = OM16*C8-OM36*S8;
  OM28 = qd[8]+OM26;
  OM38 = OM16*S8+OM36*C8;
  OM19 = OM16*C9-OM36*S9;
  OM29 = qd[9]+OM26;
  OM39 = OM16*S9+OM36*C9;
  OMp19 = C9*(OMp16-qd[9]*OM36)-S9*(OMp36+qd[9]*OM16);
  OMp29 = qdd[9]+OMp26;
  OMp39 = C9*(OMp36+qd[9]*OM16)+S9*(OMp16-qd[9]*OM36);
  BS29 = OM19*OM29;
  ALPHA19 = C9*(ALPHA15+BETA26*s->dpt[2][4]+BETA36*s->dpt[3][4])-S9*(ALPHA36+BETA86*s->dpt[2][4]+BS96*s->dpt[3][4]);
  ALPHA29 = ALPHA26+BETA66*s->dpt[3][4]+BS56*s->dpt[2][4];
  ALPHA39 = C9*(ALPHA36+BETA86*s->dpt[2][4]+BS96*s->dpt[3][4])+S9*(ALPHA15+BETA26*s->dpt[2][4]+BETA36*s->dpt[3][4]);
  OM110 = qd[10]+OM19;
  OM210 = OM29*C10+OM39*S10;
  OM310 = -(OM29*S10-OM39*C10);
  OMp110 = qdd[10]+OMp19;
  OMp210 = C10*(OMp29+qd[10]*OM39)+S10*(OMp39-qd[10]*OM29);
  OMp310 = C10*(OMp39-qd[10]*OM29)-S10*(OMp29+qd[10]*OM39);
  BS310 = OM110*OM310;
  BS910 = -(OM110*OM110+OM210*OM210);
  BETA310 = BS310+OMp210;
  BETA610 = OM210*OM310-OMp110;
  ALPHA210 = ALPHA29*C10+ALPHA39*S10;
  ALPHA310 = -(ALPHA29*S10-ALPHA39*C10);
  OM111 = OM110*C11+OM210*S11;
  OM211 = -(OM110*S11-OM210*C11);
  OM311 = qd[11]+OM310;
  OMp111 = C11*(OMp110+qd[11]*OM210)+S11*(OMp210-qd[11]*OM110);
  OMp211 = C11*(OMp210-qd[11]*OM110)-S11*(OMp110+qd[11]*OM210);
  OMp311 = qdd[11]+OMp310;
  ALPHA111 = C11*(ALPHA19+BETA310*s->dpt[3][11])+S11*(ALPHA210+BETA610*s->dpt[3][11]);
  ALPHA211 = C11*(ALPHA210+BETA610*s->dpt[3][11])-S11*(ALPHA19+BETA310*s->dpt[3][11]);
  ALPHA311 = ALPHA310+BS910*s->dpt[3][11];
  OM112 = OM111*C12-OM311*S12;
  OM212 = qd[12]+OM211;
  OM312 = OM111*S12+OM311*C12;
  OMp112 = C12*(OMp111-qd[12]*OM311)-S12*(OMp311+qd[12]*OM111);
  OMp212 = qdd[12]+OMp211;
  OMp312 = C12*(OMp311+qd[12]*OM111)+S12*(OMp111-qd[12]*OM311);
  BS912 = -(OM112*OM112+OM212*OM212);
  BETA312 = OMp212+OM112*OM312;
  BETA612 = OM212*OM312-OMp112;
  ALPHA112 = ALPHA111*C12-ALPHA311*S12;
  ALPHA312 = ALPHA111*S12+ALPHA311*C12;
  OM113 = OM112*C13+OM212*S13;
  OM213 = -(OM112*S13-OM212*C13);
  OM313 = qd[13]+OM312;
  OMp113 = C13*(OMp112+qd[13]*OM212)+S13*(OMp212-qd[13]*OM112);
  OMp213 = C13*(OMp212-qd[13]*OM112)-S13*(OMp112+qd[13]*OM212);
  OMp313 = qdd[13]+OMp312;
  BS213 = OM113*OM213;
  BS313 = OM113*OM313;
  BS613 = OM213*OM313;
  OM114 = OM16*C14+OM26*S14;
  OM214 = -(OM16*S14-OM26*C14);
  OM314 = qd[14]+OM36;
  OMp114 = C14*(OMp16+qd[14]*OM26)+S14*(OMp26-qd[14]*OM16);
  OMp214 = C14*(OMp26-qd[14]*OM16)-S14*(OMp16+qd[14]*OM26);
  OMp314 = qdd[14]+OMp36;
  ALPHA114 = C14*(ALPHA15+BETA36*s->dpt[3][5])+S14*(ALPHA26+BETA66*s->dpt[3][5]);
  ALPHA214 = C14*(ALPHA26+BETA66*s->dpt[3][5])-S14*(ALPHA15+BETA36*s->dpt[3][5]);
  ALPHA314 = ALPHA36+BS96*s->dpt[3][5];
  OM115 = OM114*C15-OM314*S15;
  OM215 = qd[15]+OM214;
  OM315 = OM114*S15+OM314*C15;
  OM116 = OM16*C16-OM36*S16;
  OM216 = qd[16]+OM26;
  OM316 = OM16*S16+OM36*C16;
  OMp116 = C16*(OMp16-qd[16]*OM36)-S16*(OMp36+qd[16]*OM16);
  OMp216 = qdd[16]+OMp26;
  OMp316 = C16*(OMp36+qd[16]*OM16)+S16*(OMp16-qd[16]*OM36);
  BS216 = OM116*OM216;
  ALPHA116 = C16*(ALPHA15+BETA26*s->dpt[2][6]+BETA36*s->dpt[3][6])-S16*(ALPHA36+BETA86*s->dpt[2][6]+BS96*s->dpt[3][6]);
  ALPHA216 = ALPHA26+BETA66*s->dpt[3][6]+BS56*s->dpt[2][6];
  ALPHA316 = C16*(ALPHA36+BETA86*s->dpt[2][6]+BS96*s->dpt[3][6])+S16*(ALPHA15+BETA26*s->dpt[2][6]+BETA36*s->dpt[3][6]);
  OM117 = qd[17]+OM116;
  OM217 = OM216*C17+OM316*S17;
  OM317 = -(OM216*S17-OM316*C17);
  OMp117 = qdd[17]+OMp116;
  OMp217 = C17*(OMp216+qd[17]*OM316)+S17*(OMp316-qd[17]*OM216);
  OMp317 = C17*(OMp316-qd[17]*OM216)-S17*(OMp216+qd[17]*OM316);
  BS317 = OM117*OM317;
  BS917 = -(OM117*OM117+OM217*OM217);
  BETA317 = BS317+OMp217;
  BETA617 = OM217*OM317-OMp117;
  ALPHA217 = ALPHA216*C17+ALPHA316*S17;
  ALPHA317 = -(ALPHA216*S17-ALPHA316*C17);
  OM118 = OM117*C18+OM217*S18;
  OM218 = -(OM117*S18-OM217*C18);
  OM318 = qd[18]+OM317;
  OMp118 = C18*(OMp117+qd[18]*OM217)+S18*(OMp217-qd[18]*OM117);
  OMp218 = C18*(OMp217-qd[18]*OM117)-S18*(OMp117+qd[18]*OM217);
  OMp318 = qdd[18]+OMp317;
  ALPHA118 = C18*(ALPHA116+BETA317*s->dpt[3][19])+S18*(ALPHA217+BETA617*s->dpt[3][19]);
  ALPHA218 = C18*(ALPHA217+BETA617*s->dpt[3][19])-S18*(ALPHA116+BETA317*s->dpt[3][19]);
  ALPHA318 = ALPHA317+BS917*s->dpt[3][19];
  OM119 = OM118*C19-OM318*S19;
  OM219 = qd[19]+OM218;
  OM319 = OM118*S19+OM318*C19;
  OMp119 = C19*(OMp118-qd[19]*OM318)-S19*(OMp318+qd[19]*OM118);
  OMp219 = qdd[19]+OMp218;
  OMp319 = C19*(OMp318+qd[19]*OM118)+S19*(OMp118-qd[19]*OM318);
  BS919 = -(OM119*OM119+OM219*OM219);
  BETA319 = OMp219+OM119*OM319;
  BETA619 = OM219*OM319-OMp119;
  ALPHA119 = ALPHA118*C19-ALPHA318*S19;
  ALPHA319 = ALPHA118*S19+ALPHA318*C19;
  OM120 = OM119*C20+OM219*S20;
  OM220 = -(OM119*S20-OM219*C20);
  OM320 = qd[20]+OM319;
  OMp120 = C20*(OMp119+qd[20]*OM219)+S20*(OMp219-qd[20]*OM119);
  OMp220 = C20*(OMp219-qd[20]*OM119)-S20*(OMp119+qd[20]*OM219);
  OMp320 = qdd[20]+OMp319;
  BS220 = OM120*OM220;
  BS320 = OM120*OM320;
  BS620 = OM220*OM320;
 
// Backward Dynamics 

  Fs120 = -(s->frc[1][20]+s->m[20]*(s->l[1][20]*(OM220*OM220+OM320*OM320)-s->l[2][20]*(BS220-OMp320)-s->l[3][20]*(BS320+
 OMp220)-C20*(ALPHA119+BETA319*s->dpt[3][22])-S20*(ALPHA218+BETA619*s->dpt[3][22])));
  Fs220 = -(s->frc[2][20]-s->m[20]*(s->l[1][20]*(BS220+OMp320)-s->l[2][20]*(OM120*OM120+OM320*OM320)+s->l[3][20]*(BS620-
 OMp120)+C20*(ALPHA218+BETA619*s->dpt[3][22])-S20*(ALPHA119+BETA319*s->dpt[3][22])));
  Fs320 = -(s->frc[3][20]-s->m[20]*(ALPHA319+BS919*s->dpt[3][22]+s->l[1][20]*(BS320-OMp220)+s->l[2][20]*(BS620+OMp120)-
 s->l[3][20]*(OM120*OM120+OM220*OM220)));
  Cq120 = -(s->trq[1][20]-s->In[1][20]*OMp120-s->In[2][20]*OMp220-s->In[3][20]*OMp320+Fs220*s->l[3][20]-Fs320*
 s->l[2][20]-OM220*(s->In[3][20]*OM120+s->In[6][20]*OM220+s->In[9][20]*OM320)+OM320*(s->In[2][20]*OM120+s->In[5][20]*OM220+
 s->In[6][20]*OM320));
  Cq220 = -(s->trq[2][20]-s->In[2][20]*OMp120-s->In[5][20]*OMp220-s->In[6][20]*OMp320-Fs120*s->l[3][20]+Fs320*
 s->l[1][20]+OM120*(s->In[3][20]*OM120+s->In[6][20]*OM220+s->In[9][20]*OM320)-OM320*(s->In[1][20]*OM120+s->In[2][20]*OM220+
 s->In[3][20]*OM320));
  Cq320 = -(s->trq[3][20]-s->In[3][20]*OMp120-s->In[6][20]*OMp220-s->In[9][20]*OMp320+Fs120*s->l[2][20]-Fs220*
 s->l[1][20]-OM120*(s->In[2][20]*OM120+s->In[5][20]*OM220+s->In[6][20]*OM320)+OM220*(s->In[1][20]*OM120+s->In[2][20]*OM220+
 s->In[3][20]*OM320));
  Fs119 = -(s->frc[1][19]-s->m[19]*(ALPHA119+BETA319*s->l[3][19]));
  Fs219 = -(s->frc[2][19]-s->m[19]*(ALPHA218+BETA619*s->l[3][19]));
  Fq119 = Fs119+Fs120*C20-Fs220*S20;
  Fq319 = -(s->frc[3][19]-Fs320-s->m[19]*(ALPHA319+BS919*s->l[3][19]));
  Cq119 = -(s->trq[1][19]-s->In[1][19]*OMp119-Cq120*C20+Cq220*S20+Fs219*s->l[3][19]+OM219*OM319*(s->In[5][19]-
 s->In[9][19])+s->dpt[3][22]*(Fs120*S20+Fs220*C20));
  Cq219 = -(s->trq[2][19]-s->In[5][19]*OMp219-Cq120*S20-Cq220*C20-Fs119*s->l[3][19]-OM119*OM319*(s->In[1][19]-
 s->In[9][19])-s->dpt[3][22]*(Fs120*C20-Fs220*S20));
  Cq319 = -(s->trq[3][19]-Cq320-s->In[9][19]*OMp319+OM119*OM219*(s->In[1][19]-s->In[5][19]));
  Fs118 = -(s->frc[1][18]-s->m[18]*(ALPHA118-s->l[2][18]*(OMp318-OM118*OM218)));
  Fs318 = -(s->frc[3][18]-s->m[18]*(ALPHA318+s->l[2][18]*(OMp118+OM218*OM318)));
  Fq118 = Fs118+Fq119*C19+Fq319*S19;
  Fq218 = -(s->frc[2][18]-Fs219-s->m[18]*(ALPHA218-s->l[2][18]*(OM118*OM118+OM318*OM318))-Fs120*S20-Fs220*C20);
  Cq118 = -(s->trq[1][18]-s->In[1][18]*OMp118-Cq119*C19-Cq319*S19-Fs318*s->l[2][18]+OM218*OM318*(s->In[5][18]-
 s->In[9][18]));
  Cq218 = -(s->trq[2][18]-Cq219-s->In[5][18]*OMp218-OM118*OM318*(s->In[1][18]-s->In[9][18]));
  Cq318 = -(s->trq[3][18]-s->In[9][18]*OMp318+Cq119*S19-Cq319*C19+Fs118*s->l[2][18]+OM118*OM218*(s->In[1][18]-
 s->In[5][18]));
  Fs117 = -(s->frc[1][17]-s->m[17]*(ALPHA116+BETA317*s->l[3][17]-s->l[1][17]*(OM217*OM217+OM317*OM317)));
  Fs217 = -(s->frc[2][17]-s->m[17]*(ALPHA217+BETA617*s->l[3][17]+s->l[1][17]*(OMp317+OM117*OM217)));
  Fs317 = -(s->frc[3][17]-s->m[17]*(ALPHA317+BS917*s->l[3][17]+s->l[1][17]*(BS317-OMp217)));
  Fq217 = Fs217+Fq118*S18+Fq218*C18;
  Fq317 = Fs317+Fs318-Fq119*S19+Fq319*C19;
  Cq117 = -(s->trq[1][17]-s->In[1][17]*OMp117-Cq118*C18+Cq218*S18+Fs217*s->l[3][17]+OM217*OM317*(s->In[5][17]-
 s->In[9][17])+s->dpt[3][19]*(Fq118*S18+Fq218*C18));
  Cq217 = -(s->trq[2][17]-s->In[5][17]*OMp217-Cq118*S18-Cq218*C18-Fs117*s->l[3][17]+Fs317*s->l[1][17]-OM117*OM317*(
 s->In[1][17]-s->In[9][17])-s->dpt[3][19]*(Fq118*C18-Fq218*S18));
  Cq317 = -(s->trq[3][17]-Cq318-s->In[9][17]*OMp317-Fs217*s->l[1][17]+OM117*OM217*(s->In[1][17]-s->In[5][17]));
  Fs116 = -(s->frc[1][16]-s->m[16]*(ALPHA116-s->l[1][16]*(OM216*OM216+OM316*OM316)+s->l[2][16]*(BS216-OMp316)));
  Fs216 = -(s->frc[2][16]-s->m[16]*(ALPHA216+s->l[1][16]*(BS216+OMp316)-s->l[2][16]*(OM116*OM116+OM316*OM316)));
  Fs316 = -(s->frc[3][16]-s->m[16]*(ALPHA316-s->l[1][16]*(OMp216-OM116*OM316)+s->l[2][16]*(OMp116+OM216*OM316)));
  Fq116 = Fs116+Fs117+Fq118*C18-Fq218*S18;
  Fq216 = Fs216+Fq217*C17-Fq317*S17;
  Fq316 = Fs316+Fq217*S17+Fq317*C17;
  Cq116 = -(s->trq[1][16]-Cq117-s->In[1][16]*OMp116-Fs316*s->l[2][16]+OM216*OM316*(s->In[5][16]-s->In[9][16]));
  Cq216 = -(s->trq[2][16]-s->In[5][16]*OMp216-Cq217*C17+Cq317*S17+Fs316*s->l[1][16]-OM116*OM316*(s->In[1][16]-
 s->In[9][16]));
  Cq316 = -(s->trq[3][16]-s->In[9][16]*OMp316-Cq217*S17-Cq317*C17+Fs116*s->l[2][16]-Fs216*s->l[1][16]+OM116*OM216*(
 s->In[1][16]-s->In[5][16]));
  Fs115 = -(s->frc[1][15]-s->m[15]*(ALPHA114*C15-ALPHA314*S15));
  Fs315 = -(s->frc[3][15]-s->m[15]*(ALPHA114*S15+ALPHA314*C15));
  Cq115 = -(s->trq[1][15]-s->In[1][15]*(C15*(OMp114-qd[15]*OM314)-S15*(OMp314+qd[15]*OM114))+OM215*OM315*(s->In[5][15]-
 s->In[9][15]));
  Cq215 = -(s->trq[2][15]-s->In[5][15]*(qdd[15]+OMp214)-OM115*OM315*(s->In[1][15]-s->In[9][15]));
  Cq315 = -(s->trq[3][15]-s->In[9][15]*(C15*(OMp314+qd[15]*OM114)+S15*(OMp114-qd[15]*OM314))+OM115*OM215*(s->In[1][15]-
 s->In[5][15]));
  Fq114 = -(s->frc[1][14]-s->m[14]*ALPHA114-Fs115*C15-Fs315*S15);
  Fq214 = -(s->frc[2][14]+s->frc[2][15]-s->m[14]*ALPHA214-s->m[15]*ALPHA214);
  Cq114 = -(s->trq[1][14]-s->In[1][14]*OMp114-Cq115*C15-Cq315*S15+OM214*OM314*(s->In[5][14]-s->In[9][14]));
  Cq214 = -(s->trq[2][14]-Cq215-s->In[5][14]*OMp214-OM114*OM314*(s->In[1][14]-s->In[9][14]));
  Cq314 = -(s->trq[3][14]-s->In[9][14]*OMp314+Cq115*S15-Cq315*C15+OM114*OM214*(s->In[1][14]-s->In[5][14]));
  Fs113 = -(s->frc[1][13]+s->m[13]*(s->l[1][13]*(OM213*OM213+OM313*OM313)-s->l[2][13]*(BS213-OMp313)-s->l[3][13]*(BS313+
 OMp213)-C13*(ALPHA112+BETA312*s->dpt[3][14])-S13*(ALPHA211+BETA612*s->dpt[3][14])));
  Fs213 = -(s->frc[2][13]-s->m[13]*(s->l[1][13]*(BS213+OMp313)-s->l[2][13]*(OM113*OM113+OM313*OM313)+s->l[3][13]*(BS613-
 OMp113)+C13*(ALPHA211+BETA612*s->dpt[3][14])-S13*(ALPHA112+BETA312*s->dpt[3][14])));
  Fs313 = -(s->frc[3][13]-s->m[13]*(ALPHA312+BS912*s->dpt[3][14]+s->l[1][13]*(BS313-OMp213)+s->l[2][13]*(BS613+OMp113)-
 s->l[3][13]*(OM113*OM113+OM213*OM213)));
  Cq113 = -(s->trq[1][13]-s->In[1][13]*OMp113-s->In[2][13]*OMp213-s->In[3][13]*OMp313+Fs213*s->l[3][13]-Fs313*
 s->l[2][13]-OM213*(s->In[3][13]*OM113+s->In[6][13]*OM213+s->In[9][13]*OM313)+OM313*(s->In[2][13]*OM113+s->In[5][13]*OM213+
 s->In[6][13]*OM313));
  Cq213 = -(s->trq[2][13]-s->In[2][13]*OMp113-s->In[5][13]*OMp213-s->In[6][13]*OMp313-Fs113*s->l[3][13]+Fs313*
 s->l[1][13]+OM113*(s->In[3][13]*OM113+s->In[6][13]*OM213+s->In[9][13]*OM313)-OM313*(s->In[1][13]*OM113+s->In[2][13]*OM213+
 s->In[3][13]*OM313));
  Cq313 = -(s->trq[3][13]-s->In[3][13]*OMp113-s->In[6][13]*OMp213-s->In[9][13]*OMp313+Fs113*s->l[2][13]-Fs213*
 s->l[1][13]-OM113*(s->In[2][13]*OM113+s->In[5][13]*OM213+s->In[6][13]*OM313)+OM213*(s->In[1][13]*OM113+s->In[2][13]*OM213+
 s->In[3][13]*OM313));
  Fs112 = -(s->frc[1][12]-s->m[12]*(ALPHA112+BETA312*s->l[3][12]));
  Fs212 = -(s->frc[2][12]-s->m[12]*(ALPHA211+BETA612*s->l[3][12]));
  Fq112 = Fs112+Fs113*C13-Fs213*S13;
  Fq312 = -(s->frc[3][12]-Fs313-s->m[12]*(ALPHA312+BS912*s->l[3][12]));
  Cq112 = -(s->trq[1][12]-s->In[1][12]*OMp112-Cq113*C13+Cq213*S13+Fs212*s->l[3][12]+OM212*OM312*(s->In[5][12]-
 s->In[9][12])+s->dpt[3][14]*(Fs113*S13+Fs213*C13));
  Cq212 = -(s->trq[2][12]-s->In[5][12]*OMp212-Cq113*S13-Cq213*C13-Fs112*s->l[3][12]-OM112*OM312*(s->In[1][12]-
 s->In[9][12])-s->dpt[3][14]*(Fs113*C13-Fs213*S13));
  Cq312 = -(s->trq[3][12]-Cq313-s->In[9][12]*OMp312+OM112*OM212*(s->In[1][12]-s->In[5][12]));
  Fs111 = -(s->frc[1][11]-s->m[11]*(ALPHA111-s->l[2][11]*(OMp311-OM111*OM211)));
  Fs311 = -(s->frc[3][11]-s->m[11]*(ALPHA311+s->l[2][11]*(OMp111+OM211*OM311)));
  Fq111 = Fs111+Fq112*C12+Fq312*S12;
  Fq211 = -(s->frc[2][11]-Fs212-s->m[11]*(ALPHA211-s->l[2][11]*(OM111*OM111+OM311*OM311))-Fs113*S13-Fs213*C13);
  Cq111 = -(s->trq[1][11]-s->In[1][11]*OMp111-Cq112*C12-Cq312*S12-Fs311*s->l[2][11]+OM211*OM311*(s->In[5][11]-
 s->In[9][11]));
  Cq211 = -(s->trq[2][11]-Cq212-s->In[5][11]*OMp211-OM111*OM311*(s->In[1][11]-s->In[9][11]));
  Cq311 = -(s->trq[3][11]-s->In[9][11]*OMp311+Cq112*S12-Cq312*C12+Fs111*s->l[2][11]+OM111*OM211*(s->In[1][11]-
 s->In[5][11]));
  Fs110 = -(s->frc[1][10]-s->m[10]*(ALPHA19+BETA310*s->l[3][10]-s->l[1][10]*(OM210*OM210+OM310*OM310)));
  Fs210 = -(s->frc[2][10]-s->m[10]*(ALPHA210+BETA610*s->l[3][10]+s->l[1][10]*(OMp310+OM110*OM210)));
  Fs310 = -(s->frc[3][10]-s->m[10]*(ALPHA310+BS910*s->l[3][10]+s->l[1][10]*(BS310-OMp210)));
  Fq210 = Fs210+Fq111*S11+Fq211*C11;
  Fq310 = Fs310+Fs311-Fq112*S12+Fq312*C12;
  Cq110 = -(s->trq[1][10]-s->In[1][10]*OMp110-Cq111*C11+Cq211*S11+Fs210*s->l[3][10]+OM210*OM310*(s->In[5][10]-
 s->In[9][10])+s->dpt[3][11]*(Fq111*S11+Fq211*C11));
  Cq210 = -(s->trq[2][10]-s->In[5][10]*OMp210-Cq111*S11-Cq211*C11-Fs110*s->l[3][10]+Fs310*s->l[1][10]-OM110*OM310*(
 s->In[1][10]-s->In[9][10])-s->dpt[3][11]*(Fq111*C11-Fq211*S11));
  Cq310 = -(s->trq[3][10]-Cq311-s->In[9][10]*OMp310-Fs210*s->l[1][10]+OM110*OM210*(s->In[1][10]-s->In[5][10]));
  Fs19 = -(s->frc[1][9]-s->m[9]*(ALPHA19-s->l[1][9]*(OM29*OM29+OM39*OM39)+s->l[2][9]*(BS29-OMp39)));
  Fs29 = -(s->frc[2][9]-s->m[9]*(ALPHA29+s->l[1][9]*(BS29+OMp39)-s->l[2][9]*(OM19*OM19+OM39*OM39)));
  Fs39 = -(s->frc[3][9]-s->m[9]*(ALPHA39-s->l[1][9]*(OMp29-OM19*OM39)+s->l[2][9]*(OMp19+OM29*OM39)));
  Fq19 = Fs110+Fs19+Fq111*C11-Fq211*S11;
  Fq29 = Fs29+Fq210*C10-Fq310*S10;
  Fq39 = Fs39+Fq210*S10+Fq310*C10;
  Cq19 = -(s->trq[1][9]-Cq110-s->In[1][9]*OMp19-Fs39*s->l[2][9]+OM29*OM39*(s->In[5][9]-s->In[9][9]));
  Cq29 = -(s->trq[2][9]-s->In[5][9]*OMp29-Cq210*C10+Cq310*S10+Fs39*s->l[1][9]-OM19*OM39*(s->In[1][9]-s->In[9][9]));
  Cq39 = -(s->trq[3][9]-s->In[9][9]*OMp39-Cq210*S10-Cq310*C10+Fs19*s->l[2][9]-Fs29*s->l[1][9]+OM19*OM29*(s->In[1][9]-
 s->In[5][9]));
  Fs18 = -(s->frc[1][8]-s->m[8]*(C8*(ALPHA15+BETA26*s->dpt[2][3])-S8*(ALPHA36+BETA86*s->dpt[2][3])));
  Fs38 = -(s->frc[3][8]-s->m[8]*(C8*(ALPHA36+BETA86*s->dpt[2][3])+S8*(ALPHA15+BETA26*s->dpt[2][3])));
  Cq18 = -(s->trq[1][8]-s->In[1][8]*(C8*(OMp16-qd[8]*OM36)-S8*(OMp36+qd[8]*OM16))+OM28*OM38*(s->In[5][8]-s->In[9][8]));
  Cq28 = -(s->trq[2][8]-s->In[5][8]*(qdd[8]+OMp26)-OM18*OM38*(s->In[1][8]-s->In[9][8]));
  Cq38 = -(s->trq[3][8]-s->In[9][8]*(C8*(OMp36+qd[8]*OM16)+S8*(OMp16-qd[8]*OM36))+OM18*OM28*(s->In[1][8]-s->In[5][8]));
  Fs17 = -(s->frc[1][7]-s->m[7]*(C7*(ALPHA15+BETA26*s->dpt[2][2])-S7*(ALPHA36+BETA86*s->dpt[2][2])));
  Fs37 = -(s->frc[3][7]-s->m[7]*(C7*(ALPHA36+BETA86*s->dpt[2][2])+S7*(ALPHA15+BETA26*s->dpt[2][2])));
  Cq17 = -(s->trq[1][7]-s->In[1][7]*(C7*(OMp16-qd[7]*OM36)-S7*(OMp36+qd[7]*OM16))+OM27*OM37*(s->In[5][7]-s->In[9][7]));
  Cq27 = -(s->trq[2][7]-s->In[5][7]*(qdd[7]+OMp26)-OM17*OM37*(s->In[1][7]-s->In[9][7]));
  Cq37 = -(s->trq[3][7]-s->In[9][7]*(C7*(OMp36+qd[7]*OM16)+S7*(OMp16-qd[7]*OM36))+OM17*OM27*(s->In[1][7]-s->In[5][7]));
  Fs16 = -(s->frc[1][6]-s->m[6]*(ALPHA15+BETA26*s->l[2][6]+BETA36*s->l[3][6]-s->l[1][6]*(OM26*OM26+OM36*OM36)));
  Fs26 = -(s->frc[2][6]-s->m[6]*(ALPHA26+BETA66*s->l[3][6]+BS56*s->l[2][6]+s->l[1][6]*(BS26+OMp36)));
  Fs36 = -(s->frc[3][6]-s->m[6]*(ALPHA36+BETA86*s->l[2][6]+BS96*s->l[3][6]+s->l[1][6]*(BS36-OMp26)));
  Fq16 = Fs16+Fq114*C14+Fq116*C16+Fq19*C9-Fq214*S14+Fq316*S16+Fq39*S9+Fs17*C7+Fs18*C8+Fs37*S7+Fs38*S8;
  Fq26 = -(s->frc[2][7]+s->frc[2][8]-Fq216-Fq29-Fs26-s->m[7]*(ALPHA26+BS56*s->dpt[2][2])-s->m[8]*(ALPHA26+BS56*
 s->dpt[2][3])-Fq114*S14-Fq214*C14);
  Fq36 = -(s->frc[3][14]-Fs36-s->m[14]*ALPHA314+Fq116*S16+Fq19*S9-Fq316*C16-Fq39*C9+Fs115*S15+Fs17*S7+Fs18*S8-Fs315*C15-
 Fs37*C7-Fs38*C8);
  Cq16 = -(s->trq[1][6]-s->In[1][6]*OMp16-s->In[2][6]*OMp26-s->In[3][6]*OMp36-Cq114*C14-Cq116*C16-Cq17*C7-Cq18*C8-Cq19*
 C9+Cq214*S14-Cq316*S16-Cq37*S7-Cq38*S8-Cq39*S9+Fq216*s->dpt[3][6]+Fq29*s->dpt[3][4]+Fs26*s->l[3][6]-Fs36*s->l[2][6]-OM26*(
 s->In[3][6]*OM16+s->In[6][6]*OM26+s->In[9][6]*OM36)+OM36*(s->In[2][6]*OM16+s->In[6][6]*OM36)+s->dpt[2][2]*(Fs17*S7-Fs37*C7)+
 s->dpt[2][3]*(Fs18*S8-Fs38*C8)+s->dpt[2][4]*(Fq19*S9-Fq39*C9)+s->dpt[2][6]*(Fq116*S16-Fq316*C16)+s->dpt[3][5]*(Fq114*S14+
 Fq214*C14));
  Cq26 = -(s->trq[2][6]-Cq216-Cq27-Cq28-Cq29-s->In[2][6]*OMp16-s->In[6][6]*OMp36-Cq114*S14-Cq214*C14-Fs16*s->l[3][6]+
 Fs36*s->l[1][6]+OM16*(s->In[3][6]*OM16+s->In[6][6]*OM26+s->In[9][6]*OM36)-OM36*(s->In[1][6]*OM16+s->In[2][6]*OM26+
 s->In[3][6]*OM36)-s->dpt[3][4]*(Fq19*C9+Fq39*S9)-s->dpt[3][5]*(Fq114*C14-Fq214*S14)-s->dpt[3][6]*(Fq116*C16+Fq316*S16));
  Cq36 = -(s->trq[3][6]-Cq314-s->In[3][6]*OMp16-s->In[6][6]*OMp26-s->In[9][6]*OMp36+Cq116*S16+Cq17*S7+Cq18*S8+Cq19*S9-
 Cq316*C16-Cq37*C7-Cq38*C8-Cq39*C9+Fs16*s->l[2][6]-Fs26*s->l[1][6]-OM16*(s->In[2][6]*OM16+s->In[6][6]*OM36)+OM26*(s->In[1][6]
 *OM16+s->In[2][6]*OM26+s->In[3][6]*OM36)+s->dpt[2][2]*(Fs17*C7+Fs37*S7)+s->dpt[2][3]*(Fs18*C8+Fs38*S8)+s->dpt[2][4]*(Fq19*C9
 +Fq39*S9)+s->dpt[2][6]*(Fq116*C16+Fq316*S16));
  Fq25 = Fq26*C6-Fq36*S6;
  Fq35 = Fq26*S6+Fq36*C6;
  Cq25 = Cq26*C6-Cq36*S6;
  Fq14 = Fq16*C5+Fq35*S5;
  Fq34 = -(Fq16*S5-Fq35*C5);
  Cq34 = -(Cq16*S5-C5*(Cq26*S6+Cq36*C6));
  Fq13 = Fq14*C4-Fq25*S4;
  Fq23 = Fq14*S4+Fq25*C4;

// = = Block_0_2_0_0_0_0 = = 
 
// Symbolic Outputs  

  Qq[1] = Fq13;
  Qq[2] = Fq23;
  Qq[3] = Fq34;
  Qq[4] = Cq34;
  Qq[5] = Cq25;
  Qq[6] = Cq16;
  Qq[7] = Cq27;
  Qq[8] = Cq28;
  Qq[9] = Cq29;
  Qq[10] = Cq110;
  Qq[11] = Cq311;
  Qq[12] = Cq212;
  Qq[13] = Cq313;
  Qq[14] = Cq314;
  Qq[15] = Cq215;
  Qq[16] = Cq216;
  Qq[17] = Cq117;
  Qq[18] = Cq318;
  Qq[19] = Cq219;
  Qq[20] = Cq320;

// ====== END Task 0 ====== 


}
 

