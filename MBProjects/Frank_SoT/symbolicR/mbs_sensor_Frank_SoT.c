//
//-------------------------------------------------------------
//
//	ROBOTRAN - Version 6.6 (build : february 22, 2008)
//
//	Copyright 
//	Universite catholique de Louvain 
//	Departement de Mecanique 
//	Unite de Production Mecanique et Machines 
//	2, Place du Levant 
//	1348 Louvain-la-Neuve 
//	http://www.robotran.be// 
//
//	==> Generation Date : Mon Oct 16 10:08:06 2017
//
//	==> Project name : Frank_SoT
//	==> using XML input file 
//
//	==> Number of joints : 20
//
//	==> Function : F 6 : Sensors Kinematical Informations (sens) 
//	==> Flops complexity : 4111
//
//	==> Generation Time :  0.050 seconds
//	==> Post-Processing :  0.090 seconds
//
//-------------------------------------------------------------
//
 
#include <math.h> 

#include "mbs_data.h"
#include "mbs_project_interface.h"
#include "mbs_sensor.h"
 
void  mbs_sensor(MbsSensor *sens, 
              MbsData *s,
              int isens)
{ 
 
#include "mbs_sensor_Frank_SoT.h" 
#define q s->q 
#define qd s->qd 
#define qdd s->qdd 
 
 

// === begin imp_aux === 

// === end imp_aux === 

// ===== BEGIN task 0 ===== 
 
// Sensor Kinematics 



// = = Block_0_0_0_0_0_1 = = 
 
// Trigonometric Variables  

  C4 = cos(q[4]);
  S4 = sin(q[4]);
  C5 = cos(q[5]);
  S5 = sin(q[5]);
  C6 = cos(q[6]);
  S6 = sin(q[6]);

// = = Block_0_0_0_0_0_2 = = 
 
// Trigonometric Variables  

  C7 = cos(q[7]);
  S7 = sin(q[7]);

// = = Block_0_0_0_0_0_3 = = 
 
// Trigonometric Variables  

  C8 = cos(q[8]);
  S8 = sin(q[8]);

// = = Block_0_0_0_0_0_4 = = 
 
// Trigonometric Variables  

  C9 = cos(q[9]);
  S9 = sin(q[9]);
  C10 = cos(q[10]);
  S10 = sin(q[10]);
  C11 = cos(q[11]);
  S11 = sin(q[11]);
  C12 = cos(q[12]);
  S12 = sin(q[12]);
  C13 = cos(q[13]);
  S13 = sin(q[13]);

// = = Block_0_0_0_0_0_5 = = 
 
// Trigonometric Variables  

  C14 = cos(q[14]);
  S14 = sin(q[14]);

// = = Block_0_0_0_0_0_6 = = 
 
// Trigonometric Variables  

  C16 = cos(q[16]);
  S16 = sin(q[16]);
  C17 = cos(q[17]);
  S17 = sin(q[17]);
  C18 = cos(q[18]);
  S18 = sin(q[18]);
  C19 = cos(q[19]);
  S19 = sin(q[19]);
  C20 = cos(q[20]);
  S20 = sin(q[20]);

// ====== END Task 0 ====== 

// ===== BEGIN task 1 ===== 
 
switch(isens)
{
 
// 
break;
case 1:
 


// = = Block_1_0_0_1_0_1 = = 
 
// Sensor Kinematics 


    ROcp0_15 = C4*C5;
    ROcp0_25 = S4*C5;
    ROcp0_75 = C4*S5;
    ROcp0_85 = S4*S5;
    ROcp0_46 = ROcp0_75*S6-S4*C6;
    ROcp0_56 = ROcp0_85*S6+C4*C6;
    ROcp0_66 = C5*S6;
    ROcp0_76 = ROcp0_75*C6+S4*S6;
    ROcp0_86 = ROcp0_85*C6-C4*S6;
    ROcp0_96 = C5*C6;
    OMcp0_15 = -qd[5]*S4;
    OMcp0_25 = qd[5]*C4;
    OMcp0_16 = OMcp0_15+ROcp0_15*qd[6];
    OMcp0_26 = OMcp0_25+ROcp0_25*qd[6];
    OMcp0_36 = qd[4]-qd[6]*S5;
    OPcp0_16 = ROcp0_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp0_25*S5+ROcp0_25*qd[4]);
    OPcp0_26 = ROcp0_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp0_15*S5+ROcp0_15*qd[4]);
    OPcp0_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_1_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = q[1];
    sens->P[2] = q[2];
    sens->P[3] = q[3];
    sens->R[1][1] = ROcp0_15;
    sens->R[1][2] = ROcp0_25;
    sens->R[1][3] = -S5;
    sens->R[2][1] = ROcp0_46;
    sens->R[2][2] = ROcp0_56;
    sens->R[2][3] = ROcp0_66;
    sens->R[3][1] = ROcp0_76;
    sens->R[3][2] = ROcp0_86;
    sens->R[3][3] = ROcp0_96;
    sens->V[1] = qd[1];
    sens->V[2] = qd[2];
    sens->V[3] = qd[3];
    sens->OM[1] = OMcp0_16;
    sens->OM[2] = OMcp0_26;
    sens->OM[3] = OMcp0_36;
    sens->J[1][1] = (1.0);
    sens->J[2][2] = (1.0);
    sens->J[3][3] = (1.0);
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp0_15;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp0_25;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->A[1] = qdd[1];
    sens->A[2] = qdd[2];
    sens->A[3] = qdd[3];
    sens->OMP[1] = OPcp0_16;
    sens->OMP[2] = OPcp0_26;
    sens->OMP[3] = OPcp0_36;
 
// 
break;
case 2:
 


// = = Block_1_0_0_2_0_1 = = 
 
// Sensor Kinematics 


    ROcp1_15 = C4*C5;
    ROcp1_25 = S4*C5;
    ROcp1_75 = C4*S5;
    ROcp1_85 = S4*S5;
    ROcp1_46 = ROcp1_75*S6-S4*C6;
    ROcp1_56 = ROcp1_85*S6+C4*C6;
    ROcp1_66 = C5*S6;
    ROcp1_76 = ROcp1_75*C6+S4*S6;
    ROcp1_86 = ROcp1_85*C6-C4*S6;
    ROcp1_96 = C5*C6;
    OMcp1_15 = -qd[5]*S4;
    OMcp1_25 = qd[5]*C4;
    OMcp1_16 = OMcp1_15+ROcp1_15*qd[6];
    OMcp1_26 = OMcp1_25+ROcp1_25*qd[6];
    OMcp1_36 = qd[4]-qd[6]*S5;
    OPcp1_16 = ROcp1_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp1_25*S5+ROcp1_25*qd[4]);
    OPcp1_26 = ROcp1_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp1_15*S5+ROcp1_15*qd[4]);
    OPcp1_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_2_0_2 = = 
 
// Sensor Kinematics 


    ROcp1_17 = ROcp1_15*C7-ROcp1_76*S7;
    ROcp1_27 = ROcp1_25*C7-ROcp1_86*S7;
    ROcp1_37 = -(ROcp1_96*S7+S5*C7);
    ROcp1_77 = ROcp1_15*S7+ROcp1_76*C7;
    ROcp1_87 = ROcp1_25*S7+ROcp1_86*C7;
    ROcp1_97 = ROcp1_96*C7-S5*S7;
    RLcp1_17 = ROcp1_46*s->dpt[2][2];
    RLcp1_27 = ROcp1_56*s->dpt[2][2];
    RLcp1_37 = ROcp1_66*s->dpt[2][2];
    POcp1_17 = RLcp1_17+q[1];
    POcp1_27 = RLcp1_27+q[2];
    POcp1_37 = RLcp1_37+q[3];
    JTcp1_17_5 = RLcp1_37*C4;
    JTcp1_27_5 = RLcp1_37*S4;
    JTcp1_37_5 = -(RLcp1_17*C4+RLcp1_27*S4);
    JTcp1_17_6 = RLcp1_27*S5+RLcp1_37*ROcp1_25;
    JTcp1_27_6 = -(RLcp1_17*S5+RLcp1_37*ROcp1_15);
    JTcp1_37_6 = -(RLcp1_17*ROcp1_25-RLcp1_27*ROcp1_15);
    OMcp1_17 = OMcp1_16+ROcp1_46*qd[7];
    OMcp1_27 = OMcp1_26+ROcp1_56*qd[7];
    OMcp1_37 = OMcp1_36+ROcp1_66*qd[7];
    ORcp1_17 = OMcp1_26*RLcp1_37-OMcp1_36*RLcp1_27;
    ORcp1_27 = -(OMcp1_16*RLcp1_37-OMcp1_36*RLcp1_17);
    ORcp1_37 = OMcp1_16*RLcp1_27-OMcp1_26*RLcp1_17;
    VIcp1_17 = ORcp1_17+qd[1];
    VIcp1_27 = ORcp1_27+qd[2];
    VIcp1_37 = ORcp1_37+qd[3];
    OPcp1_17 = OPcp1_16+ROcp1_46*qdd[7]+qd[7]*(OMcp1_26*ROcp1_66-OMcp1_36*ROcp1_56);
    OPcp1_27 = OPcp1_26+ROcp1_56*qdd[7]-qd[7]*(OMcp1_16*ROcp1_66-OMcp1_36*ROcp1_46);
    OPcp1_37 = OPcp1_36+ROcp1_66*qdd[7]+qd[7]*(OMcp1_16*ROcp1_56-OMcp1_26*ROcp1_46);
    ACcp1_17 = qdd[1]+OMcp1_26*ORcp1_37-OMcp1_36*ORcp1_27+OPcp1_26*RLcp1_37-OPcp1_36*RLcp1_27;
    ACcp1_27 = qdd[2]-OMcp1_16*ORcp1_37+OMcp1_36*ORcp1_17-OPcp1_16*RLcp1_37+OPcp1_36*RLcp1_17;
    ACcp1_37 = qdd[3]+OMcp1_16*ORcp1_27-OMcp1_26*ORcp1_17+OPcp1_16*RLcp1_27-OPcp1_26*RLcp1_17;

// = = Block_1_0_0_2_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp1_17;
    sens->P[2] = POcp1_27;
    sens->P[3] = POcp1_37;
    sens->R[1][1] = ROcp1_17;
    sens->R[1][2] = ROcp1_27;
    sens->R[1][3] = ROcp1_37;
    sens->R[2][1] = ROcp1_46;
    sens->R[2][2] = ROcp1_56;
    sens->R[2][3] = ROcp1_66;
    sens->R[3][1] = ROcp1_77;
    sens->R[3][2] = ROcp1_87;
    sens->R[3][3] = ROcp1_97;
    sens->V[1] = VIcp1_17;
    sens->V[2] = VIcp1_27;
    sens->V[3] = VIcp1_37;
    sens->OM[1] = OMcp1_17;
    sens->OM[2] = OMcp1_27;
    sens->OM[3] = OMcp1_37;
    sens->J[1][1] = (1.0);
    sens->J[1][4] = -RLcp1_27;
    sens->J[1][5] = JTcp1_17_5;
    sens->J[1][6] = JTcp1_17_6;
    sens->J[2][2] = (1.0);
    sens->J[2][4] = RLcp1_17;
    sens->J[2][5] = JTcp1_27_5;
    sens->J[2][6] = JTcp1_27_6;
    sens->J[3][3] = (1.0);
    sens->J[3][5] = JTcp1_37_5;
    sens->J[3][6] = JTcp1_37_6;
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp1_15;
    sens->J[4][7] = ROcp1_46;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp1_25;
    sens->J[5][7] = ROcp1_56;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->J[6][7] = ROcp1_66;
    sens->A[1] = ACcp1_17;
    sens->A[2] = ACcp1_27;
    sens->A[3] = ACcp1_37;
    sens->OMP[1] = OPcp1_17;
    sens->OMP[2] = OPcp1_27;
    sens->OMP[3] = OPcp1_37;
 
// 
break;
case 3:
 


// = = Block_1_0_0_3_0_1 = = 
 
// Sensor Kinematics 


    ROcp2_15 = C4*C5;
    ROcp2_25 = S4*C5;
    ROcp2_75 = C4*S5;
    ROcp2_85 = S4*S5;
    ROcp2_46 = ROcp2_75*S6-S4*C6;
    ROcp2_56 = ROcp2_85*S6+C4*C6;
    ROcp2_66 = C5*S6;
    ROcp2_76 = ROcp2_75*C6+S4*S6;
    ROcp2_86 = ROcp2_85*C6-C4*S6;
    ROcp2_96 = C5*C6;
    OMcp2_15 = -qd[5]*S4;
    OMcp2_25 = qd[5]*C4;
    OMcp2_16 = OMcp2_15+ROcp2_15*qd[6];
    OMcp2_26 = OMcp2_25+ROcp2_25*qd[6];
    OMcp2_36 = qd[4]-qd[6]*S5;
    OPcp2_16 = ROcp2_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp2_25*S5+ROcp2_25*qd[4]);
    OPcp2_26 = ROcp2_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp2_15*S5+ROcp2_15*qd[4]);
    OPcp2_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_3_0_3 = = 
 
// Sensor Kinematics 


    ROcp2_18 = ROcp2_15*C8-ROcp2_76*S8;
    ROcp2_28 = ROcp2_25*C8-ROcp2_86*S8;
    ROcp2_38 = -(ROcp2_96*S8+S5*C8);
    ROcp2_78 = ROcp2_15*S8+ROcp2_76*C8;
    ROcp2_88 = ROcp2_25*S8+ROcp2_86*C8;
    ROcp2_98 = ROcp2_96*C8-S5*S8;
    RLcp2_18 = ROcp2_46*s->dpt[2][3];
    RLcp2_28 = ROcp2_56*s->dpt[2][3];
    RLcp2_38 = ROcp2_66*s->dpt[2][3];
    POcp2_18 = RLcp2_18+q[1];
    POcp2_28 = RLcp2_28+q[2];
    POcp2_38 = RLcp2_38+q[3];
    JTcp2_18_5 = RLcp2_38*C4;
    JTcp2_28_5 = RLcp2_38*S4;
    JTcp2_38_5 = -(RLcp2_18*C4+RLcp2_28*S4);
    JTcp2_18_6 = RLcp2_28*S5+RLcp2_38*ROcp2_25;
    JTcp2_28_6 = -(RLcp2_18*S5+RLcp2_38*ROcp2_15);
    JTcp2_38_6 = -(RLcp2_18*ROcp2_25-RLcp2_28*ROcp2_15);
    OMcp2_18 = OMcp2_16+ROcp2_46*qd[8];
    OMcp2_28 = OMcp2_26+ROcp2_56*qd[8];
    OMcp2_38 = OMcp2_36+ROcp2_66*qd[8];
    ORcp2_18 = OMcp2_26*RLcp2_38-OMcp2_36*RLcp2_28;
    ORcp2_28 = -(OMcp2_16*RLcp2_38-OMcp2_36*RLcp2_18);
    ORcp2_38 = OMcp2_16*RLcp2_28-OMcp2_26*RLcp2_18;
    VIcp2_18 = ORcp2_18+qd[1];
    VIcp2_28 = ORcp2_28+qd[2];
    VIcp2_38 = ORcp2_38+qd[3];
    OPcp2_18 = OPcp2_16+ROcp2_46*qdd[8]+qd[8]*(OMcp2_26*ROcp2_66-OMcp2_36*ROcp2_56);
    OPcp2_28 = OPcp2_26+ROcp2_56*qdd[8]-qd[8]*(OMcp2_16*ROcp2_66-OMcp2_36*ROcp2_46);
    OPcp2_38 = OPcp2_36+ROcp2_66*qdd[8]+qd[8]*(OMcp2_16*ROcp2_56-OMcp2_26*ROcp2_46);
    ACcp2_18 = qdd[1]+OMcp2_26*ORcp2_38-OMcp2_36*ORcp2_28+OPcp2_26*RLcp2_38-OPcp2_36*RLcp2_28;
    ACcp2_28 = qdd[2]-OMcp2_16*ORcp2_38+OMcp2_36*ORcp2_18-OPcp2_16*RLcp2_38+OPcp2_36*RLcp2_18;
    ACcp2_38 = qdd[3]+OMcp2_16*ORcp2_28-OMcp2_26*ORcp2_18+OPcp2_16*RLcp2_28-OPcp2_26*RLcp2_18;

// = = Block_1_0_0_3_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp2_18;
    sens->P[2] = POcp2_28;
    sens->P[3] = POcp2_38;
    sens->R[1][1] = ROcp2_18;
    sens->R[1][2] = ROcp2_28;
    sens->R[1][3] = ROcp2_38;
    sens->R[2][1] = ROcp2_46;
    sens->R[2][2] = ROcp2_56;
    sens->R[2][3] = ROcp2_66;
    sens->R[3][1] = ROcp2_78;
    sens->R[3][2] = ROcp2_88;
    sens->R[3][3] = ROcp2_98;
    sens->V[1] = VIcp2_18;
    sens->V[2] = VIcp2_28;
    sens->V[3] = VIcp2_38;
    sens->OM[1] = OMcp2_18;
    sens->OM[2] = OMcp2_28;
    sens->OM[3] = OMcp2_38;
    sens->J[1][1] = (1.0);
    sens->J[1][4] = -RLcp2_28;
    sens->J[1][5] = JTcp2_18_5;
    sens->J[1][6] = JTcp2_18_6;
    sens->J[2][2] = (1.0);
    sens->J[2][4] = RLcp2_18;
    sens->J[2][5] = JTcp2_28_5;
    sens->J[2][6] = JTcp2_28_6;
    sens->J[3][3] = (1.0);
    sens->J[3][5] = JTcp2_38_5;
    sens->J[3][6] = JTcp2_38_6;
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp2_15;
    sens->J[4][8] = ROcp2_46;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp2_25;
    sens->J[5][8] = ROcp2_56;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->J[6][8] = ROcp2_66;
    sens->A[1] = ACcp2_18;
    sens->A[2] = ACcp2_28;
    sens->A[3] = ACcp2_38;
    sens->OMP[1] = OPcp2_18;
    sens->OMP[2] = OPcp2_28;
    sens->OMP[3] = OPcp2_38;
 
// 
break;
case 4:
 


// = = Block_1_0_0_4_0_1 = = 
 
// Sensor Kinematics 


    ROcp3_15 = C4*C5;
    ROcp3_25 = S4*C5;
    ROcp3_75 = C4*S5;
    ROcp3_85 = S4*S5;
    ROcp3_46 = ROcp3_75*S6-S4*C6;
    ROcp3_56 = ROcp3_85*S6+C4*C6;
    ROcp3_66 = C5*S6;
    ROcp3_76 = ROcp3_75*C6+S4*S6;
    ROcp3_86 = ROcp3_85*C6-C4*S6;
    ROcp3_96 = C5*C6;
    OMcp3_15 = -qd[5]*S4;
    OMcp3_25 = qd[5]*C4;
    OMcp3_16 = OMcp3_15+ROcp3_15*qd[6];
    OMcp3_26 = OMcp3_25+ROcp3_25*qd[6];
    OMcp3_36 = qd[4]-qd[6]*S5;
    OPcp3_16 = ROcp3_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp3_25*S5+ROcp3_25*qd[4]);
    OPcp3_26 = ROcp3_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp3_15*S5+ROcp3_15*qd[4]);
    OPcp3_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_4_0_4 = = 
 
// Sensor Kinematics 


    ROcp3_19 = ROcp3_15*C9-ROcp3_76*S9;
    ROcp3_29 = ROcp3_25*C9-ROcp3_86*S9;
    ROcp3_39 = -(ROcp3_96*S9+S5*C9);
    ROcp3_79 = ROcp3_15*S9+ROcp3_76*C9;
    ROcp3_89 = ROcp3_25*S9+ROcp3_86*C9;
    ROcp3_99 = ROcp3_96*C9-S5*S9;
    RLcp3_19 = ROcp3_46*s->dpt[2][4]+ROcp3_76*s->dpt[3][4];
    RLcp3_29 = ROcp3_56*s->dpt[2][4]+ROcp3_86*s->dpt[3][4];
    RLcp3_39 = ROcp3_66*s->dpt[2][4]+ROcp3_96*s->dpt[3][4];
    POcp3_19 = RLcp3_19+q[1];
    POcp3_29 = RLcp3_29+q[2];
    POcp3_39 = RLcp3_39+q[3];
    JTcp3_19_5 = RLcp3_39*C4;
    JTcp3_29_5 = RLcp3_39*S4;
    JTcp3_39_5 = -(RLcp3_19*C4+RLcp3_29*S4);
    JTcp3_19_6 = RLcp3_29*S5+RLcp3_39*ROcp3_25;
    JTcp3_29_6 = -(RLcp3_19*S5+RLcp3_39*ROcp3_15);
    JTcp3_39_6 = -(RLcp3_19*ROcp3_25-RLcp3_29*ROcp3_15);
    OMcp3_19 = OMcp3_16+ROcp3_46*qd[9];
    OMcp3_29 = OMcp3_26+ROcp3_56*qd[9];
    OMcp3_39 = OMcp3_36+ROcp3_66*qd[9];
    ORcp3_19 = OMcp3_26*RLcp3_39-OMcp3_36*RLcp3_29;
    ORcp3_29 = -(OMcp3_16*RLcp3_39-OMcp3_36*RLcp3_19);
    ORcp3_39 = OMcp3_16*RLcp3_29-OMcp3_26*RLcp3_19;
    VIcp3_19 = ORcp3_19+qd[1];
    VIcp3_29 = ORcp3_29+qd[2];
    VIcp3_39 = ORcp3_39+qd[3];
    OPcp3_19 = OPcp3_16+ROcp3_46*qdd[9]+qd[9]*(OMcp3_26*ROcp3_66-OMcp3_36*ROcp3_56);
    OPcp3_29 = OPcp3_26+ROcp3_56*qdd[9]-qd[9]*(OMcp3_16*ROcp3_66-OMcp3_36*ROcp3_46);
    OPcp3_39 = OPcp3_36+ROcp3_66*qdd[9]+qd[9]*(OMcp3_16*ROcp3_56-OMcp3_26*ROcp3_46);
    ACcp3_19 = qdd[1]+OMcp3_26*ORcp3_39-OMcp3_36*ORcp3_29+OPcp3_26*RLcp3_39-OPcp3_36*RLcp3_29;
    ACcp3_29 = qdd[2]-OMcp3_16*ORcp3_39+OMcp3_36*ORcp3_19-OPcp3_16*RLcp3_39+OPcp3_36*RLcp3_19;
    ACcp3_39 = qdd[3]+OMcp3_16*ORcp3_29-OMcp3_26*ORcp3_19+OPcp3_16*RLcp3_29-OPcp3_26*RLcp3_19;

// = = Block_1_0_0_4_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp3_19;
    sens->P[2] = POcp3_29;
    sens->P[3] = POcp3_39;
    sens->R[1][1] = ROcp3_19;
    sens->R[1][2] = ROcp3_29;
    sens->R[1][3] = ROcp3_39;
    sens->R[2][1] = ROcp3_46;
    sens->R[2][2] = ROcp3_56;
    sens->R[2][3] = ROcp3_66;
    sens->R[3][1] = ROcp3_79;
    sens->R[3][2] = ROcp3_89;
    sens->R[3][3] = ROcp3_99;
    sens->V[1] = VIcp3_19;
    sens->V[2] = VIcp3_29;
    sens->V[3] = VIcp3_39;
    sens->OM[1] = OMcp3_19;
    sens->OM[2] = OMcp3_29;
    sens->OM[3] = OMcp3_39;
    sens->J[1][1] = (1.0);
    sens->J[1][4] = -RLcp3_29;
    sens->J[1][5] = JTcp3_19_5;
    sens->J[1][6] = JTcp3_19_6;
    sens->J[2][2] = (1.0);
    sens->J[2][4] = RLcp3_19;
    sens->J[2][5] = JTcp3_29_5;
    sens->J[2][6] = JTcp3_29_6;
    sens->J[3][3] = (1.0);
    sens->J[3][5] = JTcp3_39_5;
    sens->J[3][6] = JTcp3_39_6;
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp3_15;
    sens->J[4][9] = ROcp3_46;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp3_25;
    sens->J[5][9] = ROcp3_56;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->J[6][9] = ROcp3_66;
    sens->A[1] = ACcp3_19;
    sens->A[2] = ACcp3_29;
    sens->A[3] = ACcp3_39;
    sens->OMP[1] = OPcp3_19;
    sens->OMP[2] = OPcp3_29;
    sens->OMP[3] = OPcp3_39;
 
// 
break;
case 5:
 


// = = Block_1_0_0_5_0_1 = = 
 
// Sensor Kinematics 


    ROcp4_15 = C4*C5;
    ROcp4_25 = S4*C5;
    ROcp4_75 = C4*S5;
    ROcp4_85 = S4*S5;
    ROcp4_46 = ROcp4_75*S6-S4*C6;
    ROcp4_56 = ROcp4_85*S6+C4*C6;
    ROcp4_66 = C5*S6;
    ROcp4_76 = ROcp4_75*C6+S4*S6;
    ROcp4_86 = ROcp4_85*C6-C4*S6;
    ROcp4_96 = C5*C6;
    OMcp4_15 = -qd[5]*S4;
    OMcp4_25 = qd[5]*C4;
    OMcp4_16 = OMcp4_15+ROcp4_15*qd[6];
    OMcp4_26 = OMcp4_25+ROcp4_25*qd[6];
    OMcp4_36 = qd[4]-qd[6]*S5;
    OPcp4_16 = ROcp4_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp4_25*S5+ROcp4_25*qd[4]);
    OPcp4_26 = ROcp4_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp4_15*S5+ROcp4_15*qd[4]);
    OPcp4_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_5_0_4 = = 
 
// Sensor Kinematics 


    ROcp4_19 = ROcp4_15*C9-ROcp4_76*S9;
    ROcp4_29 = ROcp4_25*C9-ROcp4_86*S9;
    ROcp4_39 = -(ROcp4_96*S9+S5*C9);
    ROcp4_79 = ROcp4_15*S9+ROcp4_76*C9;
    ROcp4_89 = ROcp4_25*S9+ROcp4_86*C9;
    ROcp4_99 = ROcp4_96*C9-S5*S9;
    ROcp4_410 = ROcp4_46*C10+ROcp4_79*S10;
    ROcp4_510 = ROcp4_56*C10+ROcp4_89*S10;
    ROcp4_610 = ROcp4_66*C10+ROcp4_99*S10;
    ROcp4_710 = -(ROcp4_46*S10-ROcp4_79*C10);
    ROcp4_810 = -(ROcp4_56*S10-ROcp4_89*C10);
    ROcp4_910 = -(ROcp4_66*S10-ROcp4_99*C10);
    RLcp4_19 = ROcp4_46*s->dpt[2][4]+ROcp4_76*s->dpt[3][4];
    RLcp4_29 = ROcp4_56*s->dpt[2][4]+ROcp4_86*s->dpt[3][4];
    RLcp4_39 = ROcp4_66*s->dpt[2][4]+ROcp4_96*s->dpt[3][4];
    POcp4_19 = RLcp4_19+q[1];
    POcp4_29 = RLcp4_29+q[2];
    POcp4_39 = RLcp4_39+q[3];
    JTcp4_19_5 = RLcp4_39*C4;
    JTcp4_29_5 = RLcp4_39*S4;
    JTcp4_39_5 = -(RLcp4_19*C4+RLcp4_29*S4);
    JTcp4_19_6 = RLcp4_29*S5+RLcp4_39*ROcp4_25;
    JTcp4_29_6 = -(RLcp4_19*S5+RLcp4_39*ROcp4_15);
    JTcp4_39_6 = -(RLcp4_19*ROcp4_25-RLcp4_29*ROcp4_15);
    OMcp4_19 = OMcp4_16+ROcp4_46*qd[9];
    OMcp4_29 = OMcp4_26+ROcp4_56*qd[9];
    OMcp4_39 = OMcp4_36+ROcp4_66*qd[9];
    ORcp4_19 = OMcp4_26*RLcp4_39-OMcp4_36*RLcp4_29;
    ORcp4_29 = -(OMcp4_16*RLcp4_39-OMcp4_36*RLcp4_19);
    ORcp4_39 = OMcp4_16*RLcp4_29-OMcp4_26*RLcp4_19;
    VIcp4_19 = ORcp4_19+qd[1];
    VIcp4_29 = ORcp4_29+qd[2];
    VIcp4_39 = ORcp4_39+qd[3];
    ACcp4_19 = qdd[1]+OMcp4_26*ORcp4_39-OMcp4_36*ORcp4_29+OPcp4_26*RLcp4_39-OPcp4_36*RLcp4_29;
    ACcp4_29 = qdd[2]-OMcp4_16*ORcp4_39+OMcp4_36*ORcp4_19-OPcp4_16*RLcp4_39+OPcp4_36*RLcp4_19;
    ACcp4_39 = qdd[3]+OMcp4_16*ORcp4_29-OMcp4_26*ORcp4_19+OPcp4_16*RLcp4_29-OPcp4_26*RLcp4_19;
    OMcp4_110 = OMcp4_19+ROcp4_19*qd[10];
    OMcp4_210 = OMcp4_29+ROcp4_29*qd[10];
    OMcp4_310 = OMcp4_39+ROcp4_39*qd[10];
    OPcp4_110 = OPcp4_16+ROcp4_19*qdd[10]+ROcp4_46*qdd[9]+qd[10]*(OMcp4_29*ROcp4_39-OMcp4_39*ROcp4_29)+qd[9]*(OMcp4_26*
 ROcp4_66-OMcp4_36*ROcp4_56);
    OPcp4_210 = OPcp4_26+ROcp4_29*qdd[10]+ROcp4_56*qdd[9]-qd[10]*(OMcp4_19*ROcp4_39-OMcp4_39*ROcp4_19)-qd[9]*(OMcp4_16*
 ROcp4_66-OMcp4_36*ROcp4_46);
    OPcp4_310 = OPcp4_36+ROcp4_39*qdd[10]+ROcp4_66*qdd[9]+qd[10]*(OMcp4_19*ROcp4_29-OMcp4_29*ROcp4_19)+qd[9]*(OMcp4_16*
 ROcp4_56-OMcp4_26*ROcp4_46);

// = = Block_1_0_0_5_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp4_19;
    sens->P[2] = POcp4_29;
    sens->P[3] = POcp4_39;
    sens->R[1][1] = ROcp4_19;
    sens->R[1][2] = ROcp4_29;
    sens->R[1][3] = ROcp4_39;
    sens->R[2][1] = ROcp4_410;
    sens->R[2][2] = ROcp4_510;
    sens->R[2][3] = ROcp4_610;
    sens->R[3][1] = ROcp4_710;
    sens->R[3][2] = ROcp4_810;
    sens->R[3][3] = ROcp4_910;
    sens->V[1] = VIcp4_19;
    sens->V[2] = VIcp4_29;
    sens->V[3] = VIcp4_39;
    sens->OM[1] = OMcp4_110;
    sens->OM[2] = OMcp4_210;
    sens->OM[3] = OMcp4_310;
    sens->J[1][1] = (1.0);
    sens->J[1][4] = -RLcp4_29;
    sens->J[1][5] = JTcp4_19_5;
    sens->J[1][6] = JTcp4_19_6;
    sens->J[2][2] = (1.0);
    sens->J[2][4] = RLcp4_19;
    sens->J[2][5] = JTcp4_29_5;
    sens->J[2][6] = JTcp4_29_6;
    sens->J[3][3] = (1.0);
    sens->J[3][5] = JTcp4_39_5;
    sens->J[3][6] = JTcp4_39_6;
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp4_15;
    sens->J[4][9] = ROcp4_46;
    sens->J[4][10] = ROcp4_19;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp4_25;
    sens->J[5][9] = ROcp4_56;
    sens->J[5][10] = ROcp4_29;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->J[6][9] = ROcp4_66;
    sens->J[6][10] = ROcp4_39;
    sens->A[1] = ACcp4_19;
    sens->A[2] = ACcp4_29;
    sens->A[3] = ACcp4_39;
    sens->OMP[1] = OPcp4_110;
    sens->OMP[2] = OPcp4_210;
    sens->OMP[3] = OPcp4_310;
 
// 
break;
case 6:
 


// = = Block_1_0_0_6_0_1 = = 
 
// Sensor Kinematics 


    ROcp5_15 = C4*C5;
    ROcp5_25 = S4*C5;
    ROcp5_75 = C4*S5;
    ROcp5_85 = S4*S5;
    ROcp5_46 = ROcp5_75*S6-S4*C6;
    ROcp5_56 = ROcp5_85*S6+C4*C6;
    ROcp5_66 = C5*S6;
    ROcp5_76 = ROcp5_75*C6+S4*S6;
    ROcp5_86 = ROcp5_85*C6-C4*S6;
    ROcp5_96 = C5*C6;
    OMcp5_15 = -qd[5]*S4;
    OMcp5_25 = qd[5]*C4;
    OMcp5_16 = OMcp5_15+ROcp5_15*qd[6];
    OMcp5_26 = OMcp5_25+ROcp5_25*qd[6];
    OMcp5_36 = qd[4]-qd[6]*S5;
    OPcp5_16 = ROcp5_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp5_25*S5+ROcp5_25*qd[4]);
    OPcp5_26 = ROcp5_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp5_15*S5+ROcp5_15*qd[4]);
    OPcp5_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_6_0_4 = = 
 
// Sensor Kinematics 


    ROcp5_19 = ROcp5_15*C9-ROcp5_76*S9;
    ROcp5_29 = ROcp5_25*C9-ROcp5_86*S9;
    ROcp5_39 = -(ROcp5_96*S9+S5*C9);
    ROcp5_79 = ROcp5_15*S9+ROcp5_76*C9;
    ROcp5_89 = ROcp5_25*S9+ROcp5_86*C9;
    ROcp5_99 = ROcp5_96*C9-S5*S9;
    ROcp5_410 = ROcp5_46*C10+ROcp5_79*S10;
    ROcp5_510 = ROcp5_56*C10+ROcp5_89*S10;
    ROcp5_610 = ROcp5_66*C10+ROcp5_99*S10;
    ROcp5_710 = -(ROcp5_46*S10-ROcp5_79*C10);
    ROcp5_810 = -(ROcp5_56*S10-ROcp5_89*C10);
    ROcp5_910 = -(ROcp5_66*S10-ROcp5_99*C10);
    ROcp5_111 = ROcp5_19*C11+ROcp5_410*S11;
    ROcp5_211 = ROcp5_29*C11+ROcp5_510*S11;
    ROcp5_311 = ROcp5_39*C11+ROcp5_610*S11;
    ROcp5_411 = -(ROcp5_19*S11-ROcp5_410*C11);
    ROcp5_511 = -(ROcp5_29*S11-ROcp5_510*C11);
    ROcp5_611 = -(ROcp5_39*S11-ROcp5_610*C11);
    RLcp5_19 = ROcp5_46*s->dpt[2][4]+ROcp5_76*s->dpt[3][4];
    RLcp5_29 = ROcp5_56*s->dpt[2][4]+ROcp5_86*s->dpt[3][4];
    RLcp5_39 = ROcp5_66*s->dpt[2][4]+ROcp5_96*s->dpt[3][4];
    OMcp5_19 = OMcp5_16+ROcp5_46*qd[9];
    OMcp5_29 = OMcp5_26+ROcp5_56*qd[9];
    OMcp5_39 = OMcp5_36+ROcp5_66*qd[9];
    ORcp5_19 = OMcp5_26*RLcp5_39-OMcp5_36*RLcp5_29;
    ORcp5_29 = -(OMcp5_16*RLcp5_39-OMcp5_36*RLcp5_19);
    ORcp5_39 = OMcp5_16*RLcp5_29-OMcp5_26*RLcp5_19;
    OMcp5_110 = OMcp5_19+ROcp5_19*qd[10];
    OMcp5_210 = OMcp5_29+ROcp5_29*qd[10];
    OMcp5_310 = OMcp5_39+ROcp5_39*qd[10];
    OPcp5_110 = OPcp5_16+ROcp5_19*qdd[10]+ROcp5_46*qdd[9]+qd[10]*(OMcp5_29*ROcp5_39-OMcp5_39*ROcp5_29)+qd[9]*(OMcp5_26*
 ROcp5_66-OMcp5_36*ROcp5_56);
    OPcp5_210 = OPcp5_26+ROcp5_29*qdd[10]+ROcp5_56*qdd[9]-qd[10]*(OMcp5_19*ROcp5_39-OMcp5_39*ROcp5_19)-qd[9]*(OMcp5_16*
 ROcp5_66-OMcp5_36*ROcp5_46);
    OPcp5_310 = OPcp5_36+ROcp5_39*qdd[10]+ROcp5_66*qdd[9]+qd[10]*(OMcp5_19*ROcp5_29-OMcp5_29*ROcp5_19)+qd[9]*(OMcp5_16*
 ROcp5_56-OMcp5_26*ROcp5_46);
    RLcp5_111 = ROcp5_710*s->dpt[3][11];
    RLcp5_211 = ROcp5_810*s->dpt[3][11];
    RLcp5_311 = ROcp5_910*s->dpt[3][11];
    POcp5_111 = RLcp5_111+RLcp5_19+q[1];
    POcp5_211 = RLcp5_211+RLcp5_29+q[2];
    POcp5_311 = RLcp5_311+RLcp5_39+q[3];
    JTcp5_111_4 = -(RLcp5_211+RLcp5_29);
    JTcp5_211_4 = RLcp5_111+RLcp5_19;
    JTcp5_111_5 = C4*(RLcp5_311+RLcp5_39);
    JTcp5_211_5 = S4*(RLcp5_311+RLcp5_39);
    JTcp5_311_5 = -(C4*(RLcp5_111+RLcp5_19)+S4*(RLcp5_211+RLcp5_29));
    JTcp5_111_6 = ROcp5_25*(RLcp5_311+RLcp5_39)+S5*(RLcp5_211+RLcp5_29);
    JTcp5_211_6 = -(ROcp5_15*(RLcp5_311+RLcp5_39)+S5*(RLcp5_111+RLcp5_19));
    JTcp5_311_6 = ROcp5_15*(RLcp5_211+RLcp5_29)-ROcp5_25*(RLcp5_111+RLcp5_19);
    JTcp5_111_7 = -(RLcp5_211*ROcp5_66-RLcp5_311*ROcp5_56);
    JTcp5_211_7 = RLcp5_111*ROcp5_66-RLcp5_311*ROcp5_46;
    JTcp5_311_7 = -(RLcp5_111*ROcp5_56-RLcp5_211*ROcp5_46);
    JTcp5_111_8 = -(RLcp5_211*ROcp5_39-RLcp5_311*ROcp5_29);
    JTcp5_211_8 = RLcp5_111*ROcp5_39-RLcp5_311*ROcp5_19;
    JTcp5_311_8 = -(RLcp5_111*ROcp5_29-RLcp5_211*ROcp5_19);
    OMcp5_111 = OMcp5_110+ROcp5_710*qd[11];
    OMcp5_211 = OMcp5_210+ROcp5_810*qd[11];
    OMcp5_311 = OMcp5_310+ROcp5_910*qd[11];
    ORcp5_111 = OMcp5_210*RLcp5_311-OMcp5_310*RLcp5_211;
    ORcp5_211 = -(OMcp5_110*RLcp5_311-OMcp5_310*RLcp5_111);
    ORcp5_311 = OMcp5_110*RLcp5_211-OMcp5_210*RLcp5_111;
    VIcp5_111 = ORcp5_111+ORcp5_19+qd[1];
    VIcp5_211 = ORcp5_211+ORcp5_29+qd[2];
    VIcp5_311 = ORcp5_311+ORcp5_39+qd[3];
    OPcp5_111 = OPcp5_110+ROcp5_710*qdd[11]+qd[11]*(OMcp5_210*ROcp5_910-OMcp5_310*ROcp5_810);
    OPcp5_211 = OPcp5_210+ROcp5_810*qdd[11]-qd[11]*(OMcp5_110*ROcp5_910-OMcp5_310*ROcp5_710);
    OPcp5_311 = OPcp5_310+ROcp5_910*qdd[11]+qd[11]*(OMcp5_110*ROcp5_810-OMcp5_210*ROcp5_710);
    ACcp5_111 = qdd[1]+OMcp5_210*ORcp5_311+OMcp5_26*ORcp5_39-OMcp5_310*ORcp5_211-OMcp5_36*ORcp5_29+OPcp5_210*RLcp5_311+
 OPcp5_26*RLcp5_39-OPcp5_310*RLcp5_211-OPcp5_36*RLcp5_29;
    ACcp5_211 = qdd[2]-OMcp5_110*ORcp5_311-OMcp5_16*ORcp5_39+OMcp5_310*ORcp5_111+OMcp5_36*ORcp5_19-OPcp5_110*RLcp5_311-
 OPcp5_16*RLcp5_39+OPcp5_310*RLcp5_111+OPcp5_36*RLcp5_19;
    ACcp5_311 = qdd[3]+OMcp5_110*ORcp5_211+OMcp5_16*ORcp5_29-OMcp5_210*ORcp5_111-OMcp5_26*ORcp5_19+OPcp5_110*RLcp5_211+
 OPcp5_16*RLcp5_29-OPcp5_210*RLcp5_111-OPcp5_26*RLcp5_19;

// = = Block_1_0_0_6_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp5_111;
    sens->P[2] = POcp5_211;
    sens->P[3] = POcp5_311;
    sens->R[1][1] = ROcp5_111;
    sens->R[1][2] = ROcp5_211;
    sens->R[1][3] = ROcp5_311;
    sens->R[2][1] = ROcp5_411;
    sens->R[2][2] = ROcp5_511;
    sens->R[2][3] = ROcp5_611;
    sens->R[3][1] = ROcp5_710;
    sens->R[3][2] = ROcp5_810;
    sens->R[3][3] = ROcp5_910;
    sens->V[1] = VIcp5_111;
    sens->V[2] = VIcp5_211;
    sens->V[3] = VIcp5_311;
    sens->OM[1] = OMcp5_111;
    sens->OM[2] = OMcp5_211;
    sens->OM[3] = OMcp5_311;
    sens->J[1][1] = (1.0);
    sens->J[1][4] = JTcp5_111_4;
    sens->J[1][5] = JTcp5_111_5;
    sens->J[1][6] = JTcp5_111_6;
    sens->J[1][9] = JTcp5_111_7;
    sens->J[1][10] = JTcp5_111_8;
    sens->J[2][2] = (1.0);
    sens->J[2][4] = JTcp5_211_4;
    sens->J[2][5] = JTcp5_211_5;
    sens->J[2][6] = JTcp5_211_6;
    sens->J[2][9] = JTcp5_211_7;
    sens->J[2][10] = JTcp5_211_8;
    sens->J[3][3] = (1.0);
    sens->J[3][5] = JTcp5_311_5;
    sens->J[3][6] = JTcp5_311_6;
    sens->J[3][9] = JTcp5_311_7;
    sens->J[3][10] = JTcp5_311_8;
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp5_15;
    sens->J[4][9] = ROcp5_46;
    sens->J[4][10] = ROcp5_19;
    sens->J[4][11] = ROcp5_710;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp5_25;
    sens->J[5][9] = ROcp5_56;
    sens->J[5][10] = ROcp5_29;
    sens->J[5][11] = ROcp5_810;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->J[6][9] = ROcp5_66;
    sens->J[6][10] = ROcp5_39;
    sens->J[6][11] = ROcp5_910;
    sens->A[1] = ACcp5_111;
    sens->A[2] = ACcp5_211;
    sens->A[3] = ACcp5_311;
    sens->OMP[1] = OPcp5_111;
    sens->OMP[2] = OPcp5_211;
    sens->OMP[3] = OPcp5_311;
 
// 
break;
case 7:
 


// = = Block_1_0_0_7_0_1 = = 
 
// Sensor Kinematics 


    ROcp6_15 = C4*C5;
    ROcp6_25 = S4*C5;
    ROcp6_75 = C4*S5;
    ROcp6_85 = S4*S5;
    ROcp6_46 = ROcp6_75*S6-S4*C6;
    ROcp6_56 = ROcp6_85*S6+C4*C6;
    ROcp6_66 = C5*S6;
    ROcp6_76 = ROcp6_75*C6+S4*S6;
    ROcp6_86 = ROcp6_85*C6-C4*S6;
    ROcp6_96 = C5*C6;
    OMcp6_15 = -qd[5]*S4;
    OMcp6_25 = qd[5]*C4;
    OMcp6_16 = OMcp6_15+ROcp6_15*qd[6];
    OMcp6_26 = OMcp6_25+ROcp6_25*qd[6];
    OMcp6_36 = qd[4]-qd[6]*S5;
    OPcp6_16 = ROcp6_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp6_25*S5+ROcp6_25*qd[4]);
    OPcp6_26 = ROcp6_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp6_15*S5+ROcp6_15*qd[4]);
    OPcp6_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_7_0_4 = = 
 
// Sensor Kinematics 


    ROcp6_19 = ROcp6_15*C9-ROcp6_76*S9;
    ROcp6_29 = ROcp6_25*C9-ROcp6_86*S9;
    ROcp6_39 = -(ROcp6_96*S9+S5*C9);
    ROcp6_79 = ROcp6_15*S9+ROcp6_76*C9;
    ROcp6_89 = ROcp6_25*S9+ROcp6_86*C9;
    ROcp6_99 = ROcp6_96*C9-S5*S9;
    ROcp6_410 = ROcp6_46*C10+ROcp6_79*S10;
    ROcp6_510 = ROcp6_56*C10+ROcp6_89*S10;
    ROcp6_610 = ROcp6_66*C10+ROcp6_99*S10;
    ROcp6_710 = -(ROcp6_46*S10-ROcp6_79*C10);
    ROcp6_810 = -(ROcp6_56*S10-ROcp6_89*C10);
    ROcp6_910 = -(ROcp6_66*S10-ROcp6_99*C10);
    ROcp6_111 = ROcp6_19*C11+ROcp6_410*S11;
    ROcp6_211 = ROcp6_29*C11+ROcp6_510*S11;
    ROcp6_311 = ROcp6_39*C11+ROcp6_610*S11;
    ROcp6_411 = -(ROcp6_19*S11-ROcp6_410*C11);
    ROcp6_511 = -(ROcp6_29*S11-ROcp6_510*C11);
    ROcp6_611 = -(ROcp6_39*S11-ROcp6_610*C11);
    ROcp6_112 = ROcp6_111*C12-ROcp6_710*S12;
    ROcp6_212 = ROcp6_211*C12-ROcp6_810*S12;
    ROcp6_312 = ROcp6_311*C12-ROcp6_910*S12;
    ROcp6_712 = ROcp6_111*S12+ROcp6_710*C12;
    ROcp6_812 = ROcp6_211*S12+ROcp6_810*C12;
    ROcp6_912 = ROcp6_311*S12+ROcp6_910*C12;
    RLcp6_19 = ROcp6_46*s->dpt[2][4]+ROcp6_76*s->dpt[3][4];
    RLcp6_29 = ROcp6_56*s->dpt[2][4]+ROcp6_86*s->dpt[3][4];
    RLcp6_39 = ROcp6_66*s->dpt[2][4]+ROcp6_96*s->dpt[3][4];
    OMcp6_19 = OMcp6_16+ROcp6_46*qd[9];
    OMcp6_29 = OMcp6_26+ROcp6_56*qd[9];
    OMcp6_39 = OMcp6_36+ROcp6_66*qd[9];
    ORcp6_19 = OMcp6_26*RLcp6_39-OMcp6_36*RLcp6_29;
    ORcp6_29 = -(OMcp6_16*RLcp6_39-OMcp6_36*RLcp6_19);
    ORcp6_39 = OMcp6_16*RLcp6_29-OMcp6_26*RLcp6_19;
    OMcp6_110 = OMcp6_19+ROcp6_19*qd[10];
    OMcp6_210 = OMcp6_29+ROcp6_29*qd[10];
    OMcp6_310 = OMcp6_39+ROcp6_39*qd[10];
    OPcp6_110 = OPcp6_16+ROcp6_19*qdd[10]+ROcp6_46*qdd[9]+qd[10]*(OMcp6_29*ROcp6_39-OMcp6_39*ROcp6_29)+qd[9]*(OMcp6_26*
 ROcp6_66-OMcp6_36*ROcp6_56);
    OPcp6_210 = OPcp6_26+ROcp6_29*qdd[10]+ROcp6_56*qdd[9]-qd[10]*(OMcp6_19*ROcp6_39-OMcp6_39*ROcp6_19)-qd[9]*(OMcp6_16*
 ROcp6_66-OMcp6_36*ROcp6_46);
    OPcp6_310 = OPcp6_36+ROcp6_39*qdd[10]+ROcp6_66*qdd[9]+qd[10]*(OMcp6_19*ROcp6_29-OMcp6_29*ROcp6_19)+qd[9]*(OMcp6_16*
 ROcp6_56-OMcp6_26*ROcp6_46);
    RLcp6_111 = ROcp6_710*s->dpt[3][11];
    RLcp6_211 = ROcp6_810*s->dpt[3][11];
    RLcp6_311 = ROcp6_910*s->dpt[3][11];
    POcp6_111 = RLcp6_111+RLcp6_19+q[1];
    POcp6_211 = RLcp6_211+RLcp6_29+q[2];
    POcp6_311 = RLcp6_311+RLcp6_39+q[3];
    JTcp6_111_4 = -(RLcp6_211+RLcp6_29);
    JTcp6_211_4 = RLcp6_111+RLcp6_19;
    JTcp6_111_5 = C4*(RLcp6_311+RLcp6_39);
    JTcp6_211_5 = S4*(RLcp6_311+RLcp6_39);
    JTcp6_311_5 = -(C4*(RLcp6_111+RLcp6_19)+S4*(RLcp6_211+RLcp6_29));
    JTcp6_111_6 = ROcp6_25*(RLcp6_311+RLcp6_39)+S5*(RLcp6_211+RLcp6_29);
    JTcp6_211_6 = -(ROcp6_15*(RLcp6_311+RLcp6_39)+S5*(RLcp6_111+RLcp6_19));
    JTcp6_311_6 = ROcp6_15*(RLcp6_211+RLcp6_29)-ROcp6_25*(RLcp6_111+RLcp6_19);
    JTcp6_111_7 = -(RLcp6_211*ROcp6_66-RLcp6_311*ROcp6_56);
    JTcp6_211_7 = RLcp6_111*ROcp6_66-RLcp6_311*ROcp6_46;
    JTcp6_311_7 = -(RLcp6_111*ROcp6_56-RLcp6_211*ROcp6_46);
    JTcp6_111_8 = -(RLcp6_211*ROcp6_39-RLcp6_311*ROcp6_29);
    JTcp6_211_8 = RLcp6_111*ROcp6_39-RLcp6_311*ROcp6_19;
    JTcp6_311_8 = -(RLcp6_111*ROcp6_29-RLcp6_211*ROcp6_19);
    OMcp6_111 = OMcp6_110+ROcp6_710*qd[11];
    OMcp6_211 = OMcp6_210+ROcp6_810*qd[11];
    OMcp6_311 = OMcp6_310+ROcp6_910*qd[11];
    ORcp6_111 = OMcp6_210*RLcp6_311-OMcp6_310*RLcp6_211;
    ORcp6_211 = -(OMcp6_110*RLcp6_311-OMcp6_310*RLcp6_111);
    ORcp6_311 = OMcp6_110*RLcp6_211-OMcp6_210*RLcp6_111;
    VIcp6_111 = ORcp6_111+ORcp6_19+qd[1];
    VIcp6_211 = ORcp6_211+ORcp6_29+qd[2];
    VIcp6_311 = ORcp6_311+ORcp6_39+qd[3];
    ACcp6_111 = qdd[1]+OMcp6_210*ORcp6_311+OMcp6_26*ORcp6_39-OMcp6_310*ORcp6_211-OMcp6_36*ORcp6_29+OPcp6_210*RLcp6_311+
 OPcp6_26*RLcp6_39-OPcp6_310*RLcp6_211-OPcp6_36*RLcp6_29;
    ACcp6_211 = qdd[2]-OMcp6_110*ORcp6_311-OMcp6_16*ORcp6_39+OMcp6_310*ORcp6_111+OMcp6_36*ORcp6_19-OPcp6_110*RLcp6_311-
 OPcp6_16*RLcp6_39+OPcp6_310*RLcp6_111+OPcp6_36*RLcp6_19;
    ACcp6_311 = qdd[3]+OMcp6_110*ORcp6_211+OMcp6_16*ORcp6_29-OMcp6_210*ORcp6_111-OMcp6_26*ORcp6_19+OPcp6_110*RLcp6_211+
 OPcp6_16*RLcp6_29-OPcp6_210*RLcp6_111-OPcp6_26*RLcp6_19;
    OMcp6_112 = OMcp6_111+ROcp6_411*qd[12];
    OMcp6_212 = OMcp6_211+ROcp6_511*qd[12];
    OMcp6_312 = OMcp6_311+ROcp6_611*qd[12];
    OPcp6_112 = OPcp6_110+ROcp6_411*qdd[12]+ROcp6_710*qdd[11]+qd[11]*(OMcp6_210*ROcp6_910-OMcp6_310*ROcp6_810)+qd[12]*(
 OMcp6_211*ROcp6_611-OMcp6_311*ROcp6_511);
    OPcp6_212 = OPcp6_210+ROcp6_511*qdd[12]+ROcp6_810*qdd[11]-qd[11]*(OMcp6_110*ROcp6_910-OMcp6_310*ROcp6_710)-qd[12]*(
 OMcp6_111*ROcp6_611-OMcp6_311*ROcp6_411);
    OPcp6_312 = OPcp6_310+ROcp6_611*qdd[12]+ROcp6_910*qdd[11]+qd[11]*(OMcp6_110*ROcp6_810-OMcp6_210*ROcp6_710)+qd[12]*(
 OMcp6_111*ROcp6_511-OMcp6_211*ROcp6_411);

// = = Block_1_0_0_7_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp6_111;
    sens->P[2] = POcp6_211;
    sens->P[3] = POcp6_311;
    sens->R[1][1] = ROcp6_112;
    sens->R[1][2] = ROcp6_212;
    sens->R[1][3] = ROcp6_312;
    sens->R[2][1] = ROcp6_411;
    sens->R[2][2] = ROcp6_511;
    sens->R[2][3] = ROcp6_611;
    sens->R[3][1] = ROcp6_712;
    sens->R[3][2] = ROcp6_812;
    sens->R[3][3] = ROcp6_912;
    sens->V[1] = VIcp6_111;
    sens->V[2] = VIcp6_211;
    sens->V[3] = VIcp6_311;
    sens->OM[1] = OMcp6_112;
    sens->OM[2] = OMcp6_212;
    sens->OM[3] = OMcp6_312;
    sens->J[1][1] = (1.0);
    sens->J[1][4] = JTcp6_111_4;
    sens->J[1][5] = JTcp6_111_5;
    sens->J[1][6] = JTcp6_111_6;
    sens->J[1][9] = JTcp6_111_7;
    sens->J[1][10] = JTcp6_111_8;
    sens->J[2][2] = (1.0);
    sens->J[2][4] = JTcp6_211_4;
    sens->J[2][5] = JTcp6_211_5;
    sens->J[2][6] = JTcp6_211_6;
    sens->J[2][9] = JTcp6_211_7;
    sens->J[2][10] = JTcp6_211_8;
    sens->J[3][3] = (1.0);
    sens->J[3][5] = JTcp6_311_5;
    sens->J[3][6] = JTcp6_311_6;
    sens->J[3][9] = JTcp6_311_7;
    sens->J[3][10] = JTcp6_311_8;
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp6_15;
    sens->J[4][9] = ROcp6_46;
    sens->J[4][10] = ROcp6_19;
    sens->J[4][11] = ROcp6_710;
    sens->J[4][12] = ROcp6_411;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp6_25;
    sens->J[5][9] = ROcp6_56;
    sens->J[5][10] = ROcp6_29;
    sens->J[5][11] = ROcp6_810;
    sens->J[5][12] = ROcp6_511;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->J[6][9] = ROcp6_66;
    sens->J[6][10] = ROcp6_39;
    sens->J[6][11] = ROcp6_910;
    sens->J[6][12] = ROcp6_611;
    sens->A[1] = ACcp6_111;
    sens->A[2] = ACcp6_211;
    sens->A[3] = ACcp6_311;
    sens->OMP[1] = OPcp6_112;
    sens->OMP[2] = OPcp6_212;
    sens->OMP[3] = OPcp6_312;
 
// 
break;
case 8:
 


// = = Block_1_0_0_8_0_1 = = 
 
// Sensor Kinematics 


    ROcp7_15 = C4*C5;
    ROcp7_25 = S4*C5;
    ROcp7_75 = C4*S5;
    ROcp7_85 = S4*S5;
    ROcp7_46 = ROcp7_75*S6-S4*C6;
    ROcp7_56 = ROcp7_85*S6+C4*C6;
    ROcp7_66 = C5*S6;
    ROcp7_76 = ROcp7_75*C6+S4*S6;
    ROcp7_86 = ROcp7_85*C6-C4*S6;
    ROcp7_96 = C5*C6;
    OMcp7_15 = -qd[5]*S4;
    OMcp7_25 = qd[5]*C4;
    OMcp7_16 = OMcp7_15+ROcp7_15*qd[6];
    OMcp7_26 = OMcp7_25+ROcp7_25*qd[6];
    OMcp7_36 = qd[4]-qd[6]*S5;
    OPcp7_16 = ROcp7_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp7_25*S5+ROcp7_25*qd[4]);
    OPcp7_26 = ROcp7_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp7_15*S5+ROcp7_15*qd[4]);
    OPcp7_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_8_0_4 = = 
 
// Sensor Kinematics 


    ROcp7_19 = ROcp7_15*C9-ROcp7_76*S9;
    ROcp7_29 = ROcp7_25*C9-ROcp7_86*S9;
    ROcp7_39 = -(ROcp7_96*S9+S5*C9);
    ROcp7_79 = ROcp7_15*S9+ROcp7_76*C9;
    ROcp7_89 = ROcp7_25*S9+ROcp7_86*C9;
    ROcp7_99 = ROcp7_96*C9-S5*S9;
    ROcp7_410 = ROcp7_46*C10+ROcp7_79*S10;
    ROcp7_510 = ROcp7_56*C10+ROcp7_89*S10;
    ROcp7_610 = ROcp7_66*C10+ROcp7_99*S10;
    ROcp7_710 = -(ROcp7_46*S10-ROcp7_79*C10);
    ROcp7_810 = -(ROcp7_56*S10-ROcp7_89*C10);
    ROcp7_910 = -(ROcp7_66*S10-ROcp7_99*C10);
    ROcp7_111 = ROcp7_19*C11+ROcp7_410*S11;
    ROcp7_211 = ROcp7_29*C11+ROcp7_510*S11;
    ROcp7_311 = ROcp7_39*C11+ROcp7_610*S11;
    ROcp7_411 = -(ROcp7_19*S11-ROcp7_410*C11);
    ROcp7_511 = -(ROcp7_29*S11-ROcp7_510*C11);
    ROcp7_611 = -(ROcp7_39*S11-ROcp7_610*C11);
    ROcp7_112 = ROcp7_111*C12-ROcp7_710*S12;
    ROcp7_212 = ROcp7_211*C12-ROcp7_810*S12;
    ROcp7_312 = ROcp7_311*C12-ROcp7_910*S12;
    ROcp7_712 = ROcp7_111*S12+ROcp7_710*C12;
    ROcp7_812 = ROcp7_211*S12+ROcp7_810*C12;
    ROcp7_912 = ROcp7_311*S12+ROcp7_910*C12;
    ROcp7_113 = ROcp7_112*C13+ROcp7_411*S13;
    ROcp7_213 = ROcp7_212*C13+ROcp7_511*S13;
    ROcp7_313 = ROcp7_312*C13+ROcp7_611*S13;
    ROcp7_413 = -(ROcp7_112*S13-ROcp7_411*C13);
    ROcp7_513 = -(ROcp7_212*S13-ROcp7_511*C13);
    ROcp7_613 = -(ROcp7_312*S13-ROcp7_611*C13);
    RLcp7_19 = ROcp7_46*s->dpt[2][4]+ROcp7_76*s->dpt[3][4];
    RLcp7_29 = ROcp7_56*s->dpt[2][4]+ROcp7_86*s->dpt[3][4];
    RLcp7_39 = ROcp7_66*s->dpt[2][4]+ROcp7_96*s->dpt[3][4];
    OMcp7_19 = OMcp7_16+ROcp7_46*qd[9];
    OMcp7_29 = OMcp7_26+ROcp7_56*qd[9];
    OMcp7_39 = OMcp7_36+ROcp7_66*qd[9];
    ORcp7_19 = OMcp7_26*RLcp7_39-OMcp7_36*RLcp7_29;
    ORcp7_29 = -(OMcp7_16*RLcp7_39-OMcp7_36*RLcp7_19);
    ORcp7_39 = OMcp7_16*RLcp7_29-OMcp7_26*RLcp7_19;
    OMcp7_110 = OMcp7_19+ROcp7_19*qd[10];
    OMcp7_210 = OMcp7_29+ROcp7_29*qd[10];
    OMcp7_310 = OMcp7_39+ROcp7_39*qd[10];
    OPcp7_110 = OPcp7_16+ROcp7_19*qdd[10]+ROcp7_46*qdd[9]+qd[10]*(OMcp7_29*ROcp7_39-OMcp7_39*ROcp7_29)+qd[9]*(OMcp7_26*
 ROcp7_66-OMcp7_36*ROcp7_56);
    OPcp7_210 = OPcp7_26+ROcp7_29*qdd[10]+ROcp7_56*qdd[9]-qd[10]*(OMcp7_19*ROcp7_39-OMcp7_39*ROcp7_19)-qd[9]*(OMcp7_16*
 ROcp7_66-OMcp7_36*ROcp7_46);
    OPcp7_310 = OPcp7_36+ROcp7_39*qdd[10]+ROcp7_66*qdd[9]+qd[10]*(OMcp7_19*ROcp7_29-OMcp7_29*ROcp7_19)+qd[9]*(OMcp7_16*
 ROcp7_56-OMcp7_26*ROcp7_46);
    RLcp7_111 = ROcp7_710*s->dpt[3][11];
    RLcp7_211 = ROcp7_810*s->dpt[3][11];
    RLcp7_311 = ROcp7_910*s->dpt[3][11];
    OMcp7_111 = OMcp7_110+ROcp7_710*qd[11];
    OMcp7_211 = OMcp7_210+ROcp7_810*qd[11];
    OMcp7_311 = OMcp7_310+ROcp7_910*qd[11];
    ORcp7_111 = OMcp7_210*RLcp7_311-OMcp7_310*RLcp7_211;
    ORcp7_211 = -(OMcp7_110*RLcp7_311-OMcp7_310*RLcp7_111);
    ORcp7_311 = OMcp7_110*RLcp7_211-OMcp7_210*RLcp7_111;
    OMcp7_112 = OMcp7_111+ROcp7_411*qd[12];
    OMcp7_212 = OMcp7_211+ROcp7_511*qd[12];
    OMcp7_312 = OMcp7_311+ROcp7_611*qd[12];
    OPcp7_112 = OPcp7_110+ROcp7_411*qdd[12]+ROcp7_710*qdd[11]+qd[11]*(OMcp7_210*ROcp7_910-OMcp7_310*ROcp7_810)+qd[12]*(
 OMcp7_211*ROcp7_611-OMcp7_311*ROcp7_511);
    OPcp7_212 = OPcp7_210+ROcp7_511*qdd[12]+ROcp7_810*qdd[11]-qd[11]*(OMcp7_110*ROcp7_910-OMcp7_310*ROcp7_710)-qd[12]*(
 OMcp7_111*ROcp7_611-OMcp7_311*ROcp7_411);
    OPcp7_312 = OPcp7_310+ROcp7_611*qdd[12]+ROcp7_910*qdd[11]+qd[11]*(OMcp7_110*ROcp7_810-OMcp7_210*ROcp7_710)+qd[12]*(
 OMcp7_111*ROcp7_511-OMcp7_211*ROcp7_411);
    RLcp7_113 = ROcp7_712*s->dpt[3][14];
    RLcp7_213 = ROcp7_812*s->dpt[3][14];
    RLcp7_313 = ROcp7_912*s->dpt[3][14];
    POcp7_113 = RLcp7_111+RLcp7_113+RLcp7_19+q[1];
    POcp7_213 = RLcp7_211+RLcp7_213+RLcp7_29+q[2];
    POcp7_313 = RLcp7_311+RLcp7_313+RLcp7_39+q[3];
    JTcp7_113_4 = -(RLcp7_211+RLcp7_213+RLcp7_29);
    JTcp7_213_4 = RLcp7_111+RLcp7_113+RLcp7_19;
    JTcp7_113_5 = C4*(RLcp7_311+RLcp7_313+RLcp7_39);
    JTcp7_213_5 = S4*(RLcp7_311+RLcp7_313+RLcp7_39);
    JTcp7_313_5 = -(RLcp7_213*S4+C4*(RLcp7_111+RLcp7_113+RLcp7_19)+S4*(RLcp7_211+RLcp7_29));
    JTcp7_113_6 = RLcp7_213*S5+RLcp7_313*ROcp7_25+ROcp7_25*(RLcp7_311+RLcp7_39)+S5*(RLcp7_211+RLcp7_29);
    JTcp7_213_6 = -(RLcp7_113*S5+RLcp7_313*ROcp7_15+ROcp7_15*(RLcp7_311+RLcp7_39)+S5*(RLcp7_111+RLcp7_19));
    JTcp7_313_6 = ROcp7_15*(RLcp7_211+RLcp7_29)-ROcp7_25*(RLcp7_111+RLcp7_19)-RLcp7_113*ROcp7_25+RLcp7_213*ROcp7_15;
    JTcp7_113_7 = ROcp7_56*(RLcp7_311+RLcp7_313)-ROcp7_66*(RLcp7_211+RLcp7_213);
    JTcp7_213_7 = -(ROcp7_46*(RLcp7_311+RLcp7_313)-ROcp7_66*(RLcp7_111+RLcp7_113));
    JTcp7_313_7 = ROcp7_46*(RLcp7_211+RLcp7_213)-ROcp7_56*(RLcp7_111+RLcp7_113);
    JTcp7_113_8 = ROcp7_29*(RLcp7_311+RLcp7_313)-ROcp7_39*(RLcp7_211+RLcp7_213);
    JTcp7_213_8 = -(ROcp7_19*(RLcp7_311+RLcp7_313)-ROcp7_39*(RLcp7_111+RLcp7_113));
    JTcp7_313_8 = ROcp7_19*(RLcp7_211+RLcp7_213)-ROcp7_29*(RLcp7_111+RLcp7_113);
    JTcp7_113_9 = -(RLcp7_213*ROcp7_910-RLcp7_313*ROcp7_810);
    JTcp7_213_9 = RLcp7_113*ROcp7_910-RLcp7_313*ROcp7_710;
    JTcp7_313_9 = -(RLcp7_113*ROcp7_810-RLcp7_213*ROcp7_710);
    JTcp7_113_10 = -(RLcp7_213*ROcp7_611-RLcp7_313*ROcp7_511);
    JTcp7_213_10 = RLcp7_113*ROcp7_611-RLcp7_313*ROcp7_411;
    JTcp7_313_10 = -(RLcp7_113*ROcp7_511-RLcp7_213*ROcp7_411);
    OMcp7_113 = OMcp7_112+ROcp7_712*qd[13];
    OMcp7_213 = OMcp7_212+ROcp7_812*qd[13];
    OMcp7_313 = OMcp7_312+ROcp7_912*qd[13];
    ORcp7_113 = OMcp7_212*RLcp7_313-OMcp7_312*RLcp7_213;
    ORcp7_213 = -(OMcp7_112*RLcp7_313-OMcp7_312*RLcp7_113);
    ORcp7_313 = OMcp7_112*RLcp7_213-OMcp7_212*RLcp7_113;
    VIcp7_113 = ORcp7_111+ORcp7_113+ORcp7_19+qd[1];
    VIcp7_213 = ORcp7_211+ORcp7_213+ORcp7_29+qd[2];
    VIcp7_313 = ORcp7_311+ORcp7_313+ORcp7_39+qd[3];
    OPcp7_113 = OPcp7_112+ROcp7_712*qdd[13]+qd[13]*(OMcp7_212*ROcp7_912-OMcp7_312*ROcp7_812);
    OPcp7_213 = OPcp7_212+ROcp7_812*qdd[13]-qd[13]*(OMcp7_112*ROcp7_912-OMcp7_312*ROcp7_712);
    OPcp7_313 = OPcp7_312+ROcp7_912*qdd[13]+qd[13]*(OMcp7_112*ROcp7_812-OMcp7_212*ROcp7_712);
    ACcp7_113 = qdd[1]+OMcp7_210*ORcp7_311+OMcp7_212*ORcp7_313+OMcp7_26*ORcp7_39-OMcp7_310*ORcp7_211-OMcp7_312*ORcp7_213-
 OMcp7_36*ORcp7_29+OPcp7_210*RLcp7_311+OPcp7_212*RLcp7_313+OPcp7_26*RLcp7_39-OPcp7_310*RLcp7_211-OPcp7_312*RLcp7_213-OPcp7_36
 *RLcp7_29;
    ACcp7_213 = qdd[2]-OMcp7_110*ORcp7_311-OMcp7_112*ORcp7_313-OMcp7_16*ORcp7_39+OMcp7_310*ORcp7_111+OMcp7_312*ORcp7_113+
 OMcp7_36*ORcp7_19-OPcp7_110*RLcp7_311-OPcp7_112*RLcp7_313-OPcp7_16*RLcp7_39+OPcp7_310*RLcp7_111+OPcp7_312*RLcp7_113+OPcp7_36
 *RLcp7_19;
    ACcp7_313 = qdd[3]+OMcp7_110*ORcp7_211+OMcp7_112*ORcp7_213+OMcp7_16*ORcp7_29-OMcp7_210*ORcp7_111-OMcp7_212*ORcp7_113-
 OMcp7_26*ORcp7_19+OPcp7_110*RLcp7_211+OPcp7_112*RLcp7_213+OPcp7_16*RLcp7_29-OPcp7_210*RLcp7_111-OPcp7_212*RLcp7_113-OPcp7_26
 *RLcp7_19;

// = = Block_1_0_0_8_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp7_113;
    sens->P[2] = POcp7_213;
    sens->P[3] = POcp7_313;
    sens->R[1][1] = ROcp7_113;
    sens->R[1][2] = ROcp7_213;
    sens->R[1][3] = ROcp7_313;
    sens->R[2][1] = ROcp7_413;
    sens->R[2][2] = ROcp7_513;
    sens->R[2][3] = ROcp7_613;
    sens->R[3][1] = ROcp7_712;
    sens->R[3][2] = ROcp7_812;
    sens->R[3][3] = ROcp7_912;
    sens->V[1] = VIcp7_113;
    sens->V[2] = VIcp7_213;
    sens->V[3] = VIcp7_313;
    sens->OM[1] = OMcp7_113;
    sens->OM[2] = OMcp7_213;
    sens->OM[3] = OMcp7_313;
    sens->J[1][1] = (1.0);
    sens->J[1][4] = JTcp7_113_4;
    sens->J[1][5] = JTcp7_113_5;
    sens->J[1][6] = JTcp7_113_6;
    sens->J[1][9] = JTcp7_113_7;
    sens->J[1][10] = JTcp7_113_8;
    sens->J[1][11] = JTcp7_113_9;
    sens->J[1][12] = JTcp7_113_10;
    sens->J[2][2] = (1.0);
    sens->J[2][4] = JTcp7_213_4;
    sens->J[2][5] = JTcp7_213_5;
    sens->J[2][6] = JTcp7_213_6;
    sens->J[2][9] = JTcp7_213_7;
    sens->J[2][10] = JTcp7_213_8;
    sens->J[2][11] = JTcp7_213_9;
    sens->J[2][12] = JTcp7_213_10;
    sens->J[3][3] = (1.0);
    sens->J[3][5] = JTcp7_313_5;
    sens->J[3][6] = JTcp7_313_6;
    sens->J[3][9] = JTcp7_313_7;
    sens->J[3][10] = JTcp7_313_8;
    sens->J[3][11] = JTcp7_313_9;
    sens->J[3][12] = JTcp7_313_10;
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp7_15;
    sens->J[4][9] = ROcp7_46;
    sens->J[4][10] = ROcp7_19;
    sens->J[4][11] = ROcp7_710;
    sens->J[4][12] = ROcp7_411;
    sens->J[4][13] = ROcp7_712;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp7_25;
    sens->J[5][9] = ROcp7_56;
    sens->J[5][10] = ROcp7_29;
    sens->J[5][11] = ROcp7_810;
    sens->J[5][12] = ROcp7_511;
    sens->J[5][13] = ROcp7_812;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->J[6][9] = ROcp7_66;
    sens->J[6][10] = ROcp7_39;
    sens->J[6][11] = ROcp7_910;
    sens->J[6][12] = ROcp7_611;
    sens->J[6][13] = ROcp7_912;
    sens->A[1] = ACcp7_113;
    sens->A[2] = ACcp7_213;
    sens->A[3] = ACcp7_313;
    sens->OMP[1] = OPcp7_113;
    sens->OMP[2] = OPcp7_213;
    sens->OMP[3] = OPcp7_313;
 
// 
break;
case 9:
 


// = = Block_1_0_0_9_0_1 = = 
 
// Sensor Kinematics 


    ROcp8_15 = C4*C5;
    ROcp8_25 = S4*C5;
    ROcp8_75 = C4*S5;
    ROcp8_85 = S4*S5;
    ROcp8_46 = ROcp8_75*S6-S4*C6;
    ROcp8_56 = ROcp8_85*S6+C4*C6;
    ROcp8_66 = C5*S6;
    ROcp8_76 = ROcp8_75*C6+S4*S6;
    ROcp8_86 = ROcp8_85*C6-C4*S6;
    ROcp8_96 = C5*C6;
    OMcp8_15 = -qd[5]*S4;
    OMcp8_25 = qd[5]*C4;
    OMcp8_16 = OMcp8_15+ROcp8_15*qd[6];
    OMcp8_26 = OMcp8_25+ROcp8_25*qd[6];
    OMcp8_36 = qd[4]-qd[6]*S5;
    OPcp8_16 = ROcp8_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp8_25*S5+ROcp8_25*qd[4]);
    OPcp8_26 = ROcp8_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp8_15*S5+ROcp8_15*qd[4]);
    OPcp8_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_9_0_5 = = 
 
// Sensor Kinematics 


    ROcp8_114 = ROcp8_15*C14+ROcp8_46*S14;
    ROcp8_214 = ROcp8_25*C14+ROcp8_56*S14;
    ROcp8_314 = ROcp8_66*S14-C14*S5;
    ROcp8_414 = -(ROcp8_15*S14-ROcp8_46*C14);
    ROcp8_514 = -(ROcp8_25*S14-ROcp8_56*C14);
    ROcp8_614 = ROcp8_66*C14+S14*S5;
    RLcp8_114 = ROcp8_76*s->dpt[3][5];
    RLcp8_214 = ROcp8_86*s->dpt[3][5];
    RLcp8_314 = ROcp8_96*s->dpt[3][5];
    POcp8_114 = RLcp8_114+q[1];
    POcp8_214 = RLcp8_214+q[2];
    POcp8_314 = RLcp8_314+q[3];
    JTcp8_114_5 = RLcp8_314*C4;
    JTcp8_214_5 = RLcp8_314*S4;
    JTcp8_314_5 = -(RLcp8_114*C4+RLcp8_214*S4);
    JTcp8_114_6 = RLcp8_214*S5+RLcp8_314*ROcp8_25;
    JTcp8_214_6 = -(RLcp8_114*S5+RLcp8_314*ROcp8_15);
    JTcp8_314_6 = -(RLcp8_114*ROcp8_25-RLcp8_214*ROcp8_15);
    OMcp8_114 = OMcp8_16+ROcp8_76*qd[14];
    OMcp8_214 = OMcp8_26+ROcp8_86*qd[14];
    OMcp8_314 = OMcp8_36+ROcp8_96*qd[14];
    ORcp8_114 = OMcp8_26*RLcp8_314-OMcp8_36*RLcp8_214;
    ORcp8_214 = -(OMcp8_16*RLcp8_314-OMcp8_36*RLcp8_114);
    ORcp8_314 = OMcp8_16*RLcp8_214-OMcp8_26*RLcp8_114;
    VIcp8_114 = ORcp8_114+qd[1];
    VIcp8_214 = ORcp8_214+qd[2];
    VIcp8_314 = ORcp8_314+qd[3];
    OPcp8_114 = OPcp8_16+ROcp8_76*qdd[14]+qd[14]*(OMcp8_26*ROcp8_96-OMcp8_36*ROcp8_86);
    OPcp8_214 = OPcp8_26+ROcp8_86*qdd[14]-qd[14]*(OMcp8_16*ROcp8_96-OMcp8_36*ROcp8_76);
    OPcp8_314 = OPcp8_36+ROcp8_96*qdd[14]+qd[14]*(OMcp8_16*ROcp8_86-OMcp8_26*ROcp8_76);
    ACcp8_114 = qdd[1]+OMcp8_26*ORcp8_314-OMcp8_36*ORcp8_214+OPcp8_26*RLcp8_314-OPcp8_36*RLcp8_214;
    ACcp8_214 = qdd[2]-OMcp8_16*ORcp8_314+OMcp8_36*ORcp8_114-OPcp8_16*RLcp8_314+OPcp8_36*RLcp8_114;
    ACcp8_314 = qdd[3]+OMcp8_16*ORcp8_214-OMcp8_26*ORcp8_114+OPcp8_16*RLcp8_214-OPcp8_26*RLcp8_114;

// = = Block_1_0_0_9_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp8_114;
    sens->P[2] = POcp8_214;
    sens->P[3] = POcp8_314;
    sens->R[1][1] = ROcp8_114;
    sens->R[1][2] = ROcp8_214;
    sens->R[1][3] = ROcp8_314;
    sens->R[2][1] = ROcp8_414;
    sens->R[2][2] = ROcp8_514;
    sens->R[2][3] = ROcp8_614;
    sens->R[3][1] = ROcp8_76;
    sens->R[3][2] = ROcp8_86;
    sens->R[3][3] = ROcp8_96;
    sens->V[1] = VIcp8_114;
    sens->V[2] = VIcp8_214;
    sens->V[3] = VIcp8_314;
    sens->OM[1] = OMcp8_114;
    sens->OM[2] = OMcp8_214;
    sens->OM[3] = OMcp8_314;
    sens->J[1][1] = (1.0);
    sens->J[1][4] = -RLcp8_214;
    sens->J[1][5] = JTcp8_114_5;
    sens->J[1][6] = JTcp8_114_6;
    sens->J[2][2] = (1.0);
    sens->J[2][4] = RLcp8_114;
    sens->J[2][5] = JTcp8_214_5;
    sens->J[2][6] = JTcp8_214_6;
    sens->J[3][3] = (1.0);
    sens->J[3][5] = JTcp8_314_5;
    sens->J[3][6] = JTcp8_314_6;
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp8_15;
    sens->J[4][14] = ROcp8_76;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp8_25;
    sens->J[5][14] = ROcp8_86;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->J[6][14] = ROcp8_96;
    sens->A[1] = ACcp8_114;
    sens->A[2] = ACcp8_214;
    sens->A[3] = ACcp8_314;
    sens->OMP[1] = OPcp8_114;
    sens->OMP[2] = OPcp8_214;
    sens->OMP[3] = OPcp8_314;
 
// 
break;
case 10:
 


// = = Block_1_0_0_10_0_1 = = 
 
// Sensor Kinematics 


    ROcp9_15 = C4*C5;
    ROcp9_25 = S4*C5;
    ROcp9_75 = C4*S5;
    ROcp9_85 = S4*S5;
    ROcp9_46 = ROcp9_75*S6-S4*C6;
    ROcp9_56 = ROcp9_85*S6+C4*C6;
    ROcp9_66 = C5*S6;
    ROcp9_76 = ROcp9_75*C6+S4*S6;
    ROcp9_86 = ROcp9_85*C6-C4*S6;
    ROcp9_96 = C5*C6;
    OMcp9_15 = -qd[5]*S4;
    OMcp9_25 = qd[5]*C4;
    OMcp9_16 = OMcp9_15+ROcp9_15*qd[6];
    OMcp9_26 = OMcp9_25+ROcp9_25*qd[6];
    OMcp9_36 = qd[4]-qd[6]*S5;
    OPcp9_16 = ROcp9_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp9_25*S5+ROcp9_25*qd[4]);
    OPcp9_26 = ROcp9_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp9_15*S5+ROcp9_15*qd[4]);
    OPcp9_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_10_0_6 = = 
 
// Sensor Kinematics 


    ROcp9_116 = ROcp9_15*C16-ROcp9_76*S16;
    ROcp9_216 = ROcp9_25*C16-ROcp9_86*S16;
    ROcp9_316 = -(ROcp9_96*S16+C16*S5);
    ROcp9_716 = ROcp9_15*S16+ROcp9_76*C16;
    ROcp9_816 = ROcp9_25*S16+ROcp9_86*C16;
    ROcp9_916 = ROcp9_96*C16-S16*S5;
    RLcp9_116 = ROcp9_46*s->dpt[2][6]+ROcp9_76*s->dpt[3][6];
    RLcp9_216 = ROcp9_56*s->dpt[2][6]+ROcp9_86*s->dpt[3][6];
    RLcp9_316 = ROcp9_66*s->dpt[2][6]+ROcp9_96*s->dpt[3][6];
    POcp9_116 = RLcp9_116+q[1];
    POcp9_216 = RLcp9_216+q[2];
    POcp9_316 = RLcp9_316+q[3];
    JTcp9_116_5 = RLcp9_316*C4;
    JTcp9_216_5 = RLcp9_316*S4;
    JTcp9_316_5 = -(RLcp9_116*C4+RLcp9_216*S4);
    JTcp9_116_6 = RLcp9_216*S5+RLcp9_316*ROcp9_25;
    JTcp9_216_6 = -(RLcp9_116*S5+RLcp9_316*ROcp9_15);
    JTcp9_316_6 = -(RLcp9_116*ROcp9_25-RLcp9_216*ROcp9_15);
    OMcp9_116 = OMcp9_16+ROcp9_46*qd[16];
    OMcp9_216 = OMcp9_26+ROcp9_56*qd[16];
    OMcp9_316 = OMcp9_36+ROcp9_66*qd[16];
    ORcp9_116 = OMcp9_26*RLcp9_316-OMcp9_36*RLcp9_216;
    ORcp9_216 = -(OMcp9_16*RLcp9_316-OMcp9_36*RLcp9_116);
    ORcp9_316 = OMcp9_16*RLcp9_216-OMcp9_26*RLcp9_116;
    VIcp9_116 = ORcp9_116+qd[1];
    VIcp9_216 = ORcp9_216+qd[2];
    VIcp9_316 = ORcp9_316+qd[3];
    OPcp9_116 = OPcp9_16+ROcp9_46*qdd[16]+qd[16]*(OMcp9_26*ROcp9_66-OMcp9_36*ROcp9_56);
    OPcp9_216 = OPcp9_26+ROcp9_56*qdd[16]-qd[16]*(OMcp9_16*ROcp9_66-OMcp9_36*ROcp9_46);
    OPcp9_316 = OPcp9_36+ROcp9_66*qdd[16]+qd[16]*(OMcp9_16*ROcp9_56-OMcp9_26*ROcp9_46);
    ACcp9_116 = qdd[1]+OMcp9_26*ORcp9_316-OMcp9_36*ORcp9_216+OPcp9_26*RLcp9_316-OPcp9_36*RLcp9_216;
    ACcp9_216 = qdd[2]-OMcp9_16*ORcp9_316+OMcp9_36*ORcp9_116-OPcp9_16*RLcp9_316+OPcp9_36*RLcp9_116;
    ACcp9_316 = qdd[3]+OMcp9_16*ORcp9_216-OMcp9_26*ORcp9_116+OPcp9_16*RLcp9_216-OPcp9_26*RLcp9_116;

// = = Block_1_0_0_10_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp9_116;
    sens->P[2] = POcp9_216;
    sens->P[3] = POcp9_316;
    sens->R[1][1] = ROcp9_116;
    sens->R[1][2] = ROcp9_216;
    sens->R[1][3] = ROcp9_316;
    sens->R[2][1] = ROcp9_46;
    sens->R[2][2] = ROcp9_56;
    sens->R[2][3] = ROcp9_66;
    sens->R[3][1] = ROcp9_716;
    sens->R[3][2] = ROcp9_816;
    sens->R[3][3] = ROcp9_916;
    sens->V[1] = VIcp9_116;
    sens->V[2] = VIcp9_216;
    sens->V[3] = VIcp9_316;
    sens->OM[1] = OMcp9_116;
    sens->OM[2] = OMcp9_216;
    sens->OM[3] = OMcp9_316;
    sens->J[1][1] = (1.0);
    sens->J[1][4] = -RLcp9_216;
    sens->J[1][5] = JTcp9_116_5;
    sens->J[1][6] = JTcp9_116_6;
    sens->J[2][2] = (1.0);
    sens->J[2][4] = RLcp9_116;
    sens->J[2][5] = JTcp9_216_5;
    sens->J[2][6] = JTcp9_216_6;
    sens->J[3][3] = (1.0);
    sens->J[3][5] = JTcp9_316_5;
    sens->J[3][6] = JTcp9_316_6;
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp9_15;
    sens->J[4][16] = ROcp9_46;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp9_25;
    sens->J[5][16] = ROcp9_56;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->J[6][16] = ROcp9_66;
    sens->A[1] = ACcp9_116;
    sens->A[2] = ACcp9_216;
    sens->A[3] = ACcp9_316;
    sens->OMP[1] = OPcp9_116;
    sens->OMP[2] = OPcp9_216;
    sens->OMP[3] = OPcp9_316;
 
// 
break;
case 11:
 


// = = Block_1_0_0_11_0_1 = = 
 
// Sensor Kinematics 


    ROcp10_15 = C4*C5;
    ROcp10_25 = S4*C5;
    ROcp10_75 = C4*S5;
    ROcp10_85 = S4*S5;
    ROcp10_46 = ROcp10_75*S6-S4*C6;
    ROcp10_56 = ROcp10_85*S6+C4*C6;
    ROcp10_66 = C5*S6;
    ROcp10_76 = ROcp10_75*C6+S4*S6;
    ROcp10_86 = ROcp10_85*C6-C4*S6;
    ROcp10_96 = C5*C6;
    OMcp10_15 = -qd[5]*S4;
    OMcp10_25 = qd[5]*C4;
    OMcp10_16 = OMcp10_15+ROcp10_15*qd[6];
    OMcp10_26 = OMcp10_25+ROcp10_25*qd[6];
    OMcp10_36 = qd[4]-qd[6]*S5;
    OPcp10_16 = ROcp10_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp10_25*S5+ROcp10_25*qd[4]);
    OPcp10_26 = ROcp10_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp10_15*S5+ROcp10_15*qd[4]);
    OPcp10_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_11_0_6 = = 
 
// Sensor Kinematics 


    ROcp10_116 = ROcp10_15*C16-ROcp10_76*S16;
    ROcp10_216 = ROcp10_25*C16-ROcp10_86*S16;
    ROcp10_316 = -(ROcp10_96*S16+C16*S5);
    ROcp10_716 = ROcp10_15*S16+ROcp10_76*C16;
    ROcp10_816 = ROcp10_25*S16+ROcp10_86*C16;
    ROcp10_916 = ROcp10_96*C16-S16*S5;
    ROcp10_417 = ROcp10_46*C17+ROcp10_716*S17;
    ROcp10_517 = ROcp10_56*C17+ROcp10_816*S17;
    ROcp10_617 = ROcp10_66*C17+ROcp10_916*S17;
    ROcp10_717 = -(ROcp10_46*S17-ROcp10_716*C17);
    ROcp10_817 = -(ROcp10_56*S17-ROcp10_816*C17);
    ROcp10_917 = -(ROcp10_66*S17-ROcp10_916*C17);
    RLcp10_116 = ROcp10_46*s->dpt[2][6]+ROcp10_76*s->dpt[3][6];
    RLcp10_216 = ROcp10_56*s->dpt[2][6]+ROcp10_86*s->dpt[3][6];
    RLcp10_316 = ROcp10_66*s->dpt[2][6]+ROcp10_96*s->dpt[3][6];
    POcp10_116 = RLcp10_116+q[1];
    POcp10_216 = RLcp10_216+q[2];
    POcp10_316 = RLcp10_316+q[3];
    JTcp10_116_5 = RLcp10_316*C4;
    JTcp10_216_5 = RLcp10_316*S4;
    JTcp10_316_5 = -(RLcp10_116*C4+RLcp10_216*S4);
    JTcp10_116_6 = RLcp10_216*S5+RLcp10_316*ROcp10_25;
    JTcp10_216_6 = -(RLcp10_116*S5+RLcp10_316*ROcp10_15);
    JTcp10_316_6 = -(RLcp10_116*ROcp10_25-RLcp10_216*ROcp10_15);
    OMcp10_116 = OMcp10_16+ROcp10_46*qd[16];
    OMcp10_216 = OMcp10_26+ROcp10_56*qd[16];
    OMcp10_316 = OMcp10_36+ROcp10_66*qd[16];
    ORcp10_116 = OMcp10_26*RLcp10_316-OMcp10_36*RLcp10_216;
    ORcp10_216 = -(OMcp10_16*RLcp10_316-OMcp10_36*RLcp10_116);
    ORcp10_316 = OMcp10_16*RLcp10_216-OMcp10_26*RLcp10_116;
    VIcp10_116 = ORcp10_116+qd[1];
    VIcp10_216 = ORcp10_216+qd[2];
    VIcp10_316 = ORcp10_316+qd[3];
    ACcp10_116 = qdd[1]+OMcp10_26*ORcp10_316-OMcp10_36*ORcp10_216+OPcp10_26*RLcp10_316-OPcp10_36*RLcp10_216;
    ACcp10_216 = qdd[2]-OMcp10_16*ORcp10_316+OMcp10_36*ORcp10_116-OPcp10_16*RLcp10_316+OPcp10_36*RLcp10_116;
    ACcp10_316 = qdd[3]+OMcp10_16*ORcp10_216-OMcp10_26*ORcp10_116+OPcp10_16*RLcp10_216-OPcp10_26*RLcp10_116;
    OMcp10_117 = OMcp10_116+ROcp10_116*qd[17];
    OMcp10_217 = OMcp10_216+ROcp10_216*qd[17];
    OMcp10_317 = OMcp10_316+ROcp10_316*qd[17];
    OPcp10_117 = OPcp10_16+ROcp10_116*qdd[17]+ROcp10_46*qdd[16]+qd[16]*(OMcp10_26*ROcp10_66-OMcp10_36*ROcp10_56)+qd[17]*(
 OMcp10_216*ROcp10_316-OMcp10_316*ROcp10_216);
    OPcp10_217 = OPcp10_26+ROcp10_216*qdd[17]+ROcp10_56*qdd[16]-qd[16]*(OMcp10_16*ROcp10_66-OMcp10_36*ROcp10_46)-qd[17]*(
 OMcp10_116*ROcp10_316-OMcp10_316*ROcp10_116);
    OPcp10_317 = OPcp10_36+ROcp10_316*qdd[17]+ROcp10_66*qdd[16]+qd[16]*(OMcp10_16*ROcp10_56-OMcp10_26*ROcp10_46)+qd[17]*(
 OMcp10_116*ROcp10_216-OMcp10_216*ROcp10_116);

// = = Block_1_0_0_11_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp10_116;
    sens->P[2] = POcp10_216;
    sens->P[3] = POcp10_316;
    sens->R[1][1] = ROcp10_116;
    sens->R[1][2] = ROcp10_216;
    sens->R[1][3] = ROcp10_316;
    sens->R[2][1] = ROcp10_417;
    sens->R[2][2] = ROcp10_517;
    sens->R[2][3] = ROcp10_617;
    sens->R[3][1] = ROcp10_717;
    sens->R[3][2] = ROcp10_817;
    sens->R[3][3] = ROcp10_917;
    sens->V[1] = VIcp10_116;
    sens->V[2] = VIcp10_216;
    sens->V[3] = VIcp10_316;
    sens->OM[1] = OMcp10_117;
    sens->OM[2] = OMcp10_217;
    sens->OM[3] = OMcp10_317;
    sens->J[1][1] = (1.0);
    sens->J[1][4] = -RLcp10_216;
    sens->J[1][5] = JTcp10_116_5;
    sens->J[1][6] = JTcp10_116_6;
    sens->J[2][2] = (1.0);
    sens->J[2][4] = RLcp10_116;
    sens->J[2][5] = JTcp10_216_5;
    sens->J[2][6] = JTcp10_216_6;
    sens->J[3][3] = (1.0);
    sens->J[3][5] = JTcp10_316_5;
    sens->J[3][6] = JTcp10_316_6;
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp10_15;
    sens->J[4][16] = ROcp10_46;
    sens->J[4][17] = ROcp10_116;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp10_25;
    sens->J[5][16] = ROcp10_56;
    sens->J[5][17] = ROcp10_216;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->J[6][16] = ROcp10_66;
    sens->J[6][17] = ROcp10_316;
    sens->A[1] = ACcp10_116;
    sens->A[2] = ACcp10_216;
    sens->A[3] = ACcp10_316;
    sens->OMP[1] = OPcp10_117;
    sens->OMP[2] = OPcp10_217;
    sens->OMP[3] = OPcp10_317;
 
// 
break;
case 12:
 


// = = Block_1_0_0_12_0_1 = = 
 
// Sensor Kinematics 


    ROcp11_15 = C4*C5;
    ROcp11_25 = S4*C5;
    ROcp11_75 = C4*S5;
    ROcp11_85 = S4*S5;
    ROcp11_46 = ROcp11_75*S6-S4*C6;
    ROcp11_56 = ROcp11_85*S6+C4*C6;
    ROcp11_66 = C5*S6;
    ROcp11_76 = ROcp11_75*C6+S4*S6;
    ROcp11_86 = ROcp11_85*C6-C4*S6;
    ROcp11_96 = C5*C6;
    OMcp11_15 = -qd[5]*S4;
    OMcp11_25 = qd[5]*C4;
    OMcp11_16 = OMcp11_15+ROcp11_15*qd[6];
    OMcp11_26 = OMcp11_25+ROcp11_25*qd[6];
    OMcp11_36 = qd[4]-qd[6]*S5;
    OPcp11_16 = ROcp11_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp11_25*S5+ROcp11_25*qd[4]);
    OPcp11_26 = ROcp11_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp11_15*S5+ROcp11_15*qd[4]);
    OPcp11_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_12_0_6 = = 
 
// Sensor Kinematics 


    ROcp11_116 = ROcp11_15*C16-ROcp11_76*S16;
    ROcp11_216 = ROcp11_25*C16-ROcp11_86*S16;
    ROcp11_316 = -(ROcp11_96*S16+C16*S5);
    ROcp11_716 = ROcp11_15*S16+ROcp11_76*C16;
    ROcp11_816 = ROcp11_25*S16+ROcp11_86*C16;
    ROcp11_916 = ROcp11_96*C16-S16*S5;
    ROcp11_417 = ROcp11_46*C17+ROcp11_716*S17;
    ROcp11_517 = ROcp11_56*C17+ROcp11_816*S17;
    ROcp11_617 = ROcp11_66*C17+ROcp11_916*S17;
    ROcp11_717 = -(ROcp11_46*S17-ROcp11_716*C17);
    ROcp11_817 = -(ROcp11_56*S17-ROcp11_816*C17);
    ROcp11_917 = -(ROcp11_66*S17-ROcp11_916*C17);
    ROcp11_118 = ROcp11_116*C18+ROcp11_417*S18;
    ROcp11_218 = ROcp11_216*C18+ROcp11_517*S18;
    ROcp11_318 = ROcp11_316*C18+ROcp11_617*S18;
    ROcp11_418 = -(ROcp11_116*S18-ROcp11_417*C18);
    ROcp11_518 = -(ROcp11_216*S18-ROcp11_517*C18);
    ROcp11_618 = -(ROcp11_316*S18-ROcp11_617*C18);
    RLcp11_116 = ROcp11_46*s->dpt[2][6]+ROcp11_76*s->dpt[3][6];
    RLcp11_216 = ROcp11_56*s->dpt[2][6]+ROcp11_86*s->dpt[3][6];
    RLcp11_316 = ROcp11_66*s->dpt[2][6]+ROcp11_96*s->dpt[3][6];
    OMcp11_116 = OMcp11_16+ROcp11_46*qd[16];
    OMcp11_216 = OMcp11_26+ROcp11_56*qd[16];
    OMcp11_316 = OMcp11_36+ROcp11_66*qd[16];
    ORcp11_116 = OMcp11_26*RLcp11_316-OMcp11_36*RLcp11_216;
    ORcp11_216 = -(OMcp11_16*RLcp11_316-OMcp11_36*RLcp11_116);
    ORcp11_316 = OMcp11_16*RLcp11_216-OMcp11_26*RLcp11_116;
    OMcp11_117 = OMcp11_116+ROcp11_116*qd[17];
    OMcp11_217 = OMcp11_216+ROcp11_216*qd[17];
    OMcp11_317 = OMcp11_316+ROcp11_316*qd[17];
    OPcp11_117 = OPcp11_16+ROcp11_116*qdd[17]+ROcp11_46*qdd[16]+qd[16]*(OMcp11_26*ROcp11_66-OMcp11_36*ROcp11_56)+qd[17]*(
 OMcp11_216*ROcp11_316-OMcp11_316*ROcp11_216);
    OPcp11_217 = OPcp11_26+ROcp11_216*qdd[17]+ROcp11_56*qdd[16]-qd[16]*(OMcp11_16*ROcp11_66-OMcp11_36*ROcp11_46)-qd[17]*(
 OMcp11_116*ROcp11_316-OMcp11_316*ROcp11_116);
    OPcp11_317 = OPcp11_36+ROcp11_316*qdd[17]+ROcp11_66*qdd[16]+qd[16]*(OMcp11_16*ROcp11_56-OMcp11_26*ROcp11_46)+qd[17]*(
 OMcp11_116*ROcp11_216-OMcp11_216*ROcp11_116);
    RLcp11_118 = ROcp11_717*s->dpt[3][19];
    RLcp11_218 = ROcp11_817*s->dpt[3][19];
    RLcp11_318 = ROcp11_917*s->dpt[3][19];
    POcp11_118 = RLcp11_116+RLcp11_118+q[1];
    POcp11_218 = RLcp11_216+RLcp11_218+q[2];
    POcp11_318 = RLcp11_316+RLcp11_318+q[3];
    JTcp11_118_4 = -(RLcp11_216+RLcp11_218);
    JTcp11_218_4 = RLcp11_116+RLcp11_118;
    JTcp11_118_5 = C4*(RLcp11_316+RLcp11_318);
    JTcp11_218_5 = S4*(RLcp11_316+RLcp11_318);
    JTcp11_318_5 = -(C4*(RLcp11_116+RLcp11_118)+S4*(RLcp11_216+RLcp11_218));
    JTcp11_118_6 = ROcp11_25*(RLcp11_316+RLcp11_318)+S5*(RLcp11_216+RLcp11_218);
    JTcp11_218_6 = -(ROcp11_15*(RLcp11_316+RLcp11_318)+S5*(RLcp11_116+RLcp11_118));
    JTcp11_318_6 = ROcp11_15*(RLcp11_216+RLcp11_218)-ROcp11_25*(RLcp11_116+RLcp11_118);
    JTcp11_118_7 = -(RLcp11_218*ROcp11_66-RLcp11_318*ROcp11_56);
    JTcp11_218_7 = RLcp11_118*ROcp11_66-RLcp11_318*ROcp11_46;
    JTcp11_318_7 = -(RLcp11_118*ROcp11_56-RLcp11_218*ROcp11_46);
    JTcp11_118_8 = -(RLcp11_218*ROcp11_316-RLcp11_318*ROcp11_216);
    JTcp11_218_8 = RLcp11_118*ROcp11_316-RLcp11_318*ROcp11_116;
    JTcp11_318_8 = -(RLcp11_118*ROcp11_216-RLcp11_218*ROcp11_116);
    OMcp11_118 = OMcp11_117+ROcp11_717*qd[18];
    OMcp11_218 = OMcp11_217+ROcp11_817*qd[18];
    OMcp11_318 = OMcp11_317+ROcp11_917*qd[18];
    ORcp11_118 = OMcp11_217*RLcp11_318-OMcp11_317*RLcp11_218;
    ORcp11_218 = -(OMcp11_117*RLcp11_318-OMcp11_317*RLcp11_118);
    ORcp11_318 = OMcp11_117*RLcp11_218-OMcp11_217*RLcp11_118;
    VIcp11_118 = ORcp11_116+ORcp11_118+qd[1];
    VIcp11_218 = ORcp11_216+ORcp11_218+qd[2];
    VIcp11_318 = ORcp11_316+ORcp11_318+qd[3];
    OPcp11_118 = OPcp11_117+ROcp11_717*qdd[18]+qd[18]*(OMcp11_217*ROcp11_917-OMcp11_317*ROcp11_817);
    OPcp11_218 = OPcp11_217+ROcp11_817*qdd[18]-qd[18]*(OMcp11_117*ROcp11_917-OMcp11_317*ROcp11_717);
    OPcp11_318 = OPcp11_317+ROcp11_917*qdd[18]+qd[18]*(OMcp11_117*ROcp11_817-OMcp11_217*ROcp11_717);
    ACcp11_118 = qdd[1]+OMcp11_217*ORcp11_318+OMcp11_26*ORcp11_316-OMcp11_317*ORcp11_218-OMcp11_36*ORcp11_216+OPcp11_217*
 RLcp11_318+OPcp11_26*RLcp11_316-OPcp11_317*RLcp11_218-OPcp11_36*RLcp11_216;
    ACcp11_218 = qdd[2]-OMcp11_117*ORcp11_318-OMcp11_16*ORcp11_316+OMcp11_317*ORcp11_118+OMcp11_36*ORcp11_116-OPcp11_117*
 RLcp11_318-OPcp11_16*RLcp11_316+OPcp11_317*RLcp11_118+OPcp11_36*RLcp11_116;
    ACcp11_318 = qdd[3]+OMcp11_117*ORcp11_218+OMcp11_16*ORcp11_216-OMcp11_217*ORcp11_118-OMcp11_26*ORcp11_116+OPcp11_117*
 RLcp11_218+OPcp11_16*RLcp11_216-OPcp11_217*RLcp11_118-OPcp11_26*RLcp11_116;

// = = Block_1_0_0_12_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp11_118;
    sens->P[2] = POcp11_218;
    sens->P[3] = POcp11_318;
    sens->R[1][1] = ROcp11_118;
    sens->R[1][2] = ROcp11_218;
    sens->R[1][3] = ROcp11_318;
    sens->R[2][1] = ROcp11_418;
    sens->R[2][2] = ROcp11_518;
    sens->R[2][3] = ROcp11_618;
    sens->R[3][1] = ROcp11_717;
    sens->R[3][2] = ROcp11_817;
    sens->R[3][3] = ROcp11_917;
    sens->V[1] = VIcp11_118;
    sens->V[2] = VIcp11_218;
    sens->V[3] = VIcp11_318;
    sens->OM[1] = OMcp11_118;
    sens->OM[2] = OMcp11_218;
    sens->OM[3] = OMcp11_318;
    sens->J[1][1] = (1.0);
    sens->J[1][4] = JTcp11_118_4;
    sens->J[1][5] = JTcp11_118_5;
    sens->J[1][6] = JTcp11_118_6;
    sens->J[1][16] = JTcp11_118_7;
    sens->J[1][17] = JTcp11_118_8;
    sens->J[2][2] = (1.0);
    sens->J[2][4] = JTcp11_218_4;
    sens->J[2][5] = JTcp11_218_5;
    sens->J[2][6] = JTcp11_218_6;
    sens->J[2][16] = JTcp11_218_7;
    sens->J[2][17] = JTcp11_218_8;
    sens->J[3][3] = (1.0);
    sens->J[3][5] = JTcp11_318_5;
    sens->J[3][6] = JTcp11_318_6;
    sens->J[3][16] = JTcp11_318_7;
    sens->J[3][17] = JTcp11_318_8;
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp11_15;
    sens->J[4][16] = ROcp11_46;
    sens->J[4][17] = ROcp11_116;
    sens->J[4][18] = ROcp11_717;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp11_25;
    sens->J[5][16] = ROcp11_56;
    sens->J[5][17] = ROcp11_216;
    sens->J[5][18] = ROcp11_817;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->J[6][16] = ROcp11_66;
    sens->J[6][17] = ROcp11_316;
    sens->J[6][18] = ROcp11_917;
    sens->A[1] = ACcp11_118;
    sens->A[2] = ACcp11_218;
    sens->A[3] = ACcp11_318;
    sens->OMP[1] = OPcp11_118;
    sens->OMP[2] = OPcp11_218;
    sens->OMP[3] = OPcp11_318;
 
// 
break;
case 13:
 


// = = Block_1_0_0_13_0_1 = = 
 
// Sensor Kinematics 


    ROcp12_15 = C4*C5;
    ROcp12_25 = S4*C5;
    ROcp12_75 = C4*S5;
    ROcp12_85 = S4*S5;
    ROcp12_46 = ROcp12_75*S6-S4*C6;
    ROcp12_56 = ROcp12_85*S6+C4*C6;
    ROcp12_66 = C5*S6;
    ROcp12_76 = ROcp12_75*C6+S4*S6;
    ROcp12_86 = ROcp12_85*C6-C4*S6;
    ROcp12_96 = C5*C6;
    OMcp12_15 = -qd[5]*S4;
    OMcp12_25 = qd[5]*C4;
    OMcp12_16 = OMcp12_15+ROcp12_15*qd[6];
    OMcp12_26 = OMcp12_25+ROcp12_25*qd[6];
    OMcp12_36 = qd[4]-qd[6]*S5;
    OPcp12_16 = ROcp12_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp12_25*S5+ROcp12_25*qd[4]);
    OPcp12_26 = ROcp12_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp12_15*S5+ROcp12_15*qd[4]);
    OPcp12_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_13_0_6 = = 
 
// Sensor Kinematics 


    ROcp12_116 = ROcp12_15*C16-ROcp12_76*S16;
    ROcp12_216 = ROcp12_25*C16-ROcp12_86*S16;
    ROcp12_316 = -(ROcp12_96*S16+C16*S5);
    ROcp12_716 = ROcp12_15*S16+ROcp12_76*C16;
    ROcp12_816 = ROcp12_25*S16+ROcp12_86*C16;
    ROcp12_916 = ROcp12_96*C16-S16*S5;
    ROcp12_417 = ROcp12_46*C17+ROcp12_716*S17;
    ROcp12_517 = ROcp12_56*C17+ROcp12_816*S17;
    ROcp12_617 = ROcp12_66*C17+ROcp12_916*S17;
    ROcp12_717 = -(ROcp12_46*S17-ROcp12_716*C17);
    ROcp12_817 = -(ROcp12_56*S17-ROcp12_816*C17);
    ROcp12_917 = -(ROcp12_66*S17-ROcp12_916*C17);
    ROcp12_118 = ROcp12_116*C18+ROcp12_417*S18;
    ROcp12_218 = ROcp12_216*C18+ROcp12_517*S18;
    ROcp12_318 = ROcp12_316*C18+ROcp12_617*S18;
    ROcp12_418 = -(ROcp12_116*S18-ROcp12_417*C18);
    ROcp12_518 = -(ROcp12_216*S18-ROcp12_517*C18);
    ROcp12_618 = -(ROcp12_316*S18-ROcp12_617*C18);
    ROcp12_119 = ROcp12_118*C19-ROcp12_717*S19;
    ROcp12_219 = ROcp12_218*C19-ROcp12_817*S19;
    ROcp12_319 = ROcp12_318*C19-ROcp12_917*S19;
    ROcp12_719 = ROcp12_118*S19+ROcp12_717*C19;
    ROcp12_819 = ROcp12_218*S19+ROcp12_817*C19;
    ROcp12_919 = ROcp12_318*S19+ROcp12_917*C19;
    RLcp12_116 = ROcp12_46*s->dpt[2][6]+ROcp12_76*s->dpt[3][6];
    RLcp12_216 = ROcp12_56*s->dpt[2][6]+ROcp12_86*s->dpt[3][6];
    RLcp12_316 = ROcp12_66*s->dpt[2][6]+ROcp12_96*s->dpt[3][6];
    OMcp12_116 = OMcp12_16+ROcp12_46*qd[16];
    OMcp12_216 = OMcp12_26+ROcp12_56*qd[16];
    OMcp12_316 = OMcp12_36+ROcp12_66*qd[16];
    ORcp12_116 = OMcp12_26*RLcp12_316-OMcp12_36*RLcp12_216;
    ORcp12_216 = -(OMcp12_16*RLcp12_316-OMcp12_36*RLcp12_116);
    ORcp12_316 = OMcp12_16*RLcp12_216-OMcp12_26*RLcp12_116;
    OMcp12_117 = OMcp12_116+ROcp12_116*qd[17];
    OMcp12_217 = OMcp12_216+ROcp12_216*qd[17];
    OMcp12_317 = OMcp12_316+ROcp12_316*qd[17];
    OPcp12_117 = OPcp12_16+ROcp12_116*qdd[17]+ROcp12_46*qdd[16]+qd[16]*(OMcp12_26*ROcp12_66-OMcp12_36*ROcp12_56)+qd[17]*(
 OMcp12_216*ROcp12_316-OMcp12_316*ROcp12_216);
    OPcp12_217 = OPcp12_26+ROcp12_216*qdd[17]+ROcp12_56*qdd[16]-qd[16]*(OMcp12_16*ROcp12_66-OMcp12_36*ROcp12_46)-qd[17]*(
 OMcp12_116*ROcp12_316-OMcp12_316*ROcp12_116);
    OPcp12_317 = OPcp12_36+ROcp12_316*qdd[17]+ROcp12_66*qdd[16]+qd[16]*(OMcp12_16*ROcp12_56-OMcp12_26*ROcp12_46)+qd[17]*(
 OMcp12_116*ROcp12_216-OMcp12_216*ROcp12_116);
    RLcp12_118 = ROcp12_717*s->dpt[3][19];
    RLcp12_218 = ROcp12_817*s->dpt[3][19];
    RLcp12_318 = ROcp12_917*s->dpt[3][19];
    POcp12_118 = RLcp12_116+RLcp12_118+q[1];
    POcp12_218 = RLcp12_216+RLcp12_218+q[2];
    POcp12_318 = RLcp12_316+RLcp12_318+q[3];
    JTcp12_118_4 = -(RLcp12_216+RLcp12_218);
    JTcp12_218_4 = RLcp12_116+RLcp12_118;
    JTcp12_118_5 = C4*(RLcp12_316+RLcp12_318);
    JTcp12_218_5 = S4*(RLcp12_316+RLcp12_318);
    JTcp12_318_5 = -(C4*(RLcp12_116+RLcp12_118)+S4*(RLcp12_216+RLcp12_218));
    JTcp12_118_6 = ROcp12_25*(RLcp12_316+RLcp12_318)+S5*(RLcp12_216+RLcp12_218);
    JTcp12_218_6 = -(ROcp12_15*(RLcp12_316+RLcp12_318)+S5*(RLcp12_116+RLcp12_118));
    JTcp12_318_6 = ROcp12_15*(RLcp12_216+RLcp12_218)-ROcp12_25*(RLcp12_116+RLcp12_118);
    JTcp12_118_7 = -(RLcp12_218*ROcp12_66-RLcp12_318*ROcp12_56);
    JTcp12_218_7 = RLcp12_118*ROcp12_66-RLcp12_318*ROcp12_46;
    JTcp12_318_7 = -(RLcp12_118*ROcp12_56-RLcp12_218*ROcp12_46);
    JTcp12_118_8 = -(RLcp12_218*ROcp12_316-RLcp12_318*ROcp12_216);
    JTcp12_218_8 = RLcp12_118*ROcp12_316-RLcp12_318*ROcp12_116;
    JTcp12_318_8 = -(RLcp12_118*ROcp12_216-RLcp12_218*ROcp12_116);
    OMcp12_118 = OMcp12_117+ROcp12_717*qd[18];
    OMcp12_218 = OMcp12_217+ROcp12_817*qd[18];
    OMcp12_318 = OMcp12_317+ROcp12_917*qd[18];
    ORcp12_118 = OMcp12_217*RLcp12_318-OMcp12_317*RLcp12_218;
    ORcp12_218 = -(OMcp12_117*RLcp12_318-OMcp12_317*RLcp12_118);
    ORcp12_318 = OMcp12_117*RLcp12_218-OMcp12_217*RLcp12_118;
    VIcp12_118 = ORcp12_116+ORcp12_118+qd[1];
    VIcp12_218 = ORcp12_216+ORcp12_218+qd[2];
    VIcp12_318 = ORcp12_316+ORcp12_318+qd[3];
    ACcp12_118 = qdd[1]+OMcp12_217*ORcp12_318+OMcp12_26*ORcp12_316-OMcp12_317*ORcp12_218-OMcp12_36*ORcp12_216+OPcp12_217*
 RLcp12_318+OPcp12_26*RLcp12_316-OPcp12_317*RLcp12_218-OPcp12_36*RLcp12_216;
    ACcp12_218 = qdd[2]-OMcp12_117*ORcp12_318-OMcp12_16*ORcp12_316+OMcp12_317*ORcp12_118+OMcp12_36*ORcp12_116-OPcp12_117*
 RLcp12_318-OPcp12_16*RLcp12_316+OPcp12_317*RLcp12_118+OPcp12_36*RLcp12_116;
    ACcp12_318 = qdd[3]+OMcp12_117*ORcp12_218+OMcp12_16*ORcp12_216-OMcp12_217*ORcp12_118-OMcp12_26*ORcp12_116+OPcp12_117*
 RLcp12_218+OPcp12_16*RLcp12_216-OPcp12_217*RLcp12_118-OPcp12_26*RLcp12_116;
    OMcp12_119 = OMcp12_118+ROcp12_418*qd[19];
    OMcp12_219 = OMcp12_218+ROcp12_518*qd[19];
    OMcp12_319 = OMcp12_318+ROcp12_618*qd[19];
    OPcp12_119 = OPcp12_117+ROcp12_418*qdd[19]+ROcp12_717*qdd[18]+qd[18]*(OMcp12_217*ROcp12_917-OMcp12_317*ROcp12_817)+
 qd[19]*(OMcp12_218*ROcp12_618-OMcp12_318*ROcp12_518);
    OPcp12_219 = OPcp12_217+ROcp12_518*qdd[19]+ROcp12_817*qdd[18]-qd[18]*(OMcp12_117*ROcp12_917-OMcp12_317*ROcp12_717)-
 qd[19]*(OMcp12_118*ROcp12_618-OMcp12_318*ROcp12_418);
    OPcp12_319 = OPcp12_317+ROcp12_618*qdd[19]+ROcp12_917*qdd[18]+qd[18]*(OMcp12_117*ROcp12_817-OMcp12_217*ROcp12_717)+
 qd[19]*(OMcp12_118*ROcp12_518-OMcp12_218*ROcp12_418);

// = = Block_1_0_0_13_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp12_118;
    sens->P[2] = POcp12_218;
    sens->P[3] = POcp12_318;
    sens->R[1][1] = ROcp12_119;
    sens->R[1][2] = ROcp12_219;
    sens->R[1][3] = ROcp12_319;
    sens->R[2][1] = ROcp12_418;
    sens->R[2][2] = ROcp12_518;
    sens->R[2][3] = ROcp12_618;
    sens->R[3][1] = ROcp12_719;
    sens->R[3][2] = ROcp12_819;
    sens->R[3][3] = ROcp12_919;
    sens->V[1] = VIcp12_118;
    sens->V[2] = VIcp12_218;
    sens->V[3] = VIcp12_318;
    sens->OM[1] = OMcp12_119;
    sens->OM[2] = OMcp12_219;
    sens->OM[3] = OMcp12_319;
    sens->J[1][1] = (1.0);
    sens->J[1][4] = JTcp12_118_4;
    sens->J[1][5] = JTcp12_118_5;
    sens->J[1][6] = JTcp12_118_6;
    sens->J[1][16] = JTcp12_118_7;
    sens->J[1][17] = JTcp12_118_8;
    sens->J[2][2] = (1.0);
    sens->J[2][4] = JTcp12_218_4;
    sens->J[2][5] = JTcp12_218_5;
    sens->J[2][6] = JTcp12_218_6;
    sens->J[2][16] = JTcp12_218_7;
    sens->J[2][17] = JTcp12_218_8;
    sens->J[3][3] = (1.0);
    sens->J[3][5] = JTcp12_318_5;
    sens->J[3][6] = JTcp12_318_6;
    sens->J[3][16] = JTcp12_318_7;
    sens->J[3][17] = JTcp12_318_8;
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp12_15;
    sens->J[4][16] = ROcp12_46;
    sens->J[4][17] = ROcp12_116;
    sens->J[4][18] = ROcp12_717;
    sens->J[4][19] = ROcp12_418;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp12_25;
    sens->J[5][16] = ROcp12_56;
    sens->J[5][17] = ROcp12_216;
    sens->J[5][18] = ROcp12_817;
    sens->J[5][19] = ROcp12_518;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->J[6][16] = ROcp12_66;
    sens->J[6][17] = ROcp12_316;
    sens->J[6][18] = ROcp12_917;
    sens->J[6][19] = ROcp12_618;
    sens->A[1] = ACcp12_118;
    sens->A[2] = ACcp12_218;
    sens->A[3] = ACcp12_318;
    sens->OMP[1] = OPcp12_119;
    sens->OMP[2] = OPcp12_219;
    sens->OMP[3] = OPcp12_319;
 
// 
break;
case 14:
 


// = = Block_1_0_0_14_0_1 = = 
 
// Sensor Kinematics 


    ROcp13_15 = C4*C5;
    ROcp13_25 = S4*C5;
    ROcp13_75 = C4*S5;
    ROcp13_85 = S4*S5;
    ROcp13_46 = ROcp13_75*S6-S4*C6;
    ROcp13_56 = ROcp13_85*S6+C4*C6;
    ROcp13_66 = C5*S6;
    ROcp13_76 = ROcp13_75*C6+S4*S6;
    ROcp13_86 = ROcp13_85*C6-C4*S6;
    ROcp13_96 = C5*C6;
    OMcp13_15 = -qd[5]*S4;
    OMcp13_25 = qd[5]*C4;
    OMcp13_16 = OMcp13_15+ROcp13_15*qd[6];
    OMcp13_26 = OMcp13_25+ROcp13_25*qd[6];
    OMcp13_36 = qd[4]-qd[6]*S5;
    OPcp13_16 = ROcp13_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp13_25*S5+ROcp13_25*qd[4]);
    OPcp13_26 = ROcp13_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp13_15*S5+ROcp13_15*qd[4]);
    OPcp13_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_14_0_6 = = 
 
// Sensor Kinematics 


    ROcp13_116 = ROcp13_15*C16-ROcp13_76*S16;
    ROcp13_216 = ROcp13_25*C16-ROcp13_86*S16;
    ROcp13_316 = -(ROcp13_96*S16+C16*S5);
    ROcp13_716 = ROcp13_15*S16+ROcp13_76*C16;
    ROcp13_816 = ROcp13_25*S16+ROcp13_86*C16;
    ROcp13_916 = ROcp13_96*C16-S16*S5;
    ROcp13_417 = ROcp13_46*C17+ROcp13_716*S17;
    ROcp13_517 = ROcp13_56*C17+ROcp13_816*S17;
    ROcp13_617 = ROcp13_66*C17+ROcp13_916*S17;
    ROcp13_717 = -(ROcp13_46*S17-ROcp13_716*C17);
    ROcp13_817 = -(ROcp13_56*S17-ROcp13_816*C17);
    ROcp13_917 = -(ROcp13_66*S17-ROcp13_916*C17);
    ROcp13_118 = ROcp13_116*C18+ROcp13_417*S18;
    ROcp13_218 = ROcp13_216*C18+ROcp13_517*S18;
    ROcp13_318 = ROcp13_316*C18+ROcp13_617*S18;
    ROcp13_418 = -(ROcp13_116*S18-ROcp13_417*C18);
    ROcp13_518 = -(ROcp13_216*S18-ROcp13_517*C18);
    ROcp13_618 = -(ROcp13_316*S18-ROcp13_617*C18);
    ROcp13_119 = ROcp13_118*C19-ROcp13_717*S19;
    ROcp13_219 = ROcp13_218*C19-ROcp13_817*S19;
    ROcp13_319 = ROcp13_318*C19-ROcp13_917*S19;
    ROcp13_719 = ROcp13_118*S19+ROcp13_717*C19;
    ROcp13_819 = ROcp13_218*S19+ROcp13_817*C19;
    ROcp13_919 = ROcp13_318*S19+ROcp13_917*C19;
    ROcp13_120 = ROcp13_119*C20+ROcp13_418*S20;
    ROcp13_220 = ROcp13_219*C20+ROcp13_518*S20;
    ROcp13_320 = ROcp13_319*C20+ROcp13_618*S20;
    ROcp13_420 = -(ROcp13_119*S20-ROcp13_418*C20);
    ROcp13_520 = -(ROcp13_219*S20-ROcp13_518*C20);
    ROcp13_620 = -(ROcp13_319*S20-ROcp13_618*C20);
    RLcp13_116 = ROcp13_46*s->dpt[2][6]+ROcp13_76*s->dpt[3][6];
    RLcp13_216 = ROcp13_56*s->dpt[2][6]+ROcp13_86*s->dpt[3][6];
    RLcp13_316 = ROcp13_66*s->dpt[2][6]+ROcp13_96*s->dpt[3][6];
    OMcp13_116 = OMcp13_16+ROcp13_46*qd[16];
    OMcp13_216 = OMcp13_26+ROcp13_56*qd[16];
    OMcp13_316 = OMcp13_36+ROcp13_66*qd[16];
    ORcp13_116 = OMcp13_26*RLcp13_316-OMcp13_36*RLcp13_216;
    ORcp13_216 = -(OMcp13_16*RLcp13_316-OMcp13_36*RLcp13_116);
    ORcp13_316 = OMcp13_16*RLcp13_216-OMcp13_26*RLcp13_116;
    OMcp13_117 = OMcp13_116+ROcp13_116*qd[17];
    OMcp13_217 = OMcp13_216+ROcp13_216*qd[17];
    OMcp13_317 = OMcp13_316+ROcp13_316*qd[17];
    OPcp13_117 = OPcp13_16+ROcp13_116*qdd[17]+ROcp13_46*qdd[16]+qd[16]*(OMcp13_26*ROcp13_66-OMcp13_36*ROcp13_56)+qd[17]*(
 OMcp13_216*ROcp13_316-OMcp13_316*ROcp13_216);
    OPcp13_217 = OPcp13_26+ROcp13_216*qdd[17]+ROcp13_56*qdd[16]-qd[16]*(OMcp13_16*ROcp13_66-OMcp13_36*ROcp13_46)-qd[17]*(
 OMcp13_116*ROcp13_316-OMcp13_316*ROcp13_116);
    OPcp13_317 = OPcp13_36+ROcp13_316*qdd[17]+ROcp13_66*qdd[16]+qd[16]*(OMcp13_16*ROcp13_56-OMcp13_26*ROcp13_46)+qd[17]*(
 OMcp13_116*ROcp13_216-OMcp13_216*ROcp13_116);
    RLcp13_118 = ROcp13_717*s->dpt[3][19];
    RLcp13_218 = ROcp13_817*s->dpt[3][19];
    RLcp13_318 = ROcp13_917*s->dpt[3][19];
    OMcp13_118 = OMcp13_117+ROcp13_717*qd[18];
    OMcp13_218 = OMcp13_217+ROcp13_817*qd[18];
    OMcp13_318 = OMcp13_317+ROcp13_917*qd[18];
    ORcp13_118 = OMcp13_217*RLcp13_318-OMcp13_317*RLcp13_218;
    ORcp13_218 = -(OMcp13_117*RLcp13_318-OMcp13_317*RLcp13_118);
    ORcp13_318 = OMcp13_117*RLcp13_218-OMcp13_217*RLcp13_118;
    OMcp13_119 = OMcp13_118+ROcp13_418*qd[19];
    OMcp13_219 = OMcp13_218+ROcp13_518*qd[19];
    OMcp13_319 = OMcp13_318+ROcp13_618*qd[19];
    OPcp13_119 = OPcp13_117+ROcp13_418*qdd[19]+ROcp13_717*qdd[18]+qd[18]*(OMcp13_217*ROcp13_917-OMcp13_317*ROcp13_817)+
 qd[19]*(OMcp13_218*ROcp13_618-OMcp13_318*ROcp13_518);
    OPcp13_219 = OPcp13_217+ROcp13_518*qdd[19]+ROcp13_817*qdd[18]-qd[18]*(OMcp13_117*ROcp13_917-OMcp13_317*ROcp13_717)-
 qd[19]*(OMcp13_118*ROcp13_618-OMcp13_318*ROcp13_418);
    OPcp13_319 = OPcp13_317+ROcp13_618*qdd[19]+ROcp13_917*qdd[18]+qd[18]*(OMcp13_117*ROcp13_817-OMcp13_217*ROcp13_717)+
 qd[19]*(OMcp13_118*ROcp13_518-OMcp13_218*ROcp13_418);
    RLcp13_120 = ROcp13_719*s->dpt[3][22];
    RLcp13_220 = ROcp13_819*s->dpt[3][22];
    RLcp13_320 = ROcp13_919*s->dpt[3][22];
    POcp13_120 = RLcp13_116+RLcp13_118+RLcp13_120+q[1];
    POcp13_220 = RLcp13_216+RLcp13_218+RLcp13_220+q[2];
    POcp13_320 = RLcp13_316+RLcp13_318+RLcp13_320+q[3];
    JTcp13_120_4 = -(RLcp13_216+RLcp13_218+RLcp13_220);
    JTcp13_220_4 = RLcp13_116+RLcp13_118+RLcp13_120;
    JTcp13_120_5 = C4*(RLcp13_316+RLcp13_318+RLcp13_320);
    JTcp13_220_5 = S4*(RLcp13_316+RLcp13_318+RLcp13_320);
    JTcp13_320_5 = -(RLcp13_220*S4+C4*(RLcp13_116+RLcp13_118+RLcp13_120)+S4*(RLcp13_216+RLcp13_218));
    JTcp13_120_6 = RLcp13_220*S5+RLcp13_320*ROcp13_25+ROcp13_25*(RLcp13_316+RLcp13_318)+S5*(RLcp13_216+RLcp13_218);
    JTcp13_220_6 = -(RLcp13_120*S5+RLcp13_320*ROcp13_15+ROcp13_15*(RLcp13_316+RLcp13_318)+S5*(RLcp13_116+RLcp13_118));
    JTcp13_320_6 = ROcp13_15*(RLcp13_216+RLcp13_218)-ROcp13_25*(RLcp13_116+RLcp13_118)-RLcp13_120*ROcp13_25+RLcp13_220*
 ROcp13_15;
    JTcp13_120_7 = ROcp13_56*(RLcp13_318+RLcp13_320)-ROcp13_66*(RLcp13_218+RLcp13_220);
    JTcp13_220_7 = -(ROcp13_46*(RLcp13_318+RLcp13_320)-ROcp13_66*(RLcp13_118+RLcp13_120));
    JTcp13_320_7 = ROcp13_46*(RLcp13_218+RLcp13_220)-ROcp13_56*(RLcp13_118+RLcp13_120);
    JTcp13_120_8 = ROcp13_216*(RLcp13_318+RLcp13_320)-ROcp13_316*(RLcp13_218+RLcp13_220);
    JTcp13_220_8 = -(ROcp13_116*(RLcp13_318+RLcp13_320)-ROcp13_316*(RLcp13_118+RLcp13_120));
    JTcp13_320_8 = ROcp13_116*(RLcp13_218+RLcp13_220)-ROcp13_216*(RLcp13_118+RLcp13_120);
    JTcp13_120_9 = -(RLcp13_220*ROcp13_917-RLcp13_320*ROcp13_817);
    JTcp13_220_9 = RLcp13_120*ROcp13_917-RLcp13_320*ROcp13_717;
    JTcp13_320_9 = -(RLcp13_120*ROcp13_817-RLcp13_220*ROcp13_717);
    JTcp13_120_10 = -(RLcp13_220*ROcp13_618-RLcp13_320*ROcp13_518);
    JTcp13_220_10 = RLcp13_120*ROcp13_618-RLcp13_320*ROcp13_418;
    JTcp13_320_10 = -(RLcp13_120*ROcp13_518-RLcp13_220*ROcp13_418);
    OMcp13_120 = OMcp13_119+ROcp13_719*qd[20];
    OMcp13_220 = OMcp13_219+ROcp13_819*qd[20];
    OMcp13_320 = OMcp13_319+ROcp13_919*qd[20];
    ORcp13_120 = OMcp13_219*RLcp13_320-OMcp13_319*RLcp13_220;
    ORcp13_220 = -(OMcp13_119*RLcp13_320-OMcp13_319*RLcp13_120);
    ORcp13_320 = OMcp13_119*RLcp13_220-OMcp13_219*RLcp13_120;
    VIcp13_120 = ORcp13_116+ORcp13_118+ORcp13_120+qd[1];
    VIcp13_220 = ORcp13_216+ORcp13_218+ORcp13_220+qd[2];
    VIcp13_320 = ORcp13_316+ORcp13_318+ORcp13_320+qd[3];
    OPcp13_120 = OPcp13_119+ROcp13_719*qdd[20]+qd[20]*(OMcp13_219*ROcp13_919-OMcp13_319*ROcp13_819);
    OPcp13_220 = OPcp13_219+ROcp13_819*qdd[20]-qd[20]*(OMcp13_119*ROcp13_919-OMcp13_319*ROcp13_719);
    OPcp13_320 = OPcp13_319+ROcp13_919*qdd[20]+qd[20]*(OMcp13_119*ROcp13_819-OMcp13_219*ROcp13_719);
    ACcp13_120 = qdd[1]+OMcp13_217*ORcp13_318+OMcp13_219*ORcp13_320+OMcp13_26*ORcp13_316-OMcp13_317*ORcp13_218-OMcp13_319*
 ORcp13_220-OMcp13_36*ORcp13_216+OPcp13_217*RLcp13_318+OPcp13_219*RLcp13_320+OPcp13_26*RLcp13_316-OPcp13_317*RLcp13_218-
 OPcp13_319*RLcp13_220-OPcp13_36*RLcp13_216;
    ACcp13_220 = qdd[2]-OMcp13_117*ORcp13_318-OMcp13_119*ORcp13_320-OMcp13_16*ORcp13_316+OMcp13_317*ORcp13_118+OMcp13_319*
 ORcp13_120+OMcp13_36*ORcp13_116-OPcp13_117*RLcp13_318-OPcp13_119*RLcp13_320-OPcp13_16*RLcp13_316+OPcp13_317*RLcp13_118+
 OPcp13_319*RLcp13_120+OPcp13_36*RLcp13_116;
    ACcp13_320 = qdd[3]+OMcp13_117*ORcp13_218+OMcp13_119*ORcp13_220+OMcp13_16*ORcp13_216-OMcp13_217*ORcp13_118-OMcp13_219*
 ORcp13_120-OMcp13_26*ORcp13_116+OPcp13_117*RLcp13_218+OPcp13_119*RLcp13_220+OPcp13_16*RLcp13_216-OPcp13_217*RLcp13_118-
 OPcp13_219*RLcp13_120-OPcp13_26*RLcp13_116;

// = = Block_1_0_0_14_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp13_120;
    sens->P[2] = POcp13_220;
    sens->P[3] = POcp13_320;
    sens->R[1][1] = ROcp13_120;
    sens->R[1][2] = ROcp13_220;
    sens->R[1][3] = ROcp13_320;
    sens->R[2][1] = ROcp13_420;
    sens->R[2][2] = ROcp13_520;
    sens->R[2][3] = ROcp13_620;
    sens->R[3][1] = ROcp13_719;
    sens->R[3][2] = ROcp13_819;
    sens->R[3][3] = ROcp13_919;
    sens->V[1] = VIcp13_120;
    sens->V[2] = VIcp13_220;
    sens->V[3] = VIcp13_320;
    sens->OM[1] = OMcp13_120;
    sens->OM[2] = OMcp13_220;
    sens->OM[3] = OMcp13_320;
    sens->J[1][1] = (1.0);
    sens->J[1][4] = JTcp13_120_4;
    sens->J[1][5] = JTcp13_120_5;
    sens->J[1][6] = JTcp13_120_6;
    sens->J[1][16] = JTcp13_120_7;
    sens->J[1][17] = JTcp13_120_8;
    sens->J[1][18] = JTcp13_120_9;
    sens->J[1][19] = JTcp13_120_10;
    sens->J[2][2] = (1.0);
    sens->J[2][4] = JTcp13_220_4;
    sens->J[2][5] = JTcp13_220_5;
    sens->J[2][6] = JTcp13_220_6;
    sens->J[2][16] = JTcp13_220_7;
    sens->J[2][17] = JTcp13_220_8;
    sens->J[2][18] = JTcp13_220_9;
    sens->J[2][19] = JTcp13_220_10;
    sens->J[3][3] = (1.0);
    sens->J[3][5] = JTcp13_320_5;
    sens->J[3][6] = JTcp13_320_6;
    sens->J[3][16] = JTcp13_320_7;
    sens->J[3][17] = JTcp13_320_8;
    sens->J[3][18] = JTcp13_320_9;
    sens->J[3][19] = JTcp13_320_10;
    sens->J[4][5] = -S4;
    sens->J[4][6] = ROcp13_15;
    sens->J[4][16] = ROcp13_46;
    sens->J[4][17] = ROcp13_116;
    sens->J[4][18] = ROcp13_717;
    sens->J[4][19] = ROcp13_418;
    sens->J[4][20] = ROcp13_719;
    sens->J[5][5] = C4;
    sens->J[5][6] = ROcp13_25;
    sens->J[5][16] = ROcp13_56;
    sens->J[5][17] = ROcp13_216;
    sens->J[5][18] = ROcp13_817;
    sens->J[5][19] = ROcp13_518;
    sens->J[5][20] = ROcp13_819;
    sens->J[6][4] = (1.0);
    sens->J[6][6] = -S5;
    sens->J[6][16] = ROcp13_66;
    sens->J[6][17] = ROcp13_316;
    sens->J[6][18] = ROcp13_917;
    sens->J[6][19] = ROcp13_618;
    sens->J[6][20] = ROcp13_919;
    sens->A[1] = ACcp13_120;
    sens->A[2] = ACcp13_220;
    sens->A[3] = ACcp13_320;
    sens->OMP[1] = OPcp13_120;
    sens->OMP[2] = OPcp13_220;
    sens->OMP[3] = OPcp13_320;
 
// 
break;
case 15:
 


// = = Block_1_0_0_15_0_1 = = 
 
// Sensor Kinematics 


    ROcp14_15 = C4*C5;
    ROcp14_25 = S4*C5;
    ROcp14_75 = C4*S5;
    ROcp14_85 = S4*S5;
    ROcp14_46 = ROcp14_75*S6-S4*C6;
    ROcp14_56 = ROcp14_85*S6+C4*C6;
    ROcp14_66 = C5*S6;
    ROcp14_76 = ROcp14_75*C6+S4*S6;
    ROcp14_86 = ROcp14_85*C6-C4*S6;
    ROcp14_96 = C5*C6;
    OMcp14_15 = -qd[5]*S4;
    OMcp14_25 = qd[5]*C4;
    OMcp14_16 = OMcp14_15+ROcp14_15*qd[6];
    OMcp14_26 = OMcp14_25+ROcp14_25*qd[6];
    OMcp14_36 = qd[4]-qd[6]*S5;
    OPcp14_16 = ROcp14_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp14_25*S5+ROcp14_25*qd[4]);
    OPcp14_26 = ROcp14_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp14_15*S5+ROcp14_15*qd[4]);
    OPcp14_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_15_0_2 = = 
 
// Sensor Kinematics 


    ROcp14_17 = ROcp14_15*C7-ROcp14_76*S7;
    ROcp14_27 = ROcp14_25*C7-ROcp14_86*S7;
    ROcp14_37 = -(ROcp14_96*S7+S5*C7);
    ROcp14_77 = ROcp14_15*S7+ROcp14_76*C7;
    ROcp14_87 = ROcp14_25*S7+ROcp14_86*C7;
    ROcp14_97 = ROcp14_96*C7-S5*S7;
    RLcp14_17 = ROcp14_46*s->dpt[2][2];
    RLcp14_27 = ROcp14_56*s->dpt[2][2];
    RLcp14_37 = ROcp14_66*s->dpt[2][2];
    POcp14_17 = RLcp14_17+q[1];
    POcp14_27 = RLcp14_27+q[2];
    POcp14_37 = RLcp14_37+q[3];
    OMcp14_17 = OMcp14_16+ROcp14_46*qd[7];
    OMcp14_27 = OMcp14_26+ROcp14_56*qd[7];
    OMcp14_37 = OMcp14_36+ROcp14_66*qd[7];
    ORcp14_17 = OMcp14_26*RLcp14_37-OMcp14_36*RLcp14_27;
    ORcp14_27 = -(OMcp14_16*RLcp14_37-OMcp14_36*RLcp14_17);
    ORcp14_37 = OMcp14_16*RLcp14_27-OMcp14_26*RLcp14_17;
    VIcp14_17 = ORcp14_17+qd[1];
    VIcp14_27 = ORcp14_27+qd[2];
    VIcp14_37 = ORcp14_37+qd[3];
    OPcp14_17 = OPcp14_16+ROcp14_46*qdd[7]+qd[7]*(OMcp14_26*ROcp14_66-OMcp14_36*ROcp14_56);
    OPcp14_27 = OPcp14_26+ROcp14_56*qdd[7]-qd[7]*(OMcp14_16*ROcp14_66-OMcp14_36*ROcp14_46);
    OPcp14_37 = OPcp14_36+ROcp14_66*qdd[7]+qd[7]*(OMcp14_16*ROcp14_56-OMcp14_26*ROcp14_46);
    ACcp14_17 = qdd[1]+OMcp14_26*ORcp14_37-OMcp14_36*ORcp14_27+OPcp14_26*RLcp14_37-OPcp14_36*RLcp14_27;
    ACcp14_27 = qdd[2]-OMcp14_16*ORcp14_37+OMcp14_36*ORcp14_17-OPcp14_16*RLcp14_37+OPcp14_36*RLcp14_17;
    ACcp14_37 = qdd[3]+OMcp14_16*ORcp14_27-OMcp14_26*ORcp14_17+OPcp14_16*RLcp14_27-OPcp14_26*RLcp14_17;

// = = Block_1_0_0_15_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp14_17;
    sens->P[2] = POcp14_27;
    sens->P[3] = POcp14_37;
    sens->R[1][1] = ROcp14_17;
    sens->R[1][2] = ROcp14_27;
    sens->R[1][3] = ROcp14_37;
    sens->R[2][1] = ROcp14_46;
    sens->R[2][2] = ROcp14_56;
    sens->R[2][3] = ROcp14_66;
    sens->R[3][1] = ROcp14_77;
    sens->R[3][2] = ROcp14_87;
    sens->R[3][3] = ROcp14_97;
    sens->V[1] = VIcp14_17;
    sens->V[2] = VIcp14_27;
    sens->V[3] = VIcp14_37;
    sens->OM[1] = OMcp14_17;
    sens->OM[2] = OMcp14_27;
    sens->OM[3] = OMcp14_37;
    sens->A[1] = ACcp14_17;
    sens->A[2] = ACcp14_27;
    sens->A[3] = ACcp14_37;
    sens->OMP[1] = OPcp14_17;
    sens->OMP[2] = OPcp14_27;
    sens->OMP[3] = OPcp14_37;
 
// 
break;
case 16:
 


// = = Block_1_0_0_16_0_1 = = 
 
// Sensor Kinematics 


    ROcp15_15 = C4*C5;
    ROcp15_25 = S4*C5;
    ROcp15_75 = C4*S5;
    ROcp15_85 = S4*S5;
    ROcp15_46 = ROcp15_75*S6-S4*C6;
    ROcp15_56 = ROcp15_85*S6+C4*C6;
    ROcp15_66 = C5*S6;
    ROcp15_76 = ROcp15_75*C6+S4*S6;
    ROcp15_86 = ROcp15_85*C6-C4*S6;
    ROcp15_96 = C5*C6;
    OMcp15_15 = -qd[5]*S4;
    OMcp15_25 = qd[5]*C4;
    OMcp15_16 = OMcp15_15+ROcp15_15*qd[6];
    OMcp15_26 = OMcp15_25+ROcp15_25*qd[6];
    OMcp15_36 = qd[4]-qd[6]*S5;
    OPcp15_16 = ROcp15_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp15_25*S5+ROcp15_25*qd[4]);
    OPcp15_26 = ROcp15_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp15_15*S5+ROcp15_15*qd[4]);
    OPcp15_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_16_0_3 = = 
 
// Sensor Kinematics 


    ROcp15_18 = ROcp15_15*C8-ROcp15_76*S8;
    ROcp15_28 = ROcp15_25*C8-ROcp15_86*S8;
    ROcp15_38 = -(ROcp15_96*S8+S5*C8);
    ROcp15_78 = ROcp15_15*S8+ROcp15_76*C8;
    ROcp15_88 = ROcp15_25*S8+ROcp15_86*C8;
    ROcp15_98 = ROcp15_96*C8-S5*S8;
    RLcp15_18 = ROcp15_46*s->dpt[2][3];
    RLcp15_28 = ROcp15_56*s->dpt[2][3];
    RLcp15_38 = ROcp15_66*s->dpt[2][3];
    POcp15_18 = RLcp15_18+q[1];
    POcp15_28 = RLcp15_28+q[2];
    POcp15_38 = RLcp15_38+q[3];
    OMcp15_18 = OMcp15_16+ROcp15_46*qd[8];
    OMcp15_28 = OMcp15_26+ROcp15_56*qd[8];
    OMcp15_38 = OMcp15_36+ROcp15_66*qd[8];
    ORcp15_18 = OMcp15_26*RLcp15_38-OMcp15_36*RLcp15_28;
    ORcp15_28 = -(OMcp15_16*RLcp15_38-OMcp15_36*RLcp15_18);
    ORcp15_38 = OMcp15_16*RLcp15_28-OMcp15_26*RLcp15_18;
    VIcp15_18 = ORcp15_18+qd[1];
    VIcp15_28 = ORcp15_28+qd[2];
    VIcp15_38 = ORcp15_38+qd[3];
    OPcp15_18 = OPcp15_16+ROcp15_46*qdd[8]+qd[8]*(OMcp15_26*ROcp15_66-OMcp15_36*ROcp15_56);
    OPcp15_28 = OPcp15_26+ROcp15_56*qdd[8]-qd[8]*(OMcp15_16*ROcp15_66-OMcp15_36*ROcp15_46);
    OPcp15_38 = OPcp15_36+ROcp15_66*qdd[8]+qd[8]*(OMcp15_16*ROcp15_56-OMcp15_26*ROcp15_46);
    ACcp15_18 = qdd[1]+OMcp15_26*ORcp15_38-OMcp15_36*ORcp15_28+OPcp15_26*RLcp15_38-OPcp15_36*RLcp15_28;
    ACcp15_28 = qdd[2]-OMcp15_16*ORcp15_38+OMcp15_36*ORcp15_18-OPcp15_16*RLcp15_38+OPcp15_36*RLcp15_18;
    ACcp15_38 = qdd[3]+OMcp15_16*ORcp15_28-OMcp15_26*ORcp15_18+OPcp15_16*RLcp15_28-OPcp15_26*RLcp15_18;

// = = Block_1_0_0_16_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp15_18;
    sens->P[2] = POcp15_28;
    sens->P[3] = POcp15_38;
    sens->R[1][1] = ROcp15_18;
    sens->R[1][2] = ROcp15_28;
    sens->R[1][3] = ROcp15_38;
    sens->R[2][1] = ROcp15_46;
    sens->R[2][2] = ROcp15_56;
    sens->R[2][3] = ROcp15_66;
    sens->R[3][1] = ROcp15_78;
    sens->R[3][2] = ROcp15_88;
    sens->R[3][3] = ROcp15_98;
    sens->V[1] = VIcp15_18;
    sens->V[2] = VIcp15_28;
    sens->V[3] = VIcp15_38;
    sens->OM[1] = OMcp15_18;
    sens->OM[2] = OMcp15_28;
    sens->OM[3] = OMcp15_38;
    sens->A[1] = ACcp15_18;
    sens->A[2] = ACcp15_28;
    sens->A[3] = ACcp15_38;
    sens->OMP[1] = OPcp15_18;
    sens->OMP[2] = OPcp15_28;
    sens->OMP[3] = OPcp15_38;

break;
default:
break;
}


// ====== END Task 1 ====== 


}
 

