#ifndef __c2_Frank_SoT_h__
#define __c2_Frank_SoT_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef struct_struct_8DmvxxRBphZR6qetFqXQeG_tag
#define struct_struct_8DmvxxRBphZR6qetFqXQeG_tag

struct struct_8DmvxxRBphZR6qetFqXQeG_tag
{
  real_T dpt[69];
  real_T m[20];
};

#endif                                 /*struct_struct_8DmvxxRBphZR6qetFqXQeG_tag*/

#ifndef typedef_c2_struct_8DmvxxRBphZR6qetFqXQeG
#define typedef_c2_struct_8DmvxxRBphZR6qetFqXQeG

typedef struct struct_8DmvxxRBphZR6qetFqXQeG_tag
  c2_struct_8DmvxxRBphZR6qetFqXQeG;

#endif                                 /*typedef_c2_struct_8DmvxxRBphZR6qetFqXQeG*/

#ifndef struct_si7KTM2s8m600LQcqdrYp6G
#define struct_si7KTM2s8m600LQcqdrYp6G

struct si7KTM2s8m600LQcqdrYp6G
{
  real_T P[3];
  real_T R[9];
  real_T V[3];
  real_T OM[3];
  real_T A[3];
  real_T OMP[3];
  real_T J[120];
};

#endif                                 /*struct_si7KTM2s8m600LQcqdrYp6G*/

#ifndef typedef_c2_si7KTM2s8m600LQcqdrYp6G
#define typedef_c2_si7KTM2s8m600LQcqdrYp6G

typedef struct si7KTM2s8m600LQcqdrYp6G c2_si7KTM2s8m600LQcqdrYp6G;

#endif                                 /*typedef_c2_si7KTM2s8m600LQcqdrYp6G*/

#ifndef typedef_SFc2_Frank_SoTInstanceStruct
#define typedef_SFc2_Frank_SoTInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  int32_T c2_sfEvent;
  boolean_T c2_isStable;
  boolean_T c2_doneDoubleBufferReInit;
  uint8_T c2_is_active_c2_Frank_SoT;
  c2_struct_8DmvxxRBphZR6qetFqXQeG c2_rob_str;
  real_T c2_Ts;
  real_T c2_R;
  real_T c2_W;
} SFc2_Frank_SoTInstanceStruct;

#endif                                 /*typedef_SFc2_Frank_SoTInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c2_Frank_SoT_get_eml_resolved_functions_info(void);

/* Function Definitions */
extern void sf_c2_Frank_SoT_get_check_sum(mxArray *plhs[]);
extern void c2_Frank_SoT_method_dispatcher(SimStruct *S, int_T method, void
  *data);

#endif
