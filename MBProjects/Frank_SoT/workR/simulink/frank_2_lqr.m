% rosclear all;
% Physical Constant
g = 9.81;						% gravity acceleration [m/sec^2]

% frank-2.0 Parameters
m = 1.6;						% wheel weight [kg]                                                     ok
R = 0.13;						% wheel radius [m]                                                      ok
Jw = 0.0057;                    % wheel inertia moment [kgm^2]                                          ok
M = 17.9; %14.6;						% body weight [kg]  2.4;                                                ok
W = 0.496;						% body width [m]                                                        ok
L = 0.087 + 0.292;%+ 0.267;				% distance of the center of mass from the wheel axle [m]  0.1335        ok
Jpsi = 1.58;%1.25;                    % body pitch inertia moment [kgm^2] 0.0377                              ok
Jphi = 0.39;                    % body yaw inertia moment [kgm^2]                                       ok
fm = 0.00059249;				% friction coefficient between body & DC motor                          ok
fw = 0;							% friction coefficient between wheel & floor                            NO

% DC Motor Parameters			
Jm = 0.00010667;				% DC motor inertia moment [kgm^2]                                       ok
Rm = 0.84278;					% DC motor resistance [��]                                              ok
Kb = 0.048365;                  % DC motor back EMF constant [Vsec/rad]                                 ok
Kt = 0.015056;					% DC motor torque constant [Nm/A]                                       ok
n = 160;						% gear ratio                                                            OK


% State-Space Matrix Calculation Voltaage
alpha = n * Kt / Rm;
beta = n * Kt * Kb / Rm + fm;
tmp = beta + fw;

% % State-Space Matrix Calculation Current
% alpha = n * Kt;
% beta = fm;
% tmp = beta + fw;

E_11 = (2 * m + M) * R^2 + 2 * Jw + 2 * n^2 * Jm;
E_12 = M * L * R - 2 * n^2 * Jm;
E_22 = M * L^2 + Jpsi + 2 * n^2 * Jm;
detE = E_11 * E_22 - E_12^2;

A1_32 = -g * M * L * E_12 / detE;
A1_42 = g * M * L * E_11 / detE;
A1_33 = -2 * (tmp * E_22 + beta * E_12) / detE;
A1_43 = 2 * (tmp * E_12 + beta * E_11) / detE;
A1_34 = 2 * beta * (E_22 + E_12) / detE;
A1_44 = -2 * beta * (E_11 + E_12) / detE;
B1_3 = alpha * (E_22 + E_12) / detE;
B1_4 = -alpha * (E_11 + E_12) / detE;
A1 = [
	0 0 1 0
	0 0 0 1
	0 A1_32 A1_33 A1_34
	0 A1_42 A1_43 A1_44
	];
B1 = [
	0 0
	0 0
	B1_3 B1_3
	B1_4 B1_4
	];
C1 = eye(4);
D1 = zeros(4, 2);

I = m * W^2 / 2 + Jphi + (Jw + n^2 * Jm) * W^2 / (2 * R^2);
J = tmp * W^2 / (2 * R^2);
K = alpha * W / (2 * R);
A2 = [
	0 1
	0 -J / I
	];
B2 = [
	0      0
	K / I -K / I
	];
C2 = eye(2);
D2 = zeros(2);

clear alpha beta tmp
clear E_11 E_12 E_22 detE
clear A1_32 A1_33 A1_34 A1_42 A1_43 A1_44 B1_3 B1_4 I J K

% Controller Parameters 

% Servo Gain Calculation using Optimal Regulator
A_BAR = [A1, zeros(4, 1); C1(1, :), 0];
B_BAR = [B1; 0, 0];

% QQ = [
% 	1, 0,   0, 0, 0
% 	0, 6e5, 0, 0, 0
% 	0, 0,   1, 0, 0
% 	0, 0,   0, 1, 0
% 	0, 0,   0, 0, 1e1
% 	];


QQ = [
	1, 0,   0, 0, 0
	0, 7e5, 0, 0, 0
	0, 0,   1, 0, 0
	0, 0,   0, 1, 0
	0, 0,   0, 0, 1e1
	];
RR = 0.15 * eye(2);
KK = lqr(A_BAR, B_BAR, QQ, RR);
k_f = KK(1, 1:4);					% feedback gain
k_i = KK(1, 5);						% integral gain


% %LQR con l integrale di theta
% A_BAR = [A1, zeros(4, 3); C1(1, :), zeros(1, 3); zeros(2, 5), A2];
% B_BAR = [B1; 0, 0; B2];

% %stato th psi dth dpsi th_integrale phi dphi
% QQ = diag([1 7e5 1 1 4e2 1 1]);
% RR = 0.6 * eye(2); %1e1
% KK = lqr(A_BAR, B_BAR, QQ, RR);
% k_f1 = KK(1, 1:4);					% feedback gain
% k_i = KK(1, 5);						% integral gain
% k_f2 = KK(1:2, 6:7);

% %LQR senza integrale di theta
% A_BAR = [A1, zeros(4, 2);zeros(2, 4), A2];
% B_BAR = [B1; B2];
% 
% %stato th psi dth dpsi phi dphi
% QQ = diag([1 1000 1 1 1 1]);
% RR = 0.0008 * eye(2); %1e1
% KK = lqr(A_BAR, B_BAR, QQ, RR);
% k_f1 = KK(1, 1:4);					% feedback gain
% k_f2 = KK(1:2, 5:6);
% k_i = 0;



ts = 0.005;
%Simulink Equation
syms th dth ddth psi dpsi ddpsi phi dphi ddphi V_R V_L real;

Equations(1,1) = ((2*m +M)*(R^2) + 2*Jw + 2*(n^2)*Jm)*ddth + (M*L*R*cos(psi) - 2*(n^2)*Jm)*ddpsi - M*L*R*(dpsi^2)*sin(psi);
Equations(2,1) = (M*L*R*cos(psi) - 2*(n^2)*Jm)*ddth + (M*(L^2) + Jpsi + 2*(n^2)*Jm)*ddpsi - M*g*L*sin(psi) - M*(L^2)*(dphi^2)*sin(psi)*cos(psi);
Equations(3,1) = (0.5*m*(W^2) + Jphi + (W^2/(2*(R^2)))*(Jw + (n^2)*Jm) + M*(L^2)*((sin(psi))^2))*ddphi + 2*M*(L^2)*dpsi*dphi*sin(psi)*cos(psi);

alpha = (n*Kt)/Rm;
beta = ((n*Kt*Kb)/Rm) + fm;
Fth = alpha*(V_L + V_R) - 2*(beta + fw)*dth + 2*beta*dpsi;
Fpsi = -alpha*(V_L + V_R) + 2*beta*dth - 2*beta*dpsi;
Fphi  = (W/(2*R))*alpha*(V_R - V_L) - ((W^2)/(2*(R^2)))*(beta + fw)*dphi;

eqns = [Equations(1,1) == Fth, Equations(2,1) == Fpsi, Equations(3,1) == Fphi];
vars = [ddth ddpsi ddphi];
[solddth, solddpsi, solddphi] = solve(eqns, vars);