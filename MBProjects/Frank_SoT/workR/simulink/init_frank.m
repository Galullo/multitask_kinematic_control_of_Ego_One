% init_Frank
addpath('/home/parallels/.robotran/mbsysc/MBsysC/build/lib')

mbs_load('Frank_SoT');

prjname = 'Frank_SoT';

%% Numerical Data

% I use this in controller function
rob_str.dpt = MBS_data.dpt;
rob_str.m = MBS_data.m;

% step of simulation
Ts = 0.02;

% simulation time
Tsim = 29.0;

% compute the LQR gain
Frank_LQR_base;

% load trajectory 
%load('timeseries_from_bag_pose_quat.mat');
%load('timeseries_simple_arc.mat')
load('timeseries_task_tot.mat')

% rotation matrix Robotran to ROS
R_shuld = [1 0 0; 0 0 -1; 0 1 0]*[0 -1 0; 1 0 0; 0 0 1];
% R_shuld1 = [0, 0, 1; 0, -1, 0; 1, 0, 0];
R_shuld1 = eye(3);

% Gains
Kp = 10;
Ko = 5;
%% simulink
%open Frank_SoT.mdl
