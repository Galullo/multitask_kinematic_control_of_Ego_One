% my tasks

t = 0:0.02:16;
t2 = 0:0.02:15;

radius = 0.3360;
omega = 0.1;
omega_rot = pi/8;

n_sample = length(t) + length(t2);
% trajectory

% task 1
x = radius*cos(omega*t - pi/2);
y = 0*t;
z = radius*sin(omega*t - pi/2);

% task 2
x2 = radius*sin(omega*t2 + pi/2);
y2 = radius*cos(omega*t2 + pi/2);
z2 = z(end)*ones(1,length(t2));




% total trajectory
xt = [x, x2];
yt = [y, y2];
zt = [z, z2];


figure,
for i = 1:10:n_sample
    plot3(xt(i), yt(i), zt(i),'r-', 'MarkerSize',18), hold on, grid on,
    xlabel('x'), ylabel('y'), zlabel('z')
    xlim([-radius, radius]), ylim([-radius, radius]), zlim([-radius, radius])
    
    pause(0.0001);
end

figure,
plot3(xt, yt, zt,'r-', 'MarkerSize',18), hold on, grid on,
xlabel('x'), ylabel('y'), zlabel('z')
xlim([-radius, radius]), ylim([-radius, radius]), zlim([-radius, radius])

% orintation

quat_task1 = zeros(4, (length(t)));
 
 for i = 1:(length(t))
     Ry = [cos(omega*t(i)), 0, sin(omega*t(i)); 0, 1, 0; -sin(omega*t(i)), 0, cos(omega*t(i))];
     quat_task1(:,i) = rot2quat(Ry);
 end
 
 quat_task2 = zeros(4, length(t2));
 
 for i = 1:length(t2)
     Ry = [cos(omega*t(end)), 0, sin(omega*t(end)); 0, 1, 0; -sin(omega*t(end)), 0, cos(omega*t(end))];
     %Ry = [0 0 1; 0 1 0; -1 0 0];
     Rz = [cos(omega*t2(i)) -sin(omega*t2(i)) 0; sin(omega*t2(i)) cos(omega*t2(i)) 0; 0 0 1];
     %Rx = [1 0 0; 0 cos(omega_rot*t2(i)) sin(omega_rot*t2(i));0 -sin(omega_rot*t2(i)) cos(omega_rot*t2(i))];
     %Rx = [1 0 0 ; 0 0 -1; 0 1 0];
     quat_task2(:,i) = rot2quat(Rz);
 end
 
 quat_task3 = zeros(4,length(t2));
 for i = 1:length(t2)
    quat_task3(:,i) = quat_task2(:,end);
 end
 
 quat_t = [quat_task1, quat_task2];
 
 
 % timeseries
 Tsim = 31;
 time = linspace(0, Tsim, n_sample);
 
 position_arm = timeseries([xt;yt;zt],time,'Name','position_arm');
 orientation_arm = timeseries(quat_t,time,'Name','orientation_arm');
 
 save('timeseries_task_tot', 'position_arm', 'orientation_arm')