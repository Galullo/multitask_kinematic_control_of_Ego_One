% Plot data

folder = 'trash';
%% SoT
% Joints's velocity
figure
plot(time, q_dot_1(:,1), 'b--', 'LineWidth', 2), hold on,
plot(time, q_dot_1(:,8),'r--', 'LineWidth', 2), legend('right','left')
title('Shulder R_y')
xlabel('time [s]'), ylabel('dq [rad/s]')
savefig([folder '/velocity_shulder_Ry1.fig'])

figure,
plot(time, q_dot_1(:,2),'b--', 'LineWidth', 2), hold on,
plot(time, q_dot_1(:,9),'r--', 'LineWidth', 2), legend('right','left')
title('Arm R_x')
xlabel('time [s]'), ylabel('dq [rad/s]')
savefig([folder '/velocity_shulder_Rx2.fig'])

figure,
plot(time, q_dot_1(:,3),'b--', 'LineWidth', 2), hold on,
plot(time, q_dot_1(:,10),'r--', 'LineWidth', 2), legend('right','left')
title('Elbow R_z')
xlabel('time [s]'), ylabel('dq [rad/s]')
savefig([folder '/velocity_shulder_Rz3.fig'])

figure,
plot(time, q_dot_1(:,4),'b--', 'LineWidth', 2), hold on,
plot(time, q_dot_1(:,11),'r--', 'LineWidth', 2), legend('right','left')
title('Forarm R_y')
xlabel('time [s]'), ylabel('dq [rad/s]')
savefig([folder '/velocity_shulder_Ry4.fig'])

figure,
plot(time, q_dot_1(:,5),'b', 'LineWidth', 2), hold on,
plot(time, q_dot_1(:,12),'r', 'LineWidth', 2), legend('right','left')
title('Hand R_z')
xlabel('time [s]'), ylabel('dq [rad/s]')
savefig([folder '/velocity_shulder_Rz5.fig'])

figure
subplot(2,1,1)
plot(time, q_dot_1(:,6),'c--', 'LineWidth', 2), hold on,
xlabel('time [s]'), ylabel('dq [rad/s]' )
title('Nek')

subplot(2,1,2)
plot(time, q_dot_1(:,7),'c--', 'LineWidth', 2), hold on,
xlabel('time [s]'), ylabel('dq [rad/s]')
title('Head')
savefig([folder '/velocity_shulder_nek_head.fig'])



%% plot the Joints's value
figure
plot(time, q_SoT(:,1), 'b', 'LineWidth', 2), hold on,
plot(time, q_SoT(:,8),'r', 'LineWidth', 2), legend('right','left')
title('Shulder R_y')
xlabel('time [s]'), ylabel('q [rad]')
savefig([folder '/shulder_Rx1.fig'])

figure,
plot(time, q_SoT(:,2),'b', 'LineWidth', 2), hold on,
plot(time, q_SoT(:,9),'r', 'LineWidth', 2), legend('right','left')
title('Arm R_x')
xlabel('time [s]'), ylabel('q [rad]')
savefig([folder '/shulder_Ry2.fig'])

figure,
plot(time, q_SoT(:,3),'b', 'LineWidth', 2), hold on,
plot(time, q_SoT(:,10),'r', 'LineWidth', 2), legend('right','left')
title('Elbow R_z')
xlabel('time [s]'), ylabel('q [rad]')
savefig([folder '/shulder_Rz3.fig'])

figure,
plot(time, q_SoT(:,4),'b', 'LineWidth', 2), hold on,
plot(time, q_SoT(:,11),'r', 'LineWidth', 2), legend('right','left')
title('Forarm R_y')
xlabel('time [s]'), ylabel('q [rad]')
savefig([folder '/shulder_Ry4.fig'])

figure,
plot(time, q_SoT(:,5),'b', 'LineWidth', 2), hold on,
plot(time, q_SoT(:,12),'r', 'LineWidth', 2), legend('right','left')
title('Hand R_z')
xlabel('time [s]'), ylabel('q [rad]')
savefig([folder '/shulder_Rz5.fig'])

figure
subplot(2,1,1)
plot(time, q_SoT(:,6),'c', 'LineWidth', 2),
xlabel('time [s]'), ylabel('q [rad]' )
title('Nek')
subplot(2,1,2)
plot(time, q_SoT(:,7),'c', 'LineWidth', 2),
xlabel('time [s]'), ylabel('q [rad]')
title('Head')

savefig([folder '/shulder_nek_shulder.fig'])


% plot the trajectory of right hand
Pose_ee_right_loc = zeros(size(Pose_ee_right));
Pose_sim = Pose_ee_right - Pose_shul_right;

% plot ref trajectory and end-effector trajectory
for i = 1:length(Pose_ee_right)
    Pose_ee_right_loc(i,:) = (R_shuld1*Pose_sim(i,:)');
end

figure
% plot3(right_task_pose.Data(:,1), right_task_pose.Data(:,2), right_task_pose.Data(:,3),'b','LineWidth', 2), hold on,
% plot3(right_task_pose.Data(1,1), right_task_pose.Data(1,2), right_task_pose.Data(1,3),'bo','MarkerSize', 8, 'LineWidth',2), hold on,
plot3(position_arm.Data(1,:),position_arm.Data(2,:),position_arm.Data(3,:),'b','LineWidth', 2), hold on,
plot3(position_arm.Data(1,1),position_arm.Data(2,1),position_arm.Data(3,1),'bo','MarkerSize', 9, 'LineWidth',2), hold on,
plot3(Pose_ee_right_loc(:,1), Pose_ee_right_loc(:,2), Pose_ee_right_loc(:,3),'r','LineWidth', 2), grid on, hold on,
plot3(Pose_ee_right_loc(1,1), Pose_ee_right_loc(1,2), Pose_ee_right_loc(1,3),'ro','MarkerSize', 8, 'LineWidth',2), grid on
legend('ref','', 'e-e','')
xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]')
savefig([folder '/trajectory.fig'])

% plot the CoM estimated by SoT
figure,
plot3(CoM_SoT(:,1),CoM_SoT(:,2),CoM_SoT(:,3),'k', 'LineWidth', 2),
xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]'), grid on
savefig([folder '/CoM_SoT.fig'])


%% plot the CoM values from LQR
figure
title('CoM')
plot3(CoM(:,1),CoM(:,2),CoM(:,3), 'k','LineWidth',2), grid on
xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]')
xlim([-1.2 1.2]), ylim([-1 1]), zlim([0 0.4])



% orient_ee = zeros(4,length(0:Ts:Tsim));
% 
% for i = 1:length(0:Ts:Tsim)
%     orient_ee(:,i) = rotm2quat(R_ee_right1(:,:,i));
% end
% 
% plot(orient_ee)






