
Tsim = 16;
t = 0:0.02:Tsim;


radius = 0.3360;
omega = 0.3;
omega_rot = pi/8;

n_sample = length(t);% + length(t2) + length(t3);
% trajectory

% task 1
x = radius*cos(omega*t - pi/2);
y = 0*t;
z = radius*sin(omega*t - pi/2);


% total trajectory
xt = x;
yt = y;
zt = z;


% figure,
% for i = 1:10:n_sample
%     plot3(xt(i), yt(i), zt(i),'r-', 'MarkerSize',18), hold on, grid on,
%     xlabel('x'), ylabel('y'), zlabel('z')
%     xlim([-radius, radius]), ylim([-radius, radius]), zlim([-radius, radius])
%     
%     pause(0.0001);
% end

% figure,
% plot3(xt, yt, zt,'r-', 'MarkerSize',18), hold on, grid on,
% xlabel('x'), ylabel('y'), zlabel('z')
% xlim([-radius, radius]), ylim([-radius, radius]), zlim([-radius, radius])

% orintation

quat_task1 = zeros(4, (length(t)));
 
 for i = 1:(length(t))
     Ry = [cos(omega*t(i)), 0, sin(omega*t(i)); 0, 1, 0; -sin(omega*t(i)), 0, cos(omega*t(i))];
     quat_task1(:,i) = rot2quat(Ry);
 end
 
 
 quat_t = quat_task1;
 
 
 % timeseries
 
 position_arm = timeseries([xt;yt;zt],t,'Name','position_arm');
 orientation_arm = timeseries(quat_t,t,'Name','orientation_arm');
 
 save('timeseries_task_tot', 'position_arm', 'orientation_arm')