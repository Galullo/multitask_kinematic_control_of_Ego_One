Tsim = 15.60;
t = 0:0.02:Tsim;


radius = 0.3360;
omega = 0.1;

% trajectory

%task prova
xp = (radius/2)*cos(omega*t-pi/2);
yp = 0*t;
zp = (radius/2)*sin(omega*t-pi/2) - (radius/2);


% figure
% for i = 1:5:length(t)
%     plot3(xp(i),yp(i),zp(i),'r--', 'MarkerSize',10),hold on, grid on,
%     xlabel('x'), ylabel('y'), zlabel('z')
%     xlim([-radius, radius]), ylim([-radius, radius]), zlim([-radius, radius])
%     pause(0.01);
% end


quat_task_prova = zeros(4, length(t));
 
 for i = 1:length(t)
     Ry = [cos(omega*t(i)), 0, sin(omega*t(i)); 0, 1, 0; -sin(omega*t(i)), 0, cos(omega*t(i))];
     quat_task_prova(:,i) = rot2quat(Ry);
 end
 
 % timeseries
 
 position_arm = timeseries([xp;yp;zp],t,'Name','position_arm');
 orientation_arm = timeseries(quat_task_prova,t,'Name','orientation_arm');
 
 save('timeseries_task_tot', 'position_arm', 'orientation_arm')
