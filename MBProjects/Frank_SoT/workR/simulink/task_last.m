%task definitivo
Tsim = 15;
t = 0:0.02:Tsim;


radius = 0.3360;
omega = 0.1;

x = radius*cos(omega*t - pi/2);
y = radius*sin(omega*t - pi/2);
z = 0*t;

y2 = radius*sin(omega*t - pi);
z2 = radius*cos(omega*t - pi);
x2 = 0*t;

rot_X = [1 0 0; 0 cos(pi/4 + pi/2) -sin(pi/4 + pi/2); 0 sin(pi/4 + pi/2) cos(pi/4 + pi/2)];

Xt = zeros(3, length(t));

for i = 1:length(t)
    Xt(:,i) = rot_X*[x(i); y(i); z(i)] + [0 -0.2376 -0.0984]';
end

Xt2 = zeros(3, length(t));

for i = 1:length(t)
    Xt2(:,i) = [x2(i); y2(i); z2(i)];
end

% figure,
% for i = 1:length(t)
%     plot3(Xt2(1,i),Xt2(2,i),Xt2(3,i),'r--','LineWidth',5), hold on, grid on,
%     xlabel('x'), ylabel('y'), zlabel('z'),
%     xlim([-radius, radius]), ylim([-radius, radius]), zlim([-radius, radius])
%     pause(0.0001)
% end


quat_task1 = zeros(4, length(t));
 
 for i = 1:length(t)
     R_zt = [cos(omega*t(i)-pi/2) -sin(omega*t(i)-pi/2) 0; sin(omega*t(i)-pi/2) cos(omega*t(i)-pi/2) 0; 0 0 1];
     R_xt = [1 0 0; 0 cos(omega*t(i)) -sin(omega*t(i)); 0 sin(omega*t(i)) cos(omega*t(i))];
     quat_task1(:,i) = rot2quat(R_zt*R_xt);
 end
 
 quat_task2 = zeros(4, length(t));
 
 for i = 1:length(t)
     R_xt = [1 0 0; 0 cos(omega*t(i)) -sin(omega*t(i)); 0 sin(omega*t(i)) cos(omega*t(i))];
     quat_task2(:,i) = rot2quat(R_xt);
 end
 
 quat_t = quat_task2;
 
 
 % timeseries
 
 position_arm = timeseries(Xt2,t,'Name','position_arm');
 orientation_arm = timeseries(quat_t,t,'Name','orientation_arm');
 
 save('timeseries_task_tot', 'position_arm', 'orientation_arm')
