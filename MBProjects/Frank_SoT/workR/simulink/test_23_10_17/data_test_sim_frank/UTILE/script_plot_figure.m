% in this script plot all data from ROS sensor,

% call the function defined below 
plot_data = @plot_figure;

files = dir('*.mat');

for j = 1:length(files)
    % load data
    load(files(j).name)
    
    % make directory where saving fidures .fig and .png
    folder = [files(j).name '_deg'];
    mkdir(folder);
    
    conversion = 180/pi;
    % name of joints
    body = {'Shoulder', 'Arm', 'Elbow','Forearm','Hand', 'nek','head', 'Shoulder', 'Arm', 'Elbow','Forearm','Hand'};
    
    
    figure,
    plot3(CoM_SoT(:,1), CoM_SoT(:,2), CoM_SoT(:,3),'r', 'LineWidth', 2),
    xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]'), grid on
    title('CoM')
    savefig([folder '/CoM.fig'])
    saveas(gcf, [folder '/CoM.png'], 'png')
    
    % plotting each joint
    for i = 1:12
        
        if(i <= 5)
            rl = ' right';
        else
            rl = ' left';
        end
        
        if(i <= 5 || i >= 8)
            plot_data(time, conversion*q_SoT, i, cell2mat([body(i) rl]), cell2mat([folder '/' body(i) rl]));
        end
    end
    
    
end

% utility function used for plotting data
function plot_figure(time, q_des, index, body_name, fig_name)

if(index == 1 || index == 3 || index == 5 || index >= 10)
    coef = -1;
else
    coef = 1;
end


ylb = min(coef.*q_des(:,index)) -0.05;

yub = max(coef.*q_des(:,index)) +0.05;


figure 
plot(time,coef.*q_des(:,index),'r','LineWidth',2),
xlabel('time [s]'), ylabel('q [deg]'), ylim([ylb yub])
title(body_name)

savefig([fig_name '.fig'])
saveas(gcf, [fig_name '.png'], 'png')
end