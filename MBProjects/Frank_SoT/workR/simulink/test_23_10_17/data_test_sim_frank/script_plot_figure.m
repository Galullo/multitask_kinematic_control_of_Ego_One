% in this script plot all data from ROS sensor,

% call the function defined below 
plot_data = @plot_figure;

% load data
load('eperimento_alzata_gomito.mat')

% make directory where saving fidures .fig and .png
folder = 'figures_alzata_gomito_deg';
mkdir(folder);

conversion = 180/pi;
% name of joints
body = {'Shoulder', 'Arm', 'Elbow','Forearm','Hand', 'nek','head', 'Shoulder', 'Arm', 'Elbow','Forearm','Hand'};
% plotting each joint
for i = 1:12
   
    if(i <= 5)
        rl = ' right';
    else
        rl = ' left';
    end
    
    if(i <= 5 || i >= 8)
        plot_data(time, conversion*q_SoT, i, cell2mat([body(i) rl]), cell2mat([folder '/' body(i) rl]));
    end
end




% utility function used for plotting data
function plot_figure(time, q_des, index, body_name, fig_name)

if(index == 1 || index == 3 || index == 5 || index >= 10)
    coef = -1;
else
    coef = 1;
end


ylb = min(coef.*q_des(:,index)) -0.05;

yub = max(coef.*q_des(:,index)) +0.05;


figure 
plot(time,coef.*q_des(:,index),'r','LineWidth',2),
xlabel('time [s]'), ylabel('q [deg]'), ylim([ylb yub])
title(body_name)

savefig([fig_name '.fig'])
saveas(gcf, [fig_name '.png'], 'png')
end