
files = dir('*.bag');
folder = 'file_mat_good';
mkdir(folder);
for i = 1:length(files)
    bag = rosbag(files(i).name);
    
    startT = bag.StartTime;
    endT = startT + 100;
    
    
    measurement_L_bag_topic = select(bag, 'Time', [startT endT], 'Topic', '/measure_L');
    measurement_L_bag = select(measurement_L_bag_topic, 'Time',[startT endT]);
    
    measure_L = timeseries(measurement_L_bag);
    if(length(measure_L) ~= 0)
        time = measure_L.Time - ones(size(measure_L.Time))*measure_L.Time(1);
        
        % take only pose data
        measure_L = timeseries(measure_L.Data, time, 'Name','q_measured_left');
    end
    % --------------------------------------------------------------- %
    
    measurement_R_bag_topic = select(bag, 'Time', [startT endT], 'Topic', '/measure_R');
    measurement_R_bag = select(measurement_R_bag_topic, 'Time',[startT endT]);
    
    measure_R = timeseries(measurement_R_bag);
    if(length(measure_R)~= 0)
        time = measure_R.Time - ones(size(measure_R.Time))*measure_R.Time(1);
        
        % take only pose data
        measure_R = timeseries(measure_R.Data, time, 'Name','q_measured_right');
    end
    % --------------------------------------------------------------- %
    
    q_des_left_bag_topic = select(bag, 'Time', [startT endT], 'Topic', '/frank_q_des_left');
    q_des_left_bag = select(q_des_left_bag_topic, 'Time',[startT endT]);
    
    q_des_left = timeseries(q_des_left_bag);
    if(length(q_des_left) ~= 0)
        time = q_des_left.Time - ones(size(q_des_left.Time))*q_des_left.Time(1);
        
        % take only pose data
        q_des_left = timeseries(q_des_left.Data, time, 'Name','q_des_left');
    end
    % --------------------------------------------------------------------%
    
    q_des_right_bag_topic = select(bag, 'Time', [startT endT], 'Topic', '/frank_q_des_right');
    q_des_right_bag = select(q_des_right_bag_topic, 'Time',[startT endT]);
    
    q_des_right = timeseries(q_des_right_bag);
    if(length(q_des_right)~= 0)
        time = q_des_right.Time - ones(size(q_des_right.Time))*q_des_right.Time(1);
        
        % take only pose data
        q_des_right = timeseries(q_des_right.Data, time, 'Name','q_des_right');
    end
    
    [path, name, ext] = fileparts(files(i).name);
    save([folder '/' name '.mat'],'measure_L', 'measure_R','q_des_left','q_des_right');
    
end
clear
