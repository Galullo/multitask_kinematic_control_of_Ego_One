% in this script plot all data from ROS sensor,

% call the function defined below 
plot_data = @plot_figure;

files = dir('*.mat');

% init_Frank
addpath('/home/parallels/.robotran/mbsysc/MBsysC/build/lib')

mbs_load('Frank_SoT');

prjname = 'Frank_SoT';

% I use this in controller function
rob_str.dpt = MBS_data.dpt;
rob_str.m = MBS_data.m;

for j = 1:length(files)
    % load data
    load(files(j).name),
    
    conversion = 180/pi;
    % make directory where saving fidures .fig and .png
    folder = [files(j).name '_deg_COM'];
    
    mkdir(folder);
    
    % name of joints
    body = {'Shoulder', 'Arm', 'Elbow','Forearm','Hand'};
    
    %--------------------------------------------------------------------%
    % step of simulation
    Ts = measure_L.Time(2);
    
    % simulate the robotran model for estimating the COM
    Tsim = measure_L.Time(end);
    warning off
    sim('calcolo_COM_from_DATI', Tsim),
    
    xlb = min(min(CoM.Data(:,1)), min(CoM_real.Data(:,1))) - 0.005;
    xub = max(max(CoM.Data(:,1)), max(CoM_real.Data(:,1))) + 0.005;
    ylb = min(min(CoM.Data(:,2)), min(CoM_real.Data(:,2))) - 0.005;
    yub = max(max(CoM.Data(:,2)), max(CoM_real.Data(:,2))) + 0.005;
    zlb = min(min(CoM.Data(:,3)), min(CoM_real.Data(:,3))) - 0.005;
    zub = max(max(CoM.Data(:,3)), max(CoM_real.Data(:,3))) + 0.005;
    
    figure,
    plot3(CoM.Data(:,1), CoM.Data(:,2), CoM.Data(:,3),'r', 'LineWidth', 2), hold on,
    plot3(CoM.Data(1,1), CoM.Data(1,2), CoM.Data(1,3),'ro', 'LineWidth', 2),
    xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]'), grid on
    xlim([xlb xub]), ylim([ylb yub]), zlim([zlb zub])
    title('CoM')
    savefig([folder '/CoM.fig'])
    saveas(gcf, [folder '/CoM.png'], 'png')
    
    figure,
    plot3(CoM_real.Data(:,1), CoM_real.Data(:,2), CoM_real.Data(:,3),'b', 'LineWidth', 2), hold on,
    plot3(CoM_real.Data(1,1), CoM_real.Data(1,2), CoM_real.Data(1,3),'bo', 'LineWidth', 2),
    xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]'), grid on
    xlim([xlb xub]), ylim([ylb yub]), zlim([zlb zub])
    title('CoM real')
    savefig([folder '/CoM_real.fig'])
    saveas(gcf, [folder '/CoM_real.png'], 'png')
    
    figure,
    plot3(CoM.Data(:,1), CoM.Data(:,2), CoM.Data(:,3),'r', 'LineWidth', 2), hold on
    plot3(CoM.Data(1,1), CoM.Data(1,2), CoM.Data(1,3),'ro', 'LineWidth', 2), hold on
    plot3(CoM_real.Data(:,1), CoM_real.Data(:,2), CoM_real.Data(:,3),'b', 'LineWidth', 2), hold on,
    plot3(CoM_real.Data(1,1), CoM_real.Data(1,2), CoM_real.Data(1,3),'bo', 'LineWidth', 2), hold on,
    xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]'), grid on
    legend('simulation', 'start', 'real', 'start')
    xlim([xlb xub]), ylim([ylb yub]), zlim([zlb zub])
   
    savefig([folder '/CoM_vs_CoM_real.fig'])
    saveas(gcf, [folder '/CoM_vs_CoM_real.png'], 'png')
    %--------------------------------------------------------------------%
    
    % plotting each joint
    for i = 1:5
        plot_data(conversion*measure_L, conversion*q_des_left,i,cell2mat([body(i) ' left']) , cell2mat([folder '/' body(i) '_left']));
        plot_data(conversion*measure_R, conversion*q_des_right,i, cell2mat([body(i) ' right']), cell2mat([folder '/' body(i) '_right']));
    end

    close all,
end


% utility function used for plotting data
function plot_figure(measure, q_des, index, body_name, fig_name)

ylb = min(min(measure.Data(:,index)), min(q_des.Data(:,index))) -0.05;

yub = max(max(measure.Data(:,index)), max(q_des.Data(:,index))) +0.05;

figure 
subplot(1,2,1)
plot(measure.Time,measure.Data(:,index),'b','LineWidth',2),
xlabel('time [s]'), ylabel('q [deg]'), ylim([ylb yub])

subplot(1,2,2)
plot(q_des.Time,q_des.Data(:,index),'r','LineWidth',2),
xlabel('time [s]'), ylabel('q [deg]'), ylim([ylb yub])
suptitle(body_name)

savefig([fig_name '.fig'])
saveas(gcf, [fig_name '.png'], 'png')
end