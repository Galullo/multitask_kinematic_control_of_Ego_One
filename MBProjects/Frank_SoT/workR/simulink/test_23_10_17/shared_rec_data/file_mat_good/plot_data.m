load('test_alzata_braccia_wholebody_2017-10-23-18-29-13.mat'),

%% shulder left joint
figure, 
subplot(1,2,1)
plot(measure_L.Time,measure_L.Data(:,1),'b','LineWidth',2),
xlabel('time [s]'), ylabel('q [rad]'), ylim([-0.1 0.9])

subplot(1,2,2)
plot(q_des_left.Time,q_des_left.Data(:,1),'r','LineWidth',2),
xlabel('time [s]'), ylabel('q [rad]'), ylim([-0.1 0.9])
suptitle('Shulder')

%% arm left joint
figure, 
subplot(1,2,1)
plot(measure_L.Time,measure_L.Data(:,2),'b','LineWidth',2),
xlabel('time [s]'), ylabel('q [rad]'), ylim([-0.2 0.1])

subplot(1,2,2)
plot(q_des_left.Time,q_des_left.Data(:,2),'r','LineWidth',2),
xlabel('time [s]'), ylabel('q [rad]'), ylim([-0.2 0.1])
suptitle('Arm')

%% elbow left joint
figure, 
subplot(1,2,1)
plot(measure_L.Time,measure_L.Data(:,3),'b','LineWidth',2),
xlabel('time [s]'), ylabel('q [rad]'), ylim([-0.2 0.1])

subplot(1,2,2)
plot(q_des_left.Time,q_des_left.Data(:,3),'r','LineWidth',2),
xlabel('time [s]'), ylabel('q [rad]'), ylim([-0.2 0.1])
suptitle('Elbow')

%% forarm left joint
figure, 
subplot(1,2,1)
plot(measure_L.Time,measure_L.Data(:,4),'b','LineWidth',2),
xlabel('time [s]'), ylabel('q [rad]'), ylim([-0.25 0.1])

subplot(1,2,2)
plot(q_des_left.Time,q_des_left.Data(:,4),'r','LineWidth',2),
xlabel('time [s]'), ylabel('q [rad]'), ylim([-0.25 0.1])
suptitle('forarm')

%% hand left joint
figure, 
subplot(1,2,1)
plot(measure_L.Time,measure_L.Data(:,5),'b','LineWidth',2),
xlabel('time [s]'), ylabel('q [rad]'), ylim([-0.1 0.1])

subplot(1,2,2)
plot(q_des_left.Time,q_des_left.Data(:,5),'r','LineWidth',2),
xlabel('time [s]'), ylabel('q [rad]'), ylim([-0.1 0.1])
suptitle('Hand')
