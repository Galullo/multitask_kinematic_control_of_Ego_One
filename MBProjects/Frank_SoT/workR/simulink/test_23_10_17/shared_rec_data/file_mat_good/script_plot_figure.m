% in this script plot all data from ROS sensor,

% call the function defined below 
plot_data = @plot_figure;

% load data
load('test_alzata_gomito_wholebody_2017-10-23-18-55-44.mat'),

conversion = 180/pi;
% make directory where saving fidures .fig and .png
folder = 'figures_alzata_gomito_wholebody_deg_COM';
mkdir(folder);

% name of joints
body = {'Shoulder', 'Arm', 'Elbow','Forearm','Hand'};

%------------------------------------------------------------------------%
% init_Frank
addpath('/home/parallels/.robotran/mbsysc/MBsysC/build/lib')

mbs_load('Frank_SoT');

prjname = 'Frank_SoT';

% I use this in controller function
rob_str.dpt = MBS_data.dpt;
rob_str.m = MBS_data.m;

% step of simulation
Ts = measure_L.Time(2);

% simulate the robotran model for estimating the COM
Tsim = measure_L.Time(end);
sim('calcolo_COM_from_DATI', Tsim),

figure,
plot3(simout1.Data(:,1), simout1.Data(:,2), simout1.Data(:,3),'b', 'LineWidth', 2),
xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]')
title('CoM' )
savefig([folder '/CoM.fig'])
saveas(gcf, [folder '/CoM.png'], 'png')
%------------------------------------------------------------------------%

% plotting each joint
for i = 1:5
    plot_data(conversion*measure_L, conversion*q_des_left,i,cell2mat([body(i) ' left']) , cell2mat([folder '/' body(i) '_left']));
    plot_data(conversion*measure_R, conversion*q_des_right,i, cell2mat([body(i) ' right']), cell2mat([folder '/' body(i) '_right']));
end




% utility function used for plotting data
function plot_figure(measure, q_des, index, body_name, fig_name)

ylb = min(min(measure.Data(:,index)), min(q_des.Data(:,index))) -0.05;

yub = max(max(measure.Data(:,index)), max(q_des.Data(:,index))) +0.05;

figure 
subplot(1,2,1)
plot(measure.Time,measure.Data(:,index),'b','LineWidth',2),
xlabel('time [s]'), ylabel('q [deg]'), ylim([ylb yub])

subplot(1,2,2)
plot(q_des.Time,q_des.Data(:,index),'r','LineWidth',2),
xlabel('time [s]'), ylabel('q [deg]'), ylim([ylb yub])
suptitle(body_name)

savefig([fig_name '.fig'])
saveas(gcf, [fig_name '.png'], 'png')
end