% Trajectory constraction

t = 0:0.02:31.06;
t2 = 0:0.02:30;

center_shu = [0 -0.168, 0.637];
radius = 0.3360;
omega = 0.05;
x = radius*sin(omega*t) + center_shu(1);
y = 0*t + center_shu(2);
z = radius*cos(omega*t) + center_shu(3);

center_elb = [0 0 -1; 0 1 0; 1 0 0]*[0, -0.1680, 0.4690]';
x2 = (radius/2)*cos(omega*t2) + center_elb(1);
y2 = (radius/2)*sin(omega*t2) + center_elb(2);
z2 = 0*t2 + center_elb(3);

xt = [x, x2];
yt = [y, y2];
zt = [z, z2];


figure,
for i = 1:5:(length(t) + length(t2))
    
    hold on,
%     plot3(x(i),y(i),z(i),'b--', 'LineWidth',3), hold on,
%     plot3(x2(i),y2(i),z2(i),'r--', 'LineWidth',3), grid on,
    plot3(xt(i), yt(i), zt(i),'b--', 'MarkerSize',8), hold on, grid on,
    xlabel('x'), ylabel('y'), zlabel('z')
    %xlim([-radius, radius]), ylim([-0.05, 0.05]), zlim([-radius, radius])
    
    pause(0.001);
end

position_arm = timeseries([x;y;z],t,'Name','position_arm');

quat_traj = zeros(4, length(t));
 
 for i = 1:length(t)
     quat_traj(:,i) = rotm2quat([cos(omega*t(i)), 0, sin(omega*t(i)); 0, 1, 0; -sin(omega*t(i)), 0, cos(omega*t(i))]);
 end
 
 orientation_arm = timeseries(quat_traj,t,'Name','orientation_arm');
