//
//-------------------------------------------------------------
//
//	ROBOTRAN - Version 6.6 (build : february 22, 2008)
//
//	Copyright 
//	Universite catholique de Louvain 
//	Departement de Mecanique 
//	Unite de Production Mecanique et Machines 
//	2, Place du Levant 
//	1348 Louvain-la-Neuve 
//	http://www.robotran.be// 
//
//	==> Generation Date : Mon Oct 30 21:34:35 2017
//
//	==> Project name : Frank_SoT_test2
//	==> using XML input file 
//
//	==> Number of joints : 20
//
//	==> Function : F 6 : Sensors Kinematical Informations (sens) 
//	==> Flops complexity : 3505
//
//	==> Generation Time :  0.050 seconds
//	==> Post-Processing :  0.060 seconds
//
//-------------------------------------------------------------
//
 
#include <math.h> 

#include "mbs_data.h"
#include "mbs_project_interface.h"
#include "mbs_sensor.h"
 
void  mbs_gensensor(MbsSensor *sens, 
              MbsData *s,
              int isens)
{ 
 
#include "mbs_gensensor_Frank_SoT_test2.h" 
#define q s->q 
#define qd s->qd 
#define qdd s->qdd 
 
 

// === begin imp_aux === 

// === end imp_aux === 

// ===== BEGIN task 0 ===== 
 
// Sensor Kinematics 



// = = Block_0_0_0_0_0_1 = = 
 
// Trigonometric Variables  

  C4 = cos(q[4]);
  S4 = sin(q[4]);
  C5 = cos(q[5]);
  S5 = sin(q[5]);
  C6 = cos(q[6]);
  S6 = sin(q[6]);

// = = Block_0_0_0_0_0_2 = = 
 
// Trigonometric Variables  

  C7 = cos(q[7]);
  S7 = sin(q[7]);

// = = Block_0_0_0_0_0_3 = = 
 
// Trigonometric Variables  

  C8 = cos(q[8]);
  S8 = sin(q[8]);

// = = Block_0_0_0_0_0_4 = = 
 
// Trigonometric Variables  

  C9 = cos(q[9]);
  S9 = sin(q[9]);
  C10 = cos(q[10]);
  S10 = sin(q[10]);
  C11 = cos(q[11]);
  S11 = sin(q[11]);
  C12 = cos(q[12]);
  S12 = sin(q[12]);
  C13 = cos(q[13]);
  S13 = sin(q[13]);

// = = Block_0_0_0_0_0_5 = = 
 
// Trigonometric Variables  

  C14 = cos(q[14]);
  S14 = sin(q[14]);
  C15 = cos(q[15]);
  S15 = sin(q[15]);

// = = Block_0_0_0_0_0_6 = = 
 
// Trigonometric Variables  

  C16 = cos(q[16]);
  S16 = sin(q[16]);
  C17 = cos(q[17]);
  S17 = sin(q[17]);
  C18 = cos(q[18]);
  S18 = sin(q[18]);
  C19 = cos(q[19]);
  S19 = sin(q[19]);
  C20 = cos(q[20]);
  S20 = sin(q[20]);

// ====== END Task 0 ====== 

// ===== BEGIN task 1 ===== 
 
switch(isens)
{
 
// 
break;
case 1:
 


// = = Block_1_0_0_1_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = q[1];
    sens->R[1][1] = (1.0);
    sens->R[2][2] = (1.0);
    sens->R[3][3] = (1.0);
    sens->V[1] = qd[1];
    sens->A[1] = qdd[1];
 
// 
break;
case 2:
 


// = = Block_1_0_0_2_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = q[1];
    sens->P[2] = q[2];
    sens->R[1][1] = (1.0);
    sens->R[2][2] = (1.0);
    sens->R[3][3] = (1.0);
    sens->V[1] = qd[1];
    sens->V[2] = qd[2];
    sens->A[1] = qdd[1];
    sens->A[2] = qdd[2];
 
// 
break;
case 3:
 


// = = Block_1_0_0_3_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = q[1];
    sens->P[2] = q[2];
    sens->P[3] = q[3];
    sens->R[1][1] = (1.0);
    sens->R[2][2] = (1.0);
    sens->R[3][3] = (1.0);
    sens->V[1] = qd[1];
    sens->V[2] = qd[2];
    sens->V[3] = qd[3];
    sens->A[1] = qdd[1];
    sens->A[2] = qdd[2];
    sens->A[3] = qdd[3];
 
// 
break;
case 4:
 


// = = Block_1_0_0_4_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = q[1];
    sens->P[2] = q[2];
    sens->P[3] = q[3];
    sens->R[1][1] = C4;
    sens->R[1][2] = S4;
    sens->R[2][1] = -S4;
    sens->R[2][2] = C4;
    sens->R[3][3] = (1.0);
    sens->V[1] = qd[1];
    sens->V[2] = qd[2];
    sens->V[3] = qd[3];
    sens->OM[3] = qd[4];
    sens->A[1] = qdd[1];
    sens->A[2] = qdd[2];
    sens->A[3] = qdd[3];
    sens->OMP[3] = qdd[4];
 
// 
break;
case 5:
 


// = = Block_1_0_0_5_0_1 = = 
 
// Sensor Kinematics 


    ROcp4_15 = C4*C5;
    ROcp4_25 = S4*C5;
    ROcp4_75 = C4*S5;
    ROcp4_85 = S4*S5;
    OMcp4_15 = -qd[5]*S4;
    OMcp4_25 = qd[5]*C4;
    OPcp4_15 = -(qdd[5]*S4+qd[4]*qd[5]*C4);
    OPcp4_25 = qdd[5]*C4-qd[4]*qd[5]*S4;

// = = Block_1_0_0_5_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = q[1];
    sens->P[2] = q[2];
    sens->P[3] = q[3];
    sens->R[1][1] = ROcp4_15;
    sens->R[1][2] = ROcp4_25;
    sens->R[1][3] = -S5;
    sens->R[2][1] = -S4;
    sens->R[2][2] = C4;
    sens->R[3][1] = ROcp4_75;
    sens->R[3][2] = ROcp4_85;
    sens->R[3][3] = C5;
    sens->V[1] = qd[1];
    sens->V[2] = qd[2];
    sens->V[3] = qd[3];
    sens->OM[1] = OMcp4_15;
    sens->OM[2] = OMcp4_25;
    sens->OM[3] = qd[4];
    sens->A[1] = qdd[1];
    sens->A[2] = qdd[2];
    sens->A[3] = qdd[3];
    sens->OMP[1] = OPcp4_15;
    sens->OMP[2] = OPcp4_25;
    sens->OMP[3] = qdd[4];
 
// 
break;
case 6:
 


// = = Block_1_0_0_6_0_1 = = 
 
// Sensor Kinematics 


    ROcp5_15 = C4*C5;
    ROcp5_25 = S4*C5;
    ROcp5_75 = C4*S5;
    ROcp5_85 = S4*S5;
    ROcp5_46 = ROcp5_75*S6-S4*C6;
    ROcp5_56 = ROcp5_85*S6+C4*C6;
    ROcp5_66 = C5*S6;
    ROcp5_76 = ROcp5_75*C6+S4*S6;
    ROcp5_86 = ROcp5_85*C6-C4*S6;
    ROcp5_96 = C5*C6;
    OMcp5_15 = -qd[5]*S4;
    OMcp5_25 = qd[5]*C4;
    OMcp5_16 = OMcp5_15+ROcp5_15*qd[6];
    OMcp5_26 = OMcp5_25+ROcp5_25*qd[6];
    OMcp5_36 = qd[4]-qd[6]*S5;
    OPcp5_16 = ROcp5_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp5_25*S5+ROcp5_25*qd[4]);
    OPcp5_26 = ROcp5_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp5_15*S5+ROcp5_15*qd[4]);
    OPcp5_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_6_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = q[1];
    sens->P[2] = q[2];
    sens->P[3] = q[3];
    sens->R[1][1] = ROcp5_15;
    sens->R[1][2] = ROcp5_25;
    sens->R[1][3] = -S5;
    sens->R[2][1] = ROcp5_46;
    sens->R[2][2] = ROcp5_56;
    sens->R[2][3] = ROcp5_66;
    sens->R[3][1] = ROcp5_76;
    sens->R[3][2] = ROcp5_86;
    sens->R[3][3] = ROcp5_96;
    sens->V[1] = qd[1];
    sens->V[2] = qd[2];
    sens->V[3] = qd[3];
    sens->OM[1] = OMcp5_16;
    sens->OM[2] = OMcp5_26;
    sens->OM[3] = OMcp5_36;
    sens->A[1] = qdd[1];
    sens->A[2] = qdd[2];
    sens->A[3] = qdd[3];
    sens->OMP[1] = OPcp5_16;
    sens->OMP[2] = OPcp5_26;
    sens->OMP[3] = OPcp5_36;
 
// 
break;
case 7:
 


// = = Block_1_0_0_7_0_1 = = 
 
// Sensor Kinematics 


    ROcp6_15 = C4*C5;
    ROcp6_25 = S4*C5;
    ROcp6_75 = C4*S5;
    ROcp6_85 = S4*S5;
    ROcp6_46 = ROcp6_75*S6-S4*C6;
    ROcp6_56 = ROcp6_85*S6+C4*C6;
    ROcp6_66 = C5*S6;
    ROcp6_76 = ROcp6_75*C6+S4*S6;
    ROcp6_86 = ROcp6_85*C6-C4*S6;
    ROcp6_96 = C5*C6;
    OMcp6_15 = -qd[5]*S4;
    OMcp6_25 = qd[5]*C4;
    OMcp6_16 = OMcp6_15+ROcp6_15*qd[6];
    OMcp6_26 = OMcp6_25+ROcp6_25*qd[6];
    OMcp6_36 = qd[4]-qd[6]*S5;
    OPcp6_16 = ROcp6_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp6_25*S5+ROcp6_25*qd[4]);
    OPcp6_26 = ROcp6_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp6_15*S5+ROcp6_15*qd[4]);
    OPcp6_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_7_0_2 = = 
 
// Sensor Kinematics 


    ROcp6_17 = ROcp6_15*C7-ROcp6_76*S7;
    ROcp6_27 = ROcp6_25*C7-ROcp6_86*S7;
    ROcp6_37 = -(ROcp6_96*S7+S5*C7);
    ROcp6_77 = ROcp6_15*S7+ROcp6_76*C7;
    ROcp6_87 = ROcp6_25*S7+ROcp6_86*C7;
    ROcp6_97 = ROcp6_96*C7-S5*S7;
    RLcp6_17 = ROcp6_46*s->dpt[2][2];
    RLcp6_27 = ROcp6_56*s->dpt[2][2];
    RLcp6_37 = ROcp6_66*s->dpt[2][2];
    POcp6_17 = RLcp6_17+q[1];
    POcp6_27 = RLcp6_27+q[2];
    POcp6_37 = RLcp6_37+q[3];
    OMcp6_17 = OMcp6_16+ROcp6_46*qd[7];
    OMcp6_27 = OMcp6_26+ROcp6_56*qd[7];
    OMcp6_37 = OMcp6_36+ROcp6_66*qd[7];
    ORcp6_17 = OMcp6_26*RLcp6_37-OMcp6_36*RLcp6_27;
    ORcp6_27 = -(OMcp6_16*RLcp6_37-OMcp6_36*RLcp6_17);
    ORcp6_37 = OMcp6_16*RLcp6_27-OMcp6_26*RLcp6_17;
    VIcp6_17 = ORcp6_17+qd[1];
    VIcp6_27 = ORcp6_27+qd[2];
    VIcp6_37 = ORcp6_37+qd[3];
    OPcp6_17 = OPcp6_16+ROcp6_46*qdd[7]+qd[7]*(OMcp6_26*ROcp6_66-OMcp6_36*ROcp6_56);
    OPcp6_27 = OPcp6_26+ROcp6_56*qdd[7]-qd[7]*(OMcp6_16*ROcp6_66-OMcp6_36*ROcp6_46);
    OPcp6_37 = OPcp6_36+ROcp6_66*qdd[7]+qd[7]*(OMcp6_16*ROcp6_56-OMcp6_26*ROcp6_46);
    ACcp6_17 = qdd[1]+OMcp6_26*ORcp6_37-OMcp6_36*ORcp6_27+OPcp6_26*RLcp6_37-OPcp6_36*RLcp6_27;
    ACcp6_27 = qdd[2]-OMcp6_16*ORcp6_37+OMcp6_36*ORcp6_17-OPcp6_16*RLcp6_37+OPcp6_36*RLcp6_17;
    ACcp6_37 = qdd[3]+OMcp6_16*ORcp6_27-OMcp6_26*ORcp6_17+OPcp6_16*RLcp6_27-OPcp6_26*RLcp6_17;

// = = Block_1_0_0_7_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp6_17;
    sens->P[2] = POcp6_27;
    sens->P[3] = POcp6_37;
    sens->R[1][1] = ROcp6_17;
    sens->R[1][2] = ROcp6_27;
    sens->R[1][3] = ROcp6_37;
    sens->R[2][1] = ROcp6_46;
    sens->R[2][2] = ROcp6_56;
    sens->R[2][3] = ROcp6_66;
    sens->R[3][1] = ROcp6_77;
    sens->R[3][2] = ROcp6_87;
    sens->R[3][3] = ROcp6_97;
    sens->V[1] = VIcp6_17;
    sens->V[2] = VIcp6_27;
    sens->V[3] = VIcp6_37;
    sens->OM[1] = OMcp6_17;
    sens->OM[2] = OMcp6_27;
    sens->OM[3] = OMcp6_37;
    sens->A[1] = ACcp6_17;
    sens->A[2] = ACcp6_27;
    sens->A[3] = ACcp6_37;
    sens->OMP[1] = OPcp6_17;
    sens->OMP[2] = OPcp6_27;
    sens->OMP[3] = OPcp6_37;
 
// 
break;
case 8:
 


// = = Block_1_0_0_8_0_1 = = 
 
// Sensor Kinematics 


    ROcp7_15 = C4*C5;
    ROcp7_25 = S4*C5;
    ROcp7_75 = C4*S5;
    ROcp7_85 = S4*S5;
    ROcp7_46 = ROcp7_75*S6-S4*C6;
    ROcp7_56 = ROcp7_85*S6+C4*C6;
    ROcp7_66 = C5*S6;
    ROcp7_76 = ROcp7_75*C6+S4*S6;
    ROcp7_86 = ROcp7_85*C6-C4*S6;
    ROcp7_96 = C5*C6;
    OMcp7_15 = -qd[5]*S4;
    OMcp7_25 = qd[5]*C4;
    OMcp7_16 = OMcp7_15+ROcp7_15*qd[6];
    OMcp7_26 = OMcp7_25+ROcp7_25*qd[6];
    OMcp7_36 = qd[4]-qd[6]*S5;
    OPcp7_16 = ROcp7_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp7_25*S5+ROcp7_25*qd[4]);
    OPcp7_26 = ROcp7_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp7_15*S5+ROcp7_15*qd[4]);
    OPcp7_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_8_0_3 = = 
 
// Sensor Kinematics 


    ROcp7_18 = ROcp7_15*C8-ROcp7_76*S8;
    ROcp7_28 = ROcp7_25*C8-ROcp7_86*S8;
    ROcp7_38 = -(ROcp7_96*S8+S5*C8);
    ROcp7_78 = ROcp7_15*S8+ROcp7_76*C8;
    ROcp7_88 = ROcp7_25*S8+ROcp7_86*C8;
    ROcp7_98 = ROcp7_96*C8-S5*S8;
    RLcp7_18 = ROcp7_46*s->dpt[2][3];
    RLcp7_28 = ROcp7_56*s->dpt[2][3];
    RLcp7_38 = ROcp7_66*s->dpt[2][3];
    POcp7_18 = RLcp7_18+q[1];
    POcp7_28 = RLcp7_28+q[2];
    POcp7_38 = RLcp7_38+q[3];
    OMcp7_18 = OMcp7_16+ROcp7_46*qd[8];
    OMcp7_28 = OMcp7_26+ROcp7_56*qd[8];
    OMcp7_38 = OMcp7_36+ROcp7_66*qd[8];
    ORcp7_18 = OMcp7_26*RLcp7_38-OMcp7_36*RLcp7_28;
    ORcp7_28 = -(OMcp7_16*RLcp7_38-OMcp7_36*RLcp7_18);
    ORcp7_38 = OMcp7_16*RLcp7_28-OMcp7_26*RLcp7_18;
    VIcp7_18 = ORcp7_18+qd[1];
    VIcp7_28 = ORcp7_28+qd[2];
    VIcp7_38 = ORcp7_38+qd[3];
    OPcp7_18 = OPcp7_16+ROcp7_46*qdd[8]+qd[8]*(OMcp7_26*ROcp7_66-OMcp7_36*ROcp7_56);
    OPcp7_28 = OPcp7_26+ROcp7_56*qdd[8]-qd[8]*(OMcp7_16*ROcp7_66-OMcp7_36*ROcp7_46);
    OPcp7_38 = OPcp7_36+ROcp7_66*qdd[8]+qd[8]*(OMcp7_16*ROcp7_56-OMcp7_26*ROcp7_46);
    ACcp7_18 = qdd[1]+OMcp7_26*ORcp7_38-OMcp7_36*ORcp7_28+OPcp7_26*RLcp7_38-OPcp7_36*RLcp7_28;
    ACcp7_28 = qdd[2]-OMcp7_16*ORcp7_38+OMcp7_36*ORcp7_18-OPcp7_16*RLcp7_38+OPcp7_36*RLcp7_18;
    ACcp7_38 = qdd[3]+OMcp7_16*ORcp7_28-OMcp7_26*ORcp7_18+OPcp7_16*RLcp7_28-OPcp7_26*RLcp7_18;

// = = Block_1_0_0_8_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp7_18;
    sens->P[2] = POcp7_28;
    sens->P[3] = POcp7_38;
    sens->R[1][1] = ROcp7_18;
    sens->R[1][2] = ROcp7_28;
    sens->R[1][3] = ROcp7_38;
    sens->R[2][1] = ROcp7_46;
    sens->R[2][2] = ROcp7_56;
    sens->R[2][3] = ROcp7_66;
    sens->R[3][1] = ROcp7_78;
    sens->R[3][2] = ROcp7_88;
    sens->R[3][3] = ROcp7_98;
    sens->V[1] = VIcp7_18;
    sens->V[2] = VIcp7_28;
    sens->V[3] = VIcp7_38;
    sens->OM[1] = OMcp7_18;
    sens->OM[2] = OMcp7_28;
    sens->OM[3] = OMcp7_38;
    sens->A[1] = ACcp7_18;
    sens->A[2] = ACcp7_28;
    sens->A[3] = ACcp7_38;
    sens->OMP[1] = OPcp7_18;
    sens->OMP[2] = OPcp7_28;
    sens->OMP[3] = OPcp7_38;
 
// 
break;
case 9:
 


// = = Block_1_0_0_9_0_1 = = 
 
// Sensor Kinematics 


    ROcp8_15 = C4*C5;
    ROcp8_25 = S4*C5;
    ROcp8_75 = C4*S5;
    ROcp8_85 = S4*S5;
    ROcp8_46 = ROcp8_75*S6-S4*C6;
    ROcp8_56 = ROcp8_85*S6+C4*C6;
    ROcp8_66 = C5*S6;
    ROcp8_76 = ROcp8_75*C6+S4*S6;
    ROcp8_86 = ROcp8_85*C6-C4*S6;
    ROcp8_96 = C5*C6;
    OMcp8_15 = -qd[5]*S4;
    OMcp8_25 = qd[5]*C4;
    OMcp8_16 = OMcp8_15+ROcp8_15*qd[6];
    OMcp8_26 = OMcp8_25+ROcp8_25*qd[6];
    OMcp8_36 = qd[4]-qd[6]*S5;
    OPcp8_16 = ROcp8_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp8_25*S5+ROcp8_25*qd[4]);
    OPcp8_26 = ROcp8_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp8_15*S5+ROcp8_15*qd[4]);
    OPcp8_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_9_0_4 = = 
 
// Sensor Kinematics 


    ROcp8_19 = ROcp8_15*C9-ROcp8_76*S9;
    ROcp8_29 = ROcp8_25*C9-ROcp8_86*S9;
    ROcp8_39 = -(ROcp8_96*S9+S5*C9);
    ROcp8_79 = ROcp8_15*S9+ROcp8_76*C9;
    ROcp8_89 = ROcp8_25*S9+ROcp8_86*C9;
    ROcp8_99 = ROcp8_96*C9-S5*S9;
    RLcp8_19 = ROcp8_46*s->dpt[2][4]+ROcp8_76*s->dpt[3][4];
    RLcp8_29 = ROcp8_56*s->dpt[2][4]+ROcp8_86*s->dpt[3][4];
    RLcp8_39 = ROcp8_66*s->dpt[2][4]+ROcp8_96*s->dpt[3][4];
    POcp8_19 = RLcp8_19+q[1];
    POcp8_29 = RLcp8_29+q[2];
    POcp8_39 = RLcp8_39+q[3];
    OMcp8_19 = OMcp8_16+ROcp8_46*qd[9];
    OMcp8_29 = OMcp8_26+ROcp8_56*qd[9];
    OMcp8_39 = OMcp8_36+ROcp8_66*qd[9];
    ORcp8_19 = OMcp8_26*RLcp8_39-OMcp8_36*RLcp8_29;
    ORcp8_29 = -(OMcp8_16*RLcp8_39-OMcp8_36*RLcp8_19);
    ORcp8_39 = OMcp8_16*RLcp8_29-OMcp8_26*RLcp8_19;
    VIcp8_19 = ORcp8_19+qd[1];
    VIcp8_29 = ORcp8_29+qd[2];
    VIcp8_39 = ORcp8_39+qd[3];
    OPcp8_19 = OPcp8_16+ROcp8_46*qdd[9]+qd[9]*(OMcp8_26*ROcp8_66-OMcp8_36*ROcp8_56);
    OPcp8_29 = OPcp8_26+ROcp8_56*qdd[9]-qd[9]*(OMcp8_16*ROcp8_66-OMcp8_36*ROcp8_46);
    OPcp8_39 = OPcp8_36+ROcp8_66*qdd[9]+qd[9]*(OMcp8_16*ROcp8_56-OMcp8_26*ROcp8_46);
    ACcp8_19 = qdd[1]+OMcp8_26*ORcp8_39-OMcp8_36*ORcp8_29+OPcp8_26*RLcp8_39-OPcp8_36*RLcp8_29;
    ACcp8_29 = qdd[2]-OMcp8_16*ORcp8_39+OMcp8_36*ORcp8_19-OPcp8_16*RLcp8_39+OPcp8_36*RLcp8_19;
    ACcp8_39 = qdd[3]+OMcp8_16*ORcp8_29-OMcp8_26*ORcp8_19+OPcp8_16*RLcp8_29-OPcp8_26*RLcp8_19;

// = = Block_1_0_0_9_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp8_19;
    sens->P[2] = POcp8_29;
    sens->P[3] = POcp8_39;
    sens->R[1][1] = ROcp8_19;
    sens->R[1][2] = ROcp8_29;
    sens->R[1][3] = ROcp8_39;
    sens->R[2][1] = ROcp8_46;
    sens->R[2][2] = ROcp8_56;
    sens->R[2][3] = ROcp8_66;
    sens->R[3][1] = ROcp8_79;
    sens->R[3][2] = ROcp8_89;
    sens->R[3][3] = ROcp8_99;
    sens->V[1] = VIcp8_19;
    sens->V[2] = VIcp8_29;
    sens->V[3] = VIcp8_39;
    sens->OM[1] = OMcp8_19;
    sens->OM[2] = OMcp8_29;
    sens->OM[3] = OMcp8_39;
    sens->A[1] = ACcp8_19;
    sens->A[2] = ACcp8_29;
    sens->A[3] = ACcp8_39;
    sens->OMP[1] = OPcp8_19;
    sens->OMP[2] = OPcp8_29;
    sens->OMP[3] = OPcp8_39;
 
// 
break;
case 10:
 


// = = Block_1_0_0_10_0_1 = = 
 
// Sensor Kinematics 


    ROcp9_15 = C4*C5;
    ROcp9_25 = S4*C5;
    ROcp9_75 = C4*S5;
    ROcp9_85 = S4*S5;
    ROcp9_46 = ROcp9_75*S6-S4*C6;
    ROcp9_56 = ROcp9_85*S6+C4*C6;
    ROcp9_66 = C5*S6;
    ROcp9_76 = ROcp9_75*C6+S4*S6;
    ROcp9_86 = ROcp9_85*C6-C4*S6;
    ROcp9_96 = C5*C6;
    OMcp9_15 = -qd[5]*S4;
    OMcp9_25 = qd[5]*C4;
    OMcp9_16 = OMcp9_15+ROcp9_15*qd[6];
    OMcp9_26 = OMcp9_25+ROcp9_25*qd[6];
    OMcp9_36 = qd[4]-qd[6]*S5;
    OPcp9_16 = ROcp9_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp9_25*S5+ROcp9_25*qd[4]);
    OPcp9_26 = ROcp9_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp9_15*S5+ROcp9_15*qd[4]);
    OPcp9_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_10_0_4 = = 
 
// Sensor Kinematics 


    ROcp9_19 = ROcp9_15*C9-ROcp9_76*S9;
    ROcp9_29 = ROcp9_25*C9-ROcp9_86*S9;
    ROcp9_39 = -(ROcp9_96*S9+S5*C9);
    ROcp9_79 = ROcp9_15*S9+ROcp9_76*C9;
    ROcp9_89 = ROcp9_25*S9+ROcp9_86*C9;
    ROcp9_99 = ROcp9_96*C9-S5*S9;
    ROcp9_410 = ROcp9_46*C10+ROcp9_79*S10;
    ROcp9_510 = ROcp9_56*C10+ROcp9_89*S10;
    ROcp9_610 = ROcp9_66*C10+ROcp9_99*S10;
    ROcp9_710 = -(ROcp9_46*S10-ROcp9_79*C10);
    ROcp9_810 = -(ROcp9_56*S10-ROcp9_89*C10);
    ROcp9_910 = -(ROcp9_66*S10-ROcp9_99*C10);
    RLcp9_19 = ROcp9_46*s->dpt[2][4]+ROcp9_76*s->dpt[3][4];
    RLcp9_29 = ROcp9_56*s->dpt[2][4]+ROcp9_86*s->dpt[3][4];
    RLcp9_39 = ROcp9_66*s->dpt[2][4]+ROcp9_96*s->dpt[3][4];
    POcp9_19 = RLcp9_19+q[1];
    POcp9_29 = RLcp9_29+q[2];
    POcp9_39 = RLcp9_39+q[3];
    OMcp9_19 = OMcp9_16+ROcp9_46*qd[9];
    OMcp9_29 = OMcp9_26+ROcp9_56*qd[9];
    OMcp9_39 = OMcp9_36+ROcp9_66*qd[9];
    ORcp9_19 = OMcp9_26*RLcp9_39-OMcp9_36*RLcp9_29;
    ORcp9_29 = -(OMcp9_16*RLcp9_39-OMcp9_36*RLcp9_19);
    ORcp9_39 = OMcp9_16*RLcp9_29-OMcp9_26*RLcp9_19;
    VIcp9_19 = ORcp9_19+qd[1];
    VIcp9_29 = ORcp9_29+qd[2];
    VIcp9_39 = ORcp9_39+qd[3];
    ACcp9_19 = qdd[1]+OMcp9_26*ORcp9_39-OMcp9_36*ORcp9_29+OPcp9_26*RLcp9_39-OPcp9_36*RLcp9_29;
    ACcp9_29 = qdd[2]-OMcp9_16*ORcp9_39+OMcp9_36*ORcp9_19-OPcp9_16*RLcp9_39+OPcp9_36*RLcp9_19;
    ACcp9_39 = qdd[3]+OMcp9_16*ORcp9_29-OMcp9_26*ORcp9_19+OPcp9_16*RLcp9_29-OPcp9_26*RLcp9_19;
    OMcp9_110 = OMcp9_19+ROcp9_19*qd[10];
    OMcp9_210 = OMcp9_29+ROcp9_29*qd[10];
    OMcp9_310 = OMcp9_39+ROcp9_39*qd[10];
    OPcp9_110 = OPcp9_16+ROcp9_19*qdd[10]+ROcp9_46*qdd[9]+qd[10]*(OMcp9_29*ROcp9_39-OMcp9_39*ROcp9_29)+qd[9]*(OMcp9_26*
 ROcp9_66-OMcp9_36*ROcp9_56);
    OPcp9_210 = OPcp9_26+ROcp9_29*qdd[10]+ROcp9_56*qdd[9]-qd[10]*(OMcp9_19*ROcp9_39-OMcp9_39*ROcp9_19)-qd[9]*(OMcp9_16*
 ROcp9_66-OMcp9_36*ROcp9_46);
    OPcp9_310 = OPcp9_36+ROcp9_39*qdd[10]+ROcp9_66*qdd[9]+qd[10]*(OMcp9_19*ROcp9_29-OMcp9_29*ROcp9_19)+qd[9]*(OMcp9_16*
 ROcp9_56-OMcp9_26*ROcp9_46);

// = = Block_1_0_0_10_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp9_19;
    sens->P[2] = POcp9_29;
    sens->P[3] = POcp9_39;
    sens->R[1][1] = ROcp9_19;
    sens->R[1][2] = ROcp9_29;
    sens->R[1][3] = ROcp9_39;
    sens->R[2][1] = ROcp9_410;
    sens->R[2][2] = ROcp9_510;
    sens->R[2][3] = ROcp9_610;
    sens->R[3][1] = ROcp9_710;
    sens->R[3][2] = ROcp9_810;
    sens->R[3][3] = ROcp9_910;
    sens->V[1] = VIcp9_19;
    sens->V[2] = VIcp9_29;
    sens->V[3] = VIcp9_39;
    sens->OM[1] = OMcp9_110;
    sens->OM[2] = OMcp9_210;
    sens->OM[3] = OMcp9_310;
    sens->A[1] = ACcp9_19;
    sens->A[2] = ACcp9_29;
    sens->A[3] = ACcp9_39;
    sens->OMP[1] = OPcp9_110;
    sens->OMP[2] = OPcp9_210;
    sens->OMP[3] = OPcp9_310;
 
// 
break;
case 11:
 


// = = Block_1_0_0_11_0_1 = = 
 
// Sensor Kinematics 


    ROcp10_15 = C4*C5;
    ROcp10_25 = S4*C5;
    ROcp10_75 = C4*S5;
    ROcp10_85 = S4*S5;
    ROcp10_46 = ROcp10_75*S6-S4*C6;
    ROcp10_56 = ROcp10_85*S6+C4*C6;
    ROcp10_66 = C5*S6;
    ROcp10_76 = ROcp10_75*C6+S4*S6;
    ROcp10_86 = ROcp10_85*C6-C4*S6;
    ROcp10_96 = C5*C6;
    OMcp10_15 = -qd[5]*S4;
    OMcp10_25 = qd[5]*C4;
    OMcp10_16 = OMcp10_15+ROcp10_15*qd[6];
    OMcp10_26 = OMcp10_25+ROcp10_25*qd[6];
    OMcp10_36 = qd[4]-qd[6]*S5;
    OPcp10_16 = ROcp10_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp10_25*S5+ROcp10_25*qd[4]);
    OPcp10_26 = ROcp10_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp10_15*S5+ROcp10_15*qd[4]);
    OPcp10_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_11_0_4 = = 
 
// Sensor Kinematics 


    ROcp10_19 = ROcp10_15*C9-ROcp10_76*S9;
    ROcp10_29 = ROcp10_25*C9-ROcp10_86*S9;
    ROcp10_39 = -(ROcp10_96*S9+S5*C9);
    ROcp10_79 = ROcp10_15*S9+ROcp10_76*C9;
    ROcp10_89 = ROcp10_25*S9+ROcp10_86*C9;
    ROcp10_99 = ROcp10_96*C9-S5*S9;
    ROcp10_410 = ROcp10_46*C10+ROcp10_79*S10;
    ROcp10_510 = ROcp10_56*C10+ROcp10_89*S10;
    ROcp10_610 = ROcp10_66*C10+ROcp10_99*S10;
    ROcp10_710 = -(ROcp10_46*S10-ROcp10_79*C10);
    ROcp10_810 = -(ROcp10_56*S10-ROcp10_89*C10);
    ROcp10_910 = -(ROcp10_66*S10-ROcp10_99*C10);
    ROcp10_111 = ROcp10_19*C11+ROcp10_410*S11;
    ROcp10_211 = ROcp10_29*C11+ROcp10_510*S11;
    ROcp10_311 = ROcp10_39*C11+ROcp10_610*S11;
    ROcp10_411 = -(ROcp10_19*S11-ROcp10_410*C11);
    ROcp10_511 = -(ROcp10_29*S11-ROcp10_510*C11);
    ROcp10_611 = -(ROcp10_39*S11-ROcp10_610*C11);
    RLcp10_19 = ROcp10_46*s->dpt[2][4]+ROcp10_76*s->dpt[3][4];
    RLcp10_29 = ROcp10_56*s->dpt[2][4]+ROcp10_86*s->dpt[3][4];
    RLcp10_39 = ROcp10_66*s->dpt[2][4]+ROcp10_96*s->dpt[3][4];
    OMcp10_19 = OMcp10_16+ROcp10_46*qd[9];
    OMcp10_29 = OMcp10_26+ROcp10_56*qd[9];
    OMcp10_39 = OMcp10_36+ROcp10_66*qd[9];
    ORcp10_19 = OMcp10_26*RLcp10_39-OMcp10_36*RLcp10_29;
    ORcp10_29 = -(OMcp10_16*RLcp10_39-OMcp10_36*RLcp10_19);
    ORcp10_39 = OMcp10_16*RLcp10_29-OMcp10_26*RLcp10_19;
    OMcp10_110 = OMcp10_19+ROcp10_19*qd[10];
    OMcp10_210 = OMcp10_29+ROcp10_29*qd[10];
    OMcp10_310 = OMcp10_39+ROcp10_39*qd[10];
    OPcp10_110 = OPcp10_16+ROcp10_19*qdd[10]+ROcp10_46*qdd[9]+qd[10]*(OMcp10_29*ROcp10_39-OMcp10_39*ROcp10_29)+qd[9]*(
 OMcp10_26*ROcp10_66-OMcp10_36*ROcp10_56);
    OPcp10_210 = OPcp10_26+ROcp10_29*qdd[10]+ROcp10_56*qdd[9]-qd[10]*(OMcp10_19*ROcp10_39-OMcp10_39*ROcp10_19)-qd[9]*(
 OMcp10_16*ROcp10_66-OMcp10_36*ROcp10_46);
    OPcp10_310 = OPcp10_36+ROcp10_39*qdd[10]+ROcp10_66*qdd[9]+qd[10]*(OMcp10_19*ROcp10_29-OMcp10_29*ROcp10_19)+qd[9]*(
 OMcp10_16*ROcp10_56-OMcp10_26*ROcp10_46);
    RLcp10_111 = ROcp10_710*s->dpt[3][11];
    RLcp10_211 = ROcp10_810*s->dpt[3][11];
    RLcp10_311 = ROcp10_910*s->dpt[3][11];
    POcp10_111 = RLcp10_111+RLcp10_19+q[1];
    POcp10_211 = RLcp10_211+RLcp10_29+q[2];
    POcp10_311 = RLcp10_311+RLcp10_39+q[3];
    OMcp10_111 = OMcp10_110+ROcp10_710*qd[11];
    OMcp10_211 = OMcp10_210+ROcp10_810*qd[11];
    OMcp10_311 = OMcp10_310+ROcp10_910*qd[11];
    ORcp10_111 = OMcp10_210*RLcp10_311-OMcp10_310*RLcp10_211;
    ORcp10_211 = -(OMcp10_110*RLcp10_311-OMcp10_310*RLcp10_111);
    ORcp10_311 = OMcp10_110*RLcp10_211-OMcp10_210*RLcp10_111;
    VIcp10_111 = ORcp10_111+ORcp10_19+qd[1];
    VIcp10_211 = ORcp10_211+ORcp10_29+qd[2];
    VIcp10_311 = ORcp10_311+ORcp10_39+qd[3];
    OPcp10_111 = OPcp10_110+ROcp10_710*qdd[11]+qd[11]*(OMcp10_210*ROcp10_910-OMcp10_310*ROcp10_810);
    OPcp10_211 = OPcp10_210+ROcp10_810*qdd[11]-qd[11]*(OMcp10_110*ROcp10_910-OMcp10_310*ROcp10_710);
    OPcp10_311 = OPcp10_310+ROcp10_910*qdd[11]+qd[11]*(OMcp10_110*ROcp10_810-OMcp10_210*ROcp10_710);
    ACcp10_111 = qdd[1]+OMcp10_210*ORcp10_311+OMcp10_26*ORcp10_39-OMcp10_310*ORcp10_211-OMcp10_36*ORcp10_29+OPcp10_210*
 RLcp10_311+OPcp10_26*RLcp10_39-OPcp10_310*RLcp10_211-OPcp10_36*RLcp10_29;
    ACcp10_211 = qdd[2]-OMcp10_110*ORcp10_311-OMcp10_16*ORcp10_39+OMcp10_310*ORcp10_111+OMcp10_36*ORcp10_19-OPcp10_110*
 RLcp10_311-OPcp10_16*RLcp10_39+OPcp10_310*RLcp10_111+OPcp10_36*RLcp10_19;
    ACcp10_311 = qdd[3]+OMcp10_110*ORcp10_211+OMcp10_16*ORcp10_29-OMcp10_210*ORcp10_111-OMcp10_26*ORcp10_19+OPcp10_110*
 RLcp10_211+OPcp10_16*RLcp10_29-OPcp10_210*RLcp10_111-OPcp10_26*RLcp10_19;

// = = Block_1_0_0_11_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp10_111;
    sens->P[2] = POcp10_211;
    sens->P[3] = POcp10_311;
    sens->R[1][1] = ROcp10_111;
    sens->R[1][2] = ROcp10_211;
    sens->R[1][3] = ROcp10_311;
    sens->R[2][1] = ROcp10_411;
    sens->R[2][2] = ROcp10_511;
    sens->R[2][3] = ROcp10_611;
    sens->R[3][1] = ROcp10_710;
    sens->R[3][2] = ROcp10_810;
    sens->R[3][3] = ROcp10_910;
    sens->V[1] = VIcp10_111;
    sens->V[2] = VIcp10_211;
    sens->V[3] = VIcp10_311;
    sens->OM[1] = OMcp10_111;
    sens->OM[2] = OMcp10_211;
    sens->OM[3] = OMcp10_311;
    sens->A[1] = ACcp10_111;
    sens->A[2] = ACcp10_211;
    sens->A[3] = ACcp10_311;
    sens->OMP[1] = OPcp10_111;
    sens->OMP[2] = OPcp10_211;
    sens->OMP[3] = OPcp10_311;
 
// 
break;
case 12:
 


// = = Block_1_0_0_12_0_1 = = 
 
// Sensor Kinematics 


    ROcp11_15 = C4*C5;
    ROcp11_25 = S4*C5;
    ROcp11_75 = C4*S5;
    ROcp11_85 = S4*S5;
    ROcp11_46 = ROcp11_75*S6-S4*C6;
    ROcp11_56 = ROcp11_85*S6+C4*C6;
    ROcp11_66 = C5*S6;
    ROcp11_76 = ROcp11_75*C6+S4*S6;
    ROcp11_86 = ROcp11_85*C6-C4*S6;
    ROcp11_96 = C5*C6;
    OMcp11_15 = -qd[5]*S4;
    OMcp11_25 = qd[5]*C4;
    OMcp11_16 = OMcp11_15+ROcp11_15*qd[6];
    OMcp11_26 = OMcp11_25+ROcp11_25*qd[6];
    OMcp11_36 = qd[4]-qd[6]*S5;
    OPcp11_16 = ROcp11_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp11_25*S5+ROcp11_25*qd[4]);
    OPcp11_26 = ROcp11_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp11_15*S5+ROcp11_15*qd[4]);
    OPcp11_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_12_0_4 = = 
 
// Sensor Kinematics 


    ROcp11_19 = ROcp11_15*C9-ROcp11_76*S9;
    ROcp11_29 = ROcp11_25*C9-ROcp11_86*S9;
    ROcp11_39 = -(ROcp11_96*S9+S5*C9);
    ROcp11_79 = ROcp11_15*S9+ROcp11_76*C9;
    ROcp11_89 = ROcp11_25*S9+ROcp11_86*C9;
    ROcp11_99 = ROcp11_96*C9-S5*S9;
    ROcp11_410 = ROcp11_46*C10+ROcp11_79*S10;
    ROcp11_510 = ROcp11_56*C10+ROcp11_89*S10;
    ROcp11_610 = ROcp11_66*C10+ROcp11_99*S10;
    ROcp11_710 = -(ROcp11_46*S10-ROcp11_79*C10);
    ROcp11_810 = -(ROcp11_56*S10-ROcp11_89*C10);
    ROcp11_910 = -(ROcp11_66*S10-ROcp11_99*C10);
    ROcp11_111 = ROcp11_19*C11+ROcp11_410*S11;
    ROcp11_211 = ROcp11_29*C11+ROcp11_510*S11;
    ROcp11_311 = ROcp11_39*C11+ROcp11_610*S11;
    ROcp11_411 = -(ROcp11_19*S11-ROcp11_410*C11);
    ROcp11_511 = -(ROcp11_29*S11-ROcp11_510*C11);
    ROcp11_611 = -(ROcp11_39*S11-ROcp11_610*C11);
    ROcp11_112 = ROcp11_111*C12-ROcp11_710*S12;
    ROcp11_212 = ROcp11_211*C12-ROcp11_810*S12;
    ROcp11_312 = ROcp11_311*C12-ROcp11_910*S12;
    ROcp11_712 = ROcp11_111*S12+ROcp11_710*C12;
    ROcp11_812 = ROcp11_211*S12+ROcp11_810*C12;
    ROcp11_912 = ROcp11_311*S12+ROcp11_910*C12;
    RLcp11_19 = ROcp11_46*s->dpt[2][4]+ROcp11_76*s->dpt[3][4];
    RLcp11_29 = ROcp11_56*s->dpt[2][4]+ROcp11_86*s->dpt[3][4];
    RLcp11_39 = ROcp11_66*s->dpt[2][4]+ROcp11_96*s->dpt[3][4];
    OMcp11_19 = OMcp11_16+ROcp11_46*qd[9];
    OMcp11_29 = OMcp11_26+ROcp11_56*qd[9];
    OMcp11_39 = OMcp11_36+ROcp11_66*qd[9];
    ORcp11_19 = OMcp11_26*RLcp11_39-OMcp11_36*RLcp11_29;
    ORcp11_29 = -(OMcp11_16*RLcp11_39-OMcp11_36*RLcp11_19);
    ORcp11_39 = OMcp11_16*RLcp11_29-OMcp11_26*RLcp11_19;
    OMcp11_110 = OMcp11_19+ROcp11_19*qd[10];
    OMcp11_210 = OMcp11_29+ROcp11_29*qd[10];
    OMcp11_310 = OMcp11_39+ROcp11_39*qd[10];
    OPcp11_110 = OPcp11_16+ROcp11_19*qdd[10]+ROcp11_46*qdd[9]+qd[10]*(OMcp11_29*ROcp11_39-OMcp11_39*ROcp11_29)+qd[9]*(
 OMcp11_26*ROcp11_66-OMcp11_36*ROcp11_56);
    OPcp11_210 = OPcp11_26+ROcp11_29*qdd[10]+ROcp11_56*qdd[9]-qd[10]*(OMcp11_19*ROcp11_39-OMcp11_39*ROcp11_19)-qd[9]*(
 OMcp11_16*ROcp11_66-OMcp11_36*ROcp11_46);
    OPcp11_310 = OPcp11_36+ROcp11_39*qdd[10]+ROcp11_66*qdd[9]+qd[10]*(OMcp11_19*ROcp11_29-OMcp11_29*ROcp11_19)+qd[9]*(
 OMcp11_16*ROcp11_56-OMcp11_26*ROcp11_46);
    RLcp11_111 = ROcp11_710*s->dpt[3][11];
    RLcp11_211 = ROcp11_810*s->dpt[3][11];
    RLcp11_311 = ROcp11_910*s->dpt[3][11];
    POcp11_111 = RLcp11_111+RLcp11_19+q[1];
    POcp11_211 = RLcp11_211+RLcp11_29+q[2];
    POcp11_311 = RLcp11_311+RLcp11_39+q[3];
    OMcp11_111 = OMcp11_110+ROcp11_710*qd[11];
    OMcp11_211 = OMcp11_210+ROcp11_810*qd[11];
    OMcp11_311 = OMcp11_310+ROcp11_910*qd[11];
    ORcp11_111 = OMcp11_210*RLcp11_311-OMcp11_310*RLcp11_211;
    ORcp11_211 = -(OMcp11_110*RLcp11_311-OMcp11_310*RLcp11_111);
    ORcp11_311 = OMcp11_110*RLcp11_211-OMcp11_210*RLcp11_111;
    VIcp11_111 = ORcp11_111+ORcp11_19+qd[1];
    VIcp11_211 = ORcp11_211+ORcp11_29+qd[2];
    VIcp11_311 = ORcp11_311+ORcp11_39+qd[3];
    ACcp11_111 = qdd[1]+OMcp11_210*ORcp11_311+OMcp11_26*ORcp11_39-OMcp11_310*ORcp11_211-OMcp11_36*ORcp11_29+OPcp11_210*
 RLcp11_311+OPcp11_26*RLcp11_39-OPcp11_310*RLcp11_211-OPcp11_36*RLcp11_29;
    ACcp11_211 = qdd[2]-OMcp11_110*ORcp11_311-OMcp11_16*ORcp11_39+OMcp11_310*ORcp11_111+OMcp11_36*ORcp11_19-OPcp11_110*
 RLcp11_311-OPcp11_16*RLcp11_39+OPcp11_310*RLcp11_111+OPcp11_36*RLcp11_19;
    ACcp11_311 = qdd[3]+OMcp11_110*ORcp11_211+OMcp11_16*ORcp11_29-OMcp11_210*ORcp11_111-OMcp11_26*ORcp11_19+OPcp11_110*
 RLcp11_211+OPcp11_16*RLcp11_29-OPcp11_210*RLcp11_111-OPcp11_26*RLcp11_19;
    OMcp11_112 = OMcp11_111+ROcp11_411*qd[12];
    OMcp11_212 = OMcp11_211+ROcp11_511*qd[12];
    OMcp11_312 = OMcp11_311+ROcp11_611*qd[12];
    OPcp11_112 = OPcp11_110+ROcp11_411*qdd[12]+ROcp11_710*qdd[11]+qd[11]*(OMcp11_210*ROcp11_910-OMcp11_310*ROcp11_810)+
 qd[12]*(OMcp11_211*ROcp11_611-OMcp11_311*ROcp11_511);
    OPcp11_212 = OPcp11_210+ROcp11_511*qdd[12]+ROcp11_810*qdd[11]-qd[11]*(OMcp11_110*ROcp11_910-OMcp11_310*ROcp11_710)-
 qd[12]*(OMcp11_111*ROcp11_611-OMcp11_311*ROcp11_411);
    OPcp11_312 = OPcp11_310+ROcp11_611*qdd[12]+ROcp11_910*qdd[11]+qd[11]*(OMcp11_110*ROcp11_810-OMcp11_210*ROcp11_710)+
 qd[12]*(OMcp11_111*ROcp11_511-OMcp11_211*ROcp11_411);

// = = Block_1_0_0_12_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp11_111;
    sens->P[2] = POcp11_211;
    sens->P[3] = POcp11_311;
    sens->R[1][1] = ROcp11_112;
    sens->R[1][2] = ROcp11_212;
    sens->R[1][3] = ROcp11_312;
    sens->R[2][1] = ROcp11_411;
    sens->R[2][2] = ROcp11_511;
    sens->R[2][3] = ROcp11_611;
    sens->R[3][1] = ROcp11_712;
    sens->R[3][2] = ROcp11_812;
    sens->R[3][3] = ROcp11_912;
    sens->V[1] = VIcp11_111;
    sens->V[2] = VIcp11_211;
    sens->V[3] = VIcp11_311;
    sens->OM[1] = OMcp11_112;
    sens->OM[2] = OMcp11_212;
    sens->OM[3] = OMcp11_312;
    sens->A[1] = ACcp11_111;
    sens->A[2] = ACcp11_211;
    sens->A[3] = ACcp11_311;
    sens->OMP[1] = OPcp11_112;
    sens->OMP[2] = OPcp11_212;
    sens->OMP[3] = OPcp11_312;
 
// 
break;
case 13:
 


// = = Block_1_0_0_13_0_1 = = 
 
// Sensor Kinematics 


    ROcp12_15 = C4*C5;
    ROcp12_25 = S4*C5;
    ROcp12_75 = C4*S5;
    ROcp12_85 = S4*S5;
    ROcp12_46 = ROcp12_75*S6-S4*C6;
    ROcp12_56 = ROcp12_85*S6+C4*C6;
    ROcp12_66 = C5*S6;
    ROcp12_76 = ROcp12_75*C6+S4*S6;
    ROcp12_86 = ROcp12_85*C6-C4*S6;
    ROcp12_96 = C5*C6;
    OMcp12_15 = -qd[5]*S4;
    OMcp12_25 = qd[5]*C4;
    OMcp12_16 = OMcp12_15+ROcp12_15*qd[6];
    OMcp12_26 = OMcp12_25+ROcp12_25*qd[6];
    OMcp12_36 = qd[4]-qd[6]*S5;
    OPcp12_16 = ROcp12_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp12_25*S5+ROcp12_25*qd[4]);
    OPcp12_26 = ROcp12_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp12_15*S5+ROcp12_15*qd[4]);
    OPcp12_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_13_0_4 = = 
 
// Sensor Kinematics 


    ROcp12_19 = ROcp12_15*C9-ROcp12_76*S9;
    ROcp12_29 = ROcp12_25*C9-ROcp12_86*S9;
    ROcp12_39 = -(ROcp12_96*S9+S5*C9);
    ROcp12_79 = ROcp12_15*S9+ROcp12_76*C9;
    ROcp12_89 = ROcp12_25*S9+ROcp12_86*C9;
    ROcp12_99 = ROcp12_96*C9-S5*S9;
    ROcp12_410 = ROcp12_46*C10+ROcp12_79*S10;
    ROcp12_510 = ROcp12_56*C10+ROcp12_89*S10;
    ROcp12_610 = ROcp12_66*C10+ROcp12_99*S10;
    ROcp12_710 = -(ROcp12_46*S10-ROcp12_79*C10);
    ROcp12_810 = -(ROcp12_56*S10-ROcp12_89*C10);
    ROcp12_910 = -(ROcp12_66*S10-ROcp12_99*C10);
    ROcp12_111 = ROcp12_19*C11+ROcp12_410*S11;
    ROcp12_211 = ROcp12_29*C11+ROcp12_510*S11;
    ROcp12_311 = ROcp12_39*C11+ROcp12_610*S11;
    ROcp12_411 = -(ROcp12_19*S11-ROcp12_410*C11);
    ROcp12_511 = -(ROcp12_29*S11-ROcp12_510*C11);
    ROcp12_611 = -(ROcp12_39*S11-ROcp12_610*C11);
    ROcp12_112 = ROcp12_111*C12-ROcp12_710*S12;
    ROcp12_212 = ROcp12_211*C12-ROcp12_810*S12;
    ROcp12_312 = ROcp12_311*C12-ROcp12_910*S12;
    ROcp12_712 = ROcp12_111*S12+ROcp12_710*C12;
    ROcp12_812 = ROcp12_211*S12+ROcp12_810*C12;
    ROcp12_912 = ROcp12_311*S12+ROcp12_910*C12;
    ROcp12_113 = ROcp12_112*C13+ROcp12_411*S13;
    ROcp12_213 = ROcp12_212*C13+ROcp12_511*S13;
    ROcp12_313 = ROcp12_312*C13+ROcp12_611*S13;
    ROcp12_413 = -(ROcp12_112*S13-ROcp12_411*C13);
    ROcp12_513 = -(ROcp12_212*S13-ROcp12_511*C13);
    ROcp12_613 = -(ROcp12_312*S13-ROcp12_611*C13);
    RLcp12_19 = ROcp12_46*s->dpt[2][4]+ROcp12_76*s->dpt[3][4];
    RLcp12_29 = ROcp12_56*s->dpt[2][4]+ROcp12_86*s->dpt[3][4];
    RLcp12_39 = ROcp12_66*s->dpt[2][4]+ROcp12_96*s->dpt[3][4];
    OMcp12_19 = OMcp12_16+ROcp12_46*qd[9];
    OMcp12_29 = OMcp12_26+ROcp12_56*qd[9];
    OMcp12_39 = OMcp12_36+ROcp12_66*qd[9];
    ORcp12_19 = OMcp12_26*RLcp12_39-OMcp12_36*RLcp12_29;
    ORcp12_29 = -(OMcp12_16*RLcp12_39-OMcp12_36*RLcp12_19);
    ORcp12_39 = OMcp12_16*RLcp12_29-OMcp12_26*RLcp12_19;
    OMcp12_110 = OMcp12_19+ROcp12_19*qd[10];
    OMcp12_210 = OMcp12_29+ROcp12_29*qd[10];
    OMcp12_310 = OMcp12_39+ROcp12_39*qd[10];
    OPcp12_110 = OPcp12_16+ROcp12_19*qdd[10]+ROcp12_46*qdd[9]+qd[10]*(OMcp12_29*ROcp12_39-OMcp12_39*ROcp12_29)+qd[9]*(
 OMcp12_26*ROcp12_66-OMcp12_36*ROcp12_56);
    OPcp12_210 = OPcp12_26+ROcp12_29*qdd[10]+ROcp12_56*qdd[9]-qd[10]*(OMcp12_19*ROcp12_39-OMcp12_39*ROcp12_19)-qd[9]*(
 OMcp12_16*ROcp12_66-OMcp12_36*ROcp12_46);
    OPcp12_310 = OPcp12_36+ROcp12_39*qdd[10]+ROcp12_66*qdd[9]+qd[10]*(OMcp12_19*ROcp12_29-OMcp12_29*ROcp12_19)+qd[9]*(
 OMcp12_16*ROcp12_56-OMcp12_26*ROcp12_46);
    RLcp12_111 = ROcp12_710*s->dpt[3][11];
    RLcp12_211 = ROcp12_810*s->dpt[3][11];
    RLcp12_311 = ROcp12_910*s->dpt[3][11];
    OMcp12_111 = OMcp12_110+ROcp12_710*qd[11];
    OMcp12_211 = OMcp12_210+ROcp12_810*qd[11];
    OMcp12_311 = OMcp12_310+ROcp12_910*qd[11];
    ORcp12_111 = OMcp12_210*RLcp12_311-OMcp12_310*RLcp12_211;
    ORcp12_211 = -(OMcp12_110*RLcp12_311-OMcp12_310*RLcp12_111);
    ORcp12_311 = OMcp12_110*RLcp12_211-OMcp12_210*RLcp12_111;
    OMcp12_112 = OMcp12_111+ROcp12_411*qd[12];
    OMcp12_212 = OMcp12_211+ROcp12_511*qd[12];
    OMcp12_312 = OMcp12_311+ROcp12_611*qd[12];
    OPcp12_112 = OPcp12_110+ROcp12_411*qdd[12]+ROcp12_710*qdd[11]+qd[11]*(OMcp12_210*ROcp12_910-OMcp12_310*ROcp12_810)+
 qd[12]*(OMcp12_211*ROcp12_611-OMcp12_311*ROcp12_511);
    OPcp12_212 = OPcp12_210+ROcp12_511*qdd[12]+ROcp12_810*qdd[11]-qd[11]*(OMcp12_110*ROcp12_910-OMcp12_310*ROcp12_710)-
 qd[12]*(OMcp12_111*ROcp12_611-OMcp12_311*ROcp12_411);
    OPcp12_312 = OPcp12_310+ROcp12_611*qdd[12]+ROcp12_910*qdd[11]+qd[11]*(OMcp12_110*ROcp12_810-OMcp12_210*ROcp12_710)+
 qd[12]*(OMcp12_111*ROcp12_511-OMcp12_211*ROcp12_411);
    RLcp12_113 = ROcp12_712*s->dpt[3][14];
    RLcp12_213 = ROcp12_812*s->dpt[3][14];
    RLcp12_313 = ROcp12_912*s->dpt[3][14];
    POcp12_113 = RLcp12_111+RLcp12_113+RLcp12_19+q[1];
    POcp12_213 = RLcp12_211+RLcp12_213+RLcp12_29+q[2];
    POcp12_313 = RLcp12_311+RLcp12_313+RLcp12_39+q[3];
    OMcp12_113 = OMcp12_112+ROcp12_712*qd[13];
    OMcp12_213 = OMcp12_212+ROcp12_812*qd[13];
    OMcp12_313 = OMcp12_312+ROcp12_912*qd[13];
    ORcp12_113 = OMcp12_212*RLcp12_313-OMcp12_312*RLcp12_213;
    ORcp12_213 = -(OMcp12_112*RLcp12_313-OMcp12_312*RLcp12_113);
    ORcp12_313 = OMcp12_112*RLcp12_213-OMcp12_212*RLcp12_113;
    VIcp12_113 = ORcp12_111+ORcp12_113+ORcp12_19+qd[1];
    VIcp12_213 = ORcp12_211+ORcp12_213+ORcp12_29+qd[2];
    VIcp12_313 = ORcp12_311+ORcp12_313+ORcp12_39+qd[3];
    OPcp12_113 = OPcp12_112+ROcp12_712*qdd[13]+qd[13]*(OMcp12_212*ROcp12_912-OMcp12_312*ROcp12_812);
    OPcp12_213 = OPcp12_212+ROcp12_812*qdd[13]-qd[13]*(OMcp12_112*ROcp12_912-OMcp12_312*ROcp12_712);
    OPcp12_313 = OPcp12_312+ROcp12_912*qdd[13]+qd[13]*(OMcp12_112*ROcp12_812-OMcp12_212*ROcp12_712);
    ACcp12_113 = qdd[1]+OMcp12_210*ORcp12_311+OMcp12_212*ORcp12_313+OMcp12_26*ORcp12_39-OMcp12_310*ORcp12_211-OMcp12_312*
 ORcp12_213-OMcp12_36*ORcp12_29+OPcp12_210*RLcp12_311+OPcp12_212*RLcp12_313+OPcp12_26*RLcp12_39-OPcp12_310*RLcp12_211-
 OPcp12_312*RLcp12_213-OPcp12_36*RLcp12_29;
    ACcp12_213 = qdd[2]-OMcp12_110*ORcp12_311-OMcp12_112*ORcp12_313-OMcp12_16*ORcp12_39+OMcp12_310*ORcp12_111+OMcp12_312*
 ORcp12_113+OMcp12_36*ORcp12_19-OPcp12_110*RLcp12_311-OPcp12_112*RLcp12_313-OPcp12_16*RLcp12_39+OPcp12_310*RLcp12_111+
 OPcp12_312*RLcp12_113+OPcp12_36*RLcp12_19;
    ACcp12_313 = qdd[3]+OMcp12_110*ORcp12_211+OMcp12_112*ORcp12_213+OMcp12_16*ORcp12_29-OMcp12_210*ORcp12_111-OMcp12_212*
 ORcp12_113-OMcp12_26*ORcp12_19+OPcp12_110*RLcp12_211+OPcp12_112*RLcp12_213+OPcp12_16*RLcp12_29-OPcp12_210*RLcp12_111-
 OPcp12_212*RLcp12_113-OPcp12_26*RLcp12_19;

// = = Block_1_0_0_13_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp12_113;
    sens->P[2] = POcp12_213;
    sens->P[3] = POcp12_313;
    sens->R[1][1] = ROcp12_113;
    sens->R[1][2] = ROcp12_213;
    sens->R[1][3] = ROcp12_313;
    sens->R[2][1] = ROcp12_413;
    sens->R[2][2] = ROcp12_513;
    sens->R[2][3] = ROcp12_613;
    sens->R[3][1] = ROcp12_712;
    sens->R[3][2] = ROcp12_812;
    sens->R[3][3] = ROcp12_912;
    sens->V[1] = VIcp12_113;
    sens->V[2] = VIcp12_213;
    sens->V[3] = VIcp12_313;
    sens->OM[1] = OMcp12_113;
    sens->OM[2] = OMcp12_213;
    sens->OM[3] = OMcp12_313;
    sens->A[1] = ACcp12_113;
    sens->A[2] = ACcp12_213;
    sens->A[3] = ACcp12_313;
    sens->OMP[1] = OPcp12_113;
    sens->OMP[2] = OPcp12_213;
    sens->OMP[3] = OPcp12_313;
 
// 
break;
case 14:
 


// = = Block_1_0_0_14_0_1 = = 
 
// Sensor Kinematics 


    ROcp13_15 = C4*C5;
    ROcp13_25 = S4*C5;
    ROcp13_75 = C4*S5;
    ROcp13_85 = S4*S5;
    ROcp13_46 = ROcp13_75*S6-S4*C6;
    ROcp13_56 = ROcp13_85*S6+C4*C6;
    ROcp13_66 = C5*S6;
    ROcp13_76 = ROcp13_75*C6+S4*S6;
    ROcp13_86 = ROcp13_85*C6-C4*S6;
    ROcp13_96 = C5*C6;
    OMcp13_15 = -qd[5]*S4;
    OMcp13_25 = qd[5]*C4;
    OMcp13_16 = OMcp13_15+ROcp13_15*qd[6];
    OMcp13_26 = OMcp13_25+ROcp13_25*qd[6];
    OMcp13_36 = qd[4]-qd[6]*S5;
    OPcp13_16 = ROcp13_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp13_25*S5+ROcp13_25*qd[4]);
    OPcp13_26 = ROcp13_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp13_15*S5+ROcp13_15*qd[4]);
    OPcp13_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_14_0_5 = = 
 
// Sensor Kinematics 


    ROcp13_114 = ROcp13_15*C14+ROcp13_46*S14;
    ROcp13_214 = ROcp13_25*C14+ROcp13_56*S14;
    ROcp13_314 = ROcp13_66*S14-C14*S5;
    ROcp13_414 = -(ROcp13_15*S14-ROcp13_46*C14);
    ROcp13_514 = -(ROcp13_25*S14-ROcp13_56*C14);
    ROcp13_614 = ROcp13_66*C14+S14*S5;
    RLcp13_114 = ROcp13_76*s->dpt[3][5];
    RLcp13_214 = ROcp13_86*s->dpt[3][5];
    RLcp13_314 = ROcp13_96*s->dpt[3][5];
    POcp13_114 = RLcp13_114+q[1];
    POcp13_214 = RLcp13_214+q[2];
    POcp13_314 = RLcp13_314+q[3];
    OMcp13_114 = OMcp13_16+ROcp13_76*qd[14];
    OMcp13_214 = OMcp13_26+ROcp13_86*qd[14];
    OMcp13_314 = OMcp13_36+ROcp13_96*qd[14];
    ORcp13_114 = OMcp13_26*RLcp13_314-OMcp13_36*RLcp13_214;
    ORcp13_214 = -(OMcp13_16*RLcp13_314-OMcp13_36*RLcp13_114);
    ORcp13_314 = OMcp13_16*RLcp13_214-OMcp13_26*RLcp13_114;
    VIcp13_114 = ORcp13_114+qd[1];
    VIcp13_214 = ORcp13_214+qd[2];
    VIcp13_314 = ORcp13_314+qd[3];
    OPcp13_114 = OPcp13_16+ROcp13_76*qdd[14]+qd[14]*(OMcp13_26*ROcp13_96-OMcp13_36*ROcp13_86);
    OPcp13_214 = OPcp13_26+ROcp13_86*qdd[14]-qd[14]*(OMcp13_16*ROcp13_96-OMcp13_36*ROcp13_76);
    OPcp13_314 = OPcp13_36+ROcp13_96*qdd[14]+qd[14]*(OMcp13_16*ROcp13_86-OMcp13_26*ROcp13_76);
    ACcp13_114 = qdd[1]+OMcp13_26*ORcp13_314-OMcp13_36*ORcp13_214+OPcp13_26*RLcp13_314-OPcp13_36*RLcp13_214;
    ACcp13_214 = qdd[2]-OMcp13_16*ORcp13_314+OMcp13_36*ORcp13_114-OPcp13_16*RLcp13_314+OPcp13_36*RLcp13_114;
    ACcp13_314 = qdd[3]+OMcp13_16*ORcp13_214-OMcp13_26*ORcp13_114+OPcp13_16*RLcp13_214-OPcp13_26*RLcp13_114;

// = = Block_1_0_0_14_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp13_114;
    sens->P[2] = POcp13_214;
    sens->P[3] = POcp13_314;
    sens->R[1][1] = ROcp13_114;
    sens->R[1][2] = ROcp13_214;
    sens->R[1][3] = ROcp13_314;
    sens->R[2][1] = ROcp13_414;
    sens->R[2][2] = ROcp13_514;
    sens->R[2][3] = ROcp13_614;
    sens->R[3][1] = ROcp13_76;
    sens->R[3][2] = ROcp13_86;
    sens->R[3][3] = ROcp13_96;
    sens->V[1] = VIcp13_114;
    sens->V[2] = VIcp13_214;
    sens->V[3] = VIcp13_314;
    sens->OM[1] = OMcp13_114;
    sens->OM[2] = OMcp13_214;
    sens->OM[3] = OMcp13_314;
    sens->A[1] = ACcp13_114;
    sens->A[2] = ACcp13_214;
    sens->A[3] = ACcp13_314;
    sens->OMP[1] = OPcp13_114;
    sens->OMP[2] = OPcp13_214;
    sens->OMP[3] = OPcp13_314;
 
// 
break;
case 15:
 


// = = Block_1_0_0_15_0_1 = = 
 
// Sensor Kinematics 


    ROcp14_15 = C4*C5;
    ROcp14_25 = S4*C5;
    ROcp14_75 = C4*S5;
    ROcp14_85 = S4*S5;
    ROcp14_46 = ROcp14_75*S6-S4*C6;
    ROcp14_56 = ROcp14_85*S6+C4*C6;
    ROcp14_66 = C5*S6;
    ROcp14_76 = ROcp14_75*C6+S4*S6;
    ROcp14_86 = ROcp14_85*C6-C4*S6;
    ROcp14_96 = C5*C6;
    OMcp14_15 = -qd[5]*S4;
    OMcp14_25 = qd[5]*C4;
    OMcp14_16 = OMcp14_15+ROcp14_15*qd[6];
    OMcp14_26 = OMcp14_25+ROcp14_25*qd[6];
    OMcp14_36 = qd[4]-qd[6]*S5;
    OPcp14_16 = ROcp14_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp14_25*S5+ROcp14_25*qd[4]);
    OPcp14_26 = ROcp14_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp14_15*S5+ROcp14_15*qd[4]);
    OPcp14_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_15_0_5 = = 
 
// Sensor Kinematics 


    ROcp14_114 = ROcp14_15*C14+ROcp14_46*S14;
    ROcp14_214 = ROcp14_25*C14+ROcp14_56*S14;
    ROcp14_314 = ROcp14_66*S14-C14*S5;
    ROcp14_414 = -(ROcp14_15*S14-ROcp14_46*C14);
    ROcp14_514 = -(ROcp14_25*S14-ROcp14_56*C14);
    ROcp14_614 = ROcp14_66*C14+S14*S5;
    ROcp14_115 = ROcp14_114*C15-ROcp14_76*S15;
    ROcp14_215 = ROcp14_214*C15-ROcp14_86*S15;
    ROcp14_315 = ROcp14_314*C15-ROcp14_96*S15;
    ROcp14_715 = ROcp14_114*S15+ROcp14_76*C15;
    ROcp14_815 = ROcp14_214*S15+ROcp14_86*C15;
    ROcp14_915 = ROcp14_314*S15+ROcp14_96*C15;
    RLcp14_114 = ROcp14_76*s->dpt[3][5];
    RLcp14_214 = ROcp14_86*s->dpt[3][5];
    RLcp14_314 = ROcp14_96*s->dpt[3][5];
    POcp14_114 = RLcp14_114+q[1];
    POcp14_214 = RLcp14_214+q[2];
    POcp14_314 = RLcp14_314+q[3];
    OMcp14_114 = OMcp14_16+ROcp14_76*qd[14];
    OMcp14_214 = OMcp14_26+ROcp14_86*qd[14];
    OMcp14_314 = OMcp14_36+ROcp14_96*qd[14];
    ORcp14_114 = OMcp14_26*RLcp14_314-OMcp14_36*RLcp14_214;
    ORcp14_214 = -(OMcp14_16*RLcp14_314-OMcp14_36*RLcp14_114);
    ORcp14_314 = OMcp14_16*RLcp14_214-OMcp14_26*RLcp14_114;
    VIcp14_114 = ORcp14_114+qd[1];
    VIcp14_214 = ORcp14_214+qd[2];
    VIcp14_314 = ORcp14_314+qd[3];
    ACcp14_114 = qdd[1]+OMcp14_26*ORcp14_314-OMcp14_36*ORcp14_214+OPcp14_26*RLcp14_314-OPcp14_36*RLcp14_214;
    ACcp14_214 = qdd[2]-OMcp14_16*ORcp14_314+OMcp14_36*ORcp14_114-OPcp14_16*RLcp14_314+OPcp14_36*RLcp14_114;
    ACcp14_314 = qdd[3]+OMcp14_16*ORcp14_214-OMcp14_26*ORcp14_114+OPcp14_16*RLcp14_214-OPcp14_26*RLcp14_114;
    OMcp14_115 = OMcp14_114+ROcp14_414*qd[15];
    OMcp14_215 = OMcp14_214+ROcp14_514*qd[15];
    OMcp14_315 = OMcp14_314+ROcp14_614*qd[15];
    OPcp14_115 = OPcp14_16+ROcp14_414*qdd[15]+ROcp14_76*qdd[14]+qd[14]*(OMcp14_26*ROcp14_96-OMcp14_36*ROcp14_86)+qd[15]*(
 OMcp14_214*ROcp14_614-OMcp14_314*ROcp14_514);
    OPcp14_215 = OPcp14_26+ROcp14_514*qdd[15]+ROcp14_86*qdd[14]-qd[14]*(OMcp14_16*ROcp14_96-OMcp14_36*ROcp14_76)-qd[15]*(
 OMcp14_114*ROcp14_614-OMcp14_314*ROcp14_414);
    OPcp14_315 = OPcp14_36+ROcp14_614*qdd[15]+ROcp14_96*qdd[14]+qd[14]*(OMcp14_16*ROcp14_86-OMcp14_26*ROcp14_76)+qd[15]*(
 OMcp14_114*ROcp14_514-OMcp14_214*ROcp14_414);

// = = Block_1_0_0_15_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp14_114;
    sens->P[2] = POcp14_214;
    sens->P[3] = POcp14_314;
    sens->R[1][1] = ROcp14_115;
    sens->R[1][2] = ROcp14_215;
    sens->R[1][3] = ROcp14_315;
    sens->R[2][1] = ROcp14_414;
    sens->R[2][2] = ROcp14_514;
    sens->R[2][3] = ROcp14_614;
    sens->R[3][1] = ROcp14_715;
    sens->R[3][2] = ROcp14_815;
    sens->R[3][3] = ROcp14_915;
    sens->V[1] = VIcp14_114;
    sens->V[2] = VIcp14_214;
    sens->V[3] = VIcp14_314;
    sens->OM[1] = OMcp14_115;
    sens->OM[2] = OMcp14_215;
    sens->OM[3] = OMcp14_315;
    sens->A[1] = ACcp14_114;
    sens->A[2] = ACcp14_214;
    sens->A[3] = ACcp14_314;
    sens->OMP[1] = OPcp14_115;
    sens->OMP[2] = OPcp14_215;
    sens->OMP[3] = OPcp14_315;
 
// 
break;
case 16:
 


// = = Block_1_0_0_16_0_1 = = 
 
// Sensor Kinematics 


    ROcp15_15 = C4*C5;
    ROcp15_25 = S4*C5;
    ROcp15_75 = C4*S5;
    ROcp15_85 = S4*S5;
    ROcp15_46 = ROcp15_75*S6-S4*C6;
    ROcp15_56 = ROcp15_85*S6+C4*C6;
    ROcp15_66 = C5*S6;
    ROcp15_76 = ROcp15_75*C6+S4*S6;
    ROcp15_86 = ROcp15_85*C6-C4*S6;
    ROcp15_96 = C5*C6;
    OMcp15_15 = -qd[5]*S4;
    OMcp15_25 = qd[5]*C4;
    OMcp15_16 = OMcp15_15+ROcp15_15*qd[6];
    OMcp15_26 = OMcp15_25+ROcp15_25*qd[6];
    OMcp15_36 = qd[4]-qd[6]*S5;
    OPcp15_16 = ROcp15_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp15_25*S5+ROcp15_25*qd[4]);
    OPcp15_26 = ROcp15_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp15_15*S5+ROcp15_15*qd[4]);
    OPcp15_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_16_0_6 = = 
 
// Sensor Kinematics 


    ROcp15_116 = ROcp15_15*C16-ROcp15_76*S16;
    ROcp15_216 = ROcp15_25*C16-ROcp15_86*S16;
    ROcp15_316 = -(ROcp15_96*S16+C16*S5);
    ROcp15_716 = ROcp15_15*S16+ROcp15_76*C16;
    ROcp15_816 = ROcp15_25*S16+ROcp15_86*C16;
    ROcp15_916 = ROcp15_96*C16-S16*S5;
    RLcp15_116 = ROcp15_46*s->dpt[2][6]+ROcp15_76*s->dpt[3][6];
    RLcp15_216 = ROcp15_56*s->dpt[2][6]+ROcp15_86*s->dpt[3][6];
    RLcp15_316 = ROcp15_66*s->dpt[2][6]+ROcp15_96*s->dpt[3][6];
    POcp15_116 = RLcp15_116+q[1];
    POcp15_216 = RLcp15_216+q[2];
    POcp15_316 = RLcp15_316+q[3];
    OMcp15_116 = OMcp15_16+ROcp15_46*qd[16];
    OMcp15_216 = OMcp15_26+ROcp15_56*qd[16];
    OMcp15_316 = OMcp15_36+ROcp15_66*qd[16];
    ORcp15_116 = OMcp15_26*RLcp15_316-OMcp15_36*RLcp15_216;
    ORcp15_216 = -(OMcp15_16*RLcp15_316-OMcp15_36*RLcp15_116);
    ORcp15_316 = OMcp15_16*RLcp15_216-OMcp15_26*RLcp15_116;
    VIcp15_116 = ORcp15_116+qd[1];
    VIcp15_216 = ORcp15_216+qd[2];
    VIcp15_316 = ORcp15_316+qd[3];
    OPcp15_116 = OPcp15_16+ROcp15_46*qdd[16]+qd[16]*(OMcp15_26*ROcp15_66-OMcp15_36*ROcp15_56);
    OPcp15_216 = OPcp15_26+ROcp15_56*qdd[16]-qd[16]*(OMcp15_16*ROcp15_66-OMcp15_36*ROcp15_46);
    OPcp15_316 = OPcp15_36+ROcp15_66*qdd[16]+qd[16]*(OMcp15_16*ROcp15_56-OMcp15_26*ROcp15_46);
    ACcp15_116 = qdd[1]+OMcp15_26*ORcp15_316-OMcp15_36*ORcp15_216+OPcp15_26*RLcp15_316-OPcp15_36*RLcp15_216;
    ACcp15_216 = qdd[2]-OMcp15_16*ORcp15_316+OMcp15_36*ORcp15_116-OPcp15_16*RLcp15_316+OPcp15_36*RLcp15_116;
    ACcp15_316 = qdd[3]+OMcp15_16*ORcp15_216-OMcp15_26*ORcp15_116+OPcp15_16*RLcp15_216-OPcp15_26*RLcp15_116;

// = = Block_1_0_0_16_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp15_116;
    sens->P[2] = POcp15_216;
    sens->P[3] = POcp15_316;
    sens->R[1][1] = ROcp15_116;
    sens->R[1][2] = ROcp15_216;
    sens->R[1][3] = ROcp15_316;
    sens->R[2][1] = ROcp15_46;
    sens->R[2][2] = ROcp15_56;
    sens->R[2][3] = ROcp15_66;
    sens->R[3][1] = ROcp15_716;
    sens->R[3][2] = ROcp15_816;
    sens->R[3][3] = ROcp15_916;
    sens->V[1] = VIcp15_116;
    sens->V[2] = VIcp15_216;
    sens->V[3] = VIcp15_316;
    sens->OM[1] = OMcp15_116;
    sens->OM[2] = OMcp15_216;
    sens->OM[3] = OMcp15_316;
    sens->A[1] = ACcp15_116;
    sens->A[2] = ACcp15_216;
    sens->A[3] = ACcp15_316;
    sens->OMP[1] = OPcp15_116;
    sens->OMP[2] = OPcp15_216;
    sens->OMP[3] = OPcp15_316;
 
// 
break;
case 17:
 


// = = Block_1_0_0_17_0_1 = = 
 
// Sensor Kinematics 


    ROcp16_15 = C4*C5;
    ROcp16_25 = S4*C5;
    ROcp16_75 = C4*S5;
    ROcp16_85 = S4*S5;
    ROcp16_46 = ROcp16_75*S6-S4*C6;
    ROcp16_56 = ROcp16_85*S6+C4*C6;
    ROcp16_66 = C5*S6;
    ROcp16_76 = ROcp16_75*C6+S4*S6;
    ROcp16_86 = ROcp16_85*C6-C4*S6;
    ROcp16_96 = C5*C6;
    OMcp16_15 = -qd[5]*S4;
    OMcp16_25 = qd[5]*C4;
    OMcp16_16 = OMcp16_15+ROcp16_15*qd[6];
    OMcp16_26 = OMcp16_25+ROcp16_25*qd[6];
    OMcp16_36 = qd[4]-qd[6]*S5;
    OPcp16_16 = ROcp16_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp16_25*S5+ROcp16_25*qd[4]);
    OPcp16_26 = ROcp16_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp16_15*S5+ROcp16_15*qd[4]);
    OPcp16_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_17_0_6 = = 
 
// Sensor Kinematics 


    ROcp16_116 = ROcp16_15*C16-ROcp16_76*S16;
    ROcp16_216 = ROcp16_25*C16-ROcp16_86*S16;
    ROcp16_316 = -(ROcp16_96*S16+C16*S5);
    ROcp16_716 = ROcp16_15*S16+ROcp16_76*C16;
    ROcp16_816 = ROcp16_25*S16+ROcp16_86*C16;
    ROcp16_916 = ROcp16_96*C16-S16*S5;
    ROcp16_417 = ROcp16_46*C17+ROcp16_716*S17;
    ROcp16_517 = ROcp16_56*C17+ROcp16_816*S17;
    ROcp16_617 = ROcp16_66*C17+ROcp16_916*S17;
    ROcp16_717 = -(ROcp16_46*S17-ROcp16_716*C17);
    ROcp16_817 = -(ROcp16_56*S17-ROcp16_816*C17);
    ROcp16_917 = -(ROcp16_66*S17-ROcp16_916*C17);
    RLcp16_116 = ROcp16_46*s->dpt[2][6]+ROcp16_76*s->dpt[3][6];
    RLcp16_216 = ROcp16_56*s->dpt[2][6]+ROcp16_86*s->dpt[3][6];
    RLcp16_316 = ROcp16_66*s->dpt[2][6]+ROcp16_96*s->dpt[3][6];
    POcp16_116 = RLcp16_116+q[1];
    POcp16_216 = RLcp16_216+q[2];
    POcp16_316 = RLcp16_316+q[3];
    OMcp16_116 = OMcp16_16+ROcp16_46*qd[16];
    OMcp16_216 = OMcp16_26+ROcp16_56*qd[16];
    OMcp16_316 = OMcp16_36+ROcp16_66*qd[16];
    ORcp16_116 = OMcp16_26*RLcp16_316-OMcp16_36*RLcp16_216;
    ORcp16_216 = -(OMcp16_16*RLcp16_316-OMcp16_36*RLcp16_116);
    ORcp16_316 = OMcp16_16*RLcp16_216-OMcp16_26*RLcp16_116;
    VIcp16_116 = ORcp16_116+qd[1];
    VIcp16_216 = ORcp16_216+qd[2];
    VIcp16_316 = ORcp16_316+qd[3];
    ACcp16_116 = qdd[1]+OMcp16_26*ORcp16_316-OMcp16_36*ORcp16_216+OPcp16_26*RLcp16_316-OPcp16_36*RLcp16_216;
    ACcp16_216 = qdd[2]-OMcp16_16*ORcp16_316+OMcp16_36*ORcp16_116-OPcp16_16*RLcp16_316+OPcp16_36*RLcp16_116;
    ACcp16_316 = qdd[3]+OMcp16_16*ORcp16_216-OMcp16_26*ORcp16_116+OPcp16_16*RLcp16_216-OPcp16_26*RLcp16_116;
    OMcp16_117 = OMcp16_116+ROcp16_116*qd[17];
    OMcp16_217 = OMcp16_216+ROcp16_216*qd[17];
    OMcp16_317 = OMcp16_316+ROcp16_316*qd[17];
    OPcp16_117 = OPcp16_16+ROcp16_116*qdd[17]+ROcp16_46*qdd[16]+qd[16]*(OMcp16_26*ROcp16_66-OMcp16_36*ROcp16_56)+qd[17]*(
 OMcp16_216*ROcp16_316-OMcp16_316*ROcp16_216);
    OPcp16_217 = OPcp16_26+ROcp16_216*qdd[17]+ROcp16_56*qdd[16]-qd[16]*(OMcp16_16*ROcp16_66-OMcp16_36*ROcp16_46)-qd[17]*(
 OMcp16_116*ROcp16_316-OMcp16_316*ROcp16_116);
    OPcp16_317 = OPcp16_36+ROcp16_316*qdd[17]+ROcp16_66*qdd[16]+qd[16]*(OMcp16_16*ROcp16_56-OMcp16_26*ROcp16_46)+qd[17]*(
 OMcp16_116*ROcp16_216-OMcp16_216*ROcp16_116);

// = = Block_1_0_0_17_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp16_116;
    sens->P[2] = POcp16_216;
    sens->P[3] = POcp16_316;
    sens->R[1][1] = ROcp16_116;
    sens->R[1][2] = ROcp16_216;
    sens->R[1][3] = ROcp16_316;
    sens->R[2][1] = ROcp16_417;
    sens->R[2][2] = ROcp16_517;
    sens->R[2][3] = ROcp16_617;
    sens->R[3][1] = ROcp16_717;
    sens->R[3][2] = ROcp16_817;
    sens->R[3][3] = ROcp16_917;
    sens->V[1] = VIcp16_116;
    sens->V[2] = VIcp16_216;
    sens->V[3] = VIcp16_316;
    sens->OM[1] = OMcp16_117;
    sens->OM[2] = OMcp16_217;
    sens->OM[3] = OMcp16_317;
    sens->A[1] = ACcp16_116;
    sens->A[2] = ACcp16_216;
    sens->A[3] = ACcp16_316;
    sens->OMP[1] = OPcp16_117;
    sens->OMP[2] = OPcp16_217;
    sens->OMP[3] = OPcp16_317;
 
// 
break;
case 18:
 


// = = Block_1_0_0_18_0_1 = = 
 
// Sensor Kinematics 


    ROcp17_15 = C4*C5;
    ROcp17_25 = S4*C5;
    ROcp17_75 = C4*S5;
    ROcp17_85 = S4*S5;
    ROcp17_46 = ROcp17_75*S6-S4*C6;
    ROcp17_56 = ROcp17_85*S6+C4*C6;
    ROcp17_66 = C5*S6;
    ROcp17_76 = ROcp17_75*C6+S4*S6;
    ROcp17_86 = ROcp17_85*C6-C4*S6;
    ROcp17_96 = C5*C6;
    OMcp17_15 = -qd[5]*S4;
    OMcp17_25 = qd[5]*C4;
    OMcp17_16 = OMcp17_15+ROcp17_15*qd[6];
    OMcp17_26 = OMcp17_25+ROcp17_25*qd[6];
    OMcp17_36 = qd[4]-qd[6]*S5;
    OPcp17_16 = ROcp17_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp17_25*S5+ROcp17_25*qd[4]);
    OPcp17_26 = ROcp17_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp17_15*S5+ROcp17_15*qd[4]);
    OPcp17_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_18_0_6 = = 
 
// Sensor Kinematics 


    ROcp17_116 = ROcp17_15*C16-ROcp17_76*S16;
    ROcp17_216 = ROcp17_25*C16-ROcp17_86*S16;
    ROcp17_316 = -(ROcp17_96*S16+C16*S5);
    ROcp17_716 = ROcp17_15*S16+ROcp17_76*C16;
    ROcp17_816 = ROcp17_25*S16+ROcp17_86*C16;
    ROcp17_916 = ROcp17_96*C16-S16*S5;
    ROcp17_417 = ROcp17_46*C17+ROcp17_716*S17;
    ROcp17_517 = ROcp17_56*C17+ROcp17_816*S17;
    ROcp17_617 = ROcp17_66*C17+ROcp17_916*S17;
    ROcp17_717 = -(ROcp17_46*S17-ROcp17_716*C17);
    ROcp17_817 = -(ROcp17_56*S17-ROcp17_816*C17);
    ROcp17_917 = -(ROcp17_66*S17-ROcp17_916*C17);
    ROcp17_118 = ROcp17_116*C18+ROcp17_417*S18;
    ROcp17_218 = ROcp17_216*C18+ROcp17_517*S18;
    ROcp17_318 = ROcp17_316*C18+ROcp17_617*S18;
    ROcp17_418 = -(ROcp17_116*S18-ROcp17_417*C18);
    ROcp17_518 = -(ROcp17_216*S18-ROcp17_517*C18);
    ROcp17_618 = -(ROcp17_316*S18-ROcp17_617*C18);
    RLcp17_116 = ROcp17_46*s->dpt[2][6]+ROcp17_76*s->dpt[3][6];
    RLcp17_216 = ROcp17_56*s->dpt[2][6]+ROcp17_86*s->dpt[3][6];
    RLcp17_316 = ROcp17_66*s->dpt[2][6]+ROcp17_96*s->dpt[3][6];
    OMcp17_116 = OMcp17_16+ROcp17_46*qd[16];
    OMcp17_216 = OMcp17_26+ROcp17_56*qd[16];
    OMcp17_316 = OMcp17_36+ROcp17_66*qd[16];
    ORcp17_116 = OMcp17_26*RLcp17_316-OMcp17_36*RLcp17_216;
    ORcp17_216 = -(OMcp17_16*RLcp17_316-OMcp17_36*RLcp17_116);
    ORcp17_316 = OMcp17_16*RLcp17_216-OMcp17_26*RLcp17_116;
    OMcp17_117 = OMcp17_116+ROcp17_116*qd[17];
    OMcp17_217 = OMcp17_216+ROcp17_216*qd[17];
    OMcp17_317 = OMcp17_316+ROcp17_316*qd[17];
    OPcp17_117 = OPcp17_16+ROcp17_116*qdd[17]+ROcp17_46*qdd[16]+qd[16]*(OMcp17_26*ROcp17_66-OMcp17_36*ROcp17_56)+qd[17]*(
 OMcp17_216*ROcp17_316-OMcp17_316*ROcp17_216);
    OPcp17_217 = OPcp17_26+ROcp17_216*qdd[17]+ROcp17_56*qdd[16]-qd[16]*(OMcp17_16*ROcp17_66-OMcp17_36*ROcp17_46)-qd[17]*(
 OMcp17_116*ROcp17_316-OMcp17_316*ROcp17_116);
    OPcp17_317 = OPcp17_36+ROcp17_316*qdd[17]+ROcp17_66*qdd[16]+qd[16]*(OMcp17_16*ROcp17_56-OMcp17_26*ROcp17_46)+qd[17]*(
 OMcp17_116*ROcp17_216-OMcp17_216*ROcp17_116);
    RLcp17_118 = ROcp17_717*s->dpt[3][19];
    RLcp17_218 = ROcp17_817*s->dpt[3][19];
    RLcp17_318 = ROcp17_917*s->dpt[3][19];
    POcp17_118 = RLcp17_116+RLcp17_118+q[1];
    POcp17_218 = RLcp17_216+RLcp17_218+q[2];
    POcp17_318 = RLcp17_316+RLcp17_318+q[3];
    OMcp17_118 = OMcp17_117+ROcp17_717*qd[18];
    OMcp17_218 = OMcp17_217+ROcp17_817*qd[18];
    OMcp17_318 = OMcp17_317+ROcp17_917*qd[18];
    ORcp17_118 = OMcp17_217*RLcp17_318-OMcp17_317*RLcp17_218;
    ORcp17_218 = -(OMcp17_117*RLcp17_318-OMcp17_317*RLcp17_118);
    ORcp17_318 = OMcp17_117*RLcp17_218-OMcp17_217*RLcp17_118;
    VIcp17_118 = ORcp17_116+ORcp17_118+qd[1];
    VIcp17_218 = ORcp17_216+ORcp17_218+qd[2];
    VIcp17_318 = ORcp17_316+ORcp17_318+qd[3];
    OPcp17_118 = OPcp17_117+ROcp17_717*qdd[18]+qd[18]*(OMcp17_217*ROcp17_917-OMcp17_317*ROcp17_817);
    OPcp17_218 = OPcp17_217+ROcp17_817*qdd[18]-qd[18]*(OMcp17_117*ROcp17_917-OMcp17_317*ROcp17_717);
    OPcp17_318 = OPcp17_317+ROcp17_917*qdd[18]+qd[18]*(OMcp17_117*ROcp17_817-OMcp17_217*ROcp17_717);
    ACcp17_118 = qdd[1]+OMcp17_217*ORcp17_318+OMcp17_26*ORcp17_316-OMcp17_317*ORcp17_218-OMcp17_36*ORcp17_216+OPcp17_217*
 RLcp17_318+OPcp17_26*RLcp17_316-OPcp17_317*RLcp17_218-OPcp17_36*RLcp17_216;
    ACcp17_218 = qdd[2]-OMcp17_117*ORcp17_318-OMcp17_16*ORcp17_316+OMcp17_317*ORcp17_118+OMcp17_36*ORcp17_116-OPcp17_117*
 RLcp17_318-OPcp17_16*RLcp17_316+OPcp17_317*RLcp17_118+OPcp17_36*RLcp17_116;
    ACcp17_318 = qdd[3]+OMcp17_117*ORcp17_218+OMcp17_16*ORcp17_216-OMcp17_217*ORcp17_118-OMcp17_26*ORcp17_116+OPcp17_117*
 RLcp17_218+OPcp17_16*RLcp17_216-OPcp17_217*RLcp17_118-OPcp17_26*RLcp17_116;

// = = Block_1_0_0_18_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp17_118;
    sens->P[2] = POcp17_218;
    sens->P[3] = POcp17_318;
    sens->R[1][1] = ROcp17_118;
    sens->R[1][2] = ROcp17_218;
    sens->R[1][3] = ROcp17_318;
    sens->R[2][1] = ROcp17_418;
    sens->R[2][2] = ROcp17_518;
    sens->R[2][3] = ROcp17_618;
    sens->R[3][1] = ROcp17_717;
    sens->R[3][2] = ROcp17_817;
    sens->R[3][3] = ROcp17_917;
    sens->V[1] = VIcp17_118;
    sens->V[2] = VIcp17_218;
    sens->V[3] = VIcp17_318;
    sens->OM[1] = OMcp17_118;
    sens->OM[2] = OMcp17_218;
    sens->OM[3] = OMcp17_318;
    sens->A[1] = ACcp17_118;
    sens->A[2] = ACcp17_218;
    sens->A[3] = ACcp17_318;
    sens->OMP[1] = OPcp17_118;
    sens->OMP[2] = OPcp17_218;
    sens->OMP[3] = OPcp17_318;
 
// 
break;
case 19:
 


// = = Block_1_0_0_19_0_1 = = 
 
// Sensor Kinematics 


    ROcp18_15 = C4*C5;
    ROcp18_25 = S4*C5;
    ROcp18_75 = C4*S5;
    ROcp18_85 = S4*S5;
    ROcp18_46 = ROcp18_75*S6-S4*C6;
    ROcp18_56 = ROcp18_85*S6+C4*C6;
    ROcp18_66 = C5*S6;
    ROcp18_76 = ROcp18_75*C6+S4*S6;
    ROcp18_86 = ROcp18_85*C6-C4*S6;
    ROcp18_96 = C5*C6;
    OMcp18_15 = -qd[5]*S4;
    OMcp18_25 = qd[5]*C4;
    OMcp18_16 = OMcp18_15+ROcp18_15*qd[6];
    OMcp18_26 = OMcp18_25+ROcp18_25*qd[6];
    OMcp18_36 = qd[4]-qd[6]*S5;
    OPcp18_16 = ROcp18_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp18_25*S5+ROcp18_25*qd[4]);
    OPcp18_26 = ROcp18_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp18_15*S5+ROcp18_15*qd[4]);
    OPcp18_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_19_0_6 = = 
 
// Sensor Kinematics 


    ROcp18_116 = ROcp18_15*C16-ROcp18_76*S16;
    ROcp18_216 = ROcp18_25*C16-ROcp18_86*S16;
    ROcp18_316 = -(ROcp18_96*S16+C16*S5);
    ROcp18_716 = ROcp18_15*S16+ROcp18_76*C16;
    ROcp18_816 = ROcp18_25*S16+ROcp18_86*C16;
    ROcp18_916 = ROcp18_96*C16-S16*S5;
    ROcp18_417 = ROcp18_46*C17+ROcp18_716*S17;
    ROcp18_517 = ROcp18_56*C17+ROcp18_816*S17;
    ROcp18_617 = ROcp18_66*C17+ROcp18_916*S17;
    ROcp18_717 = -(ROcp18_46*S17-ROcp18_716*C17);
    ROcp18_817 = -(ROcp18_56*S17-ROcp18_816*C17);
    ROcp18_917 = -(ROcp18_66*S17-ROcp18_916*C17);
    ROcp18_118 = ROcp18_116*C18+ROcp18_417*S18;
    ROcp18_218 = ROcp18_216*C18+ROcp18_517*S18;
    ROcp18_318 = ROcp18_316*C18+ROcp18_617*S18;
    ROcp18_418 = -(ROcp18_116*S18-ROcp18_417*C18);
    ROcp18_518 = -(ROcp18_216*S18-ROcp18_517*C18);
    ROcp18_618 = -(ROcp18_316*S18-ROcp18_617*C18);
    ROcp18_119 = ROcp18_118*C19-ROcp18_717*S19;
    ROcp18_219 = ROcp18_218*C19-ROcp18_817*S19;
    ROcp18_319 = ROcp18_318*C19-ROcp18_917*S19;
    ROcp18_719 = ROcp18_118*S19+ROcp18_717*C19;
    ROcp18_819 = ROcp18_218*S19+ROcp18_817*C19;
    ROcp18_919 = ROcp18_318*S19+ROcp18_917*C19;
    RLcp18_116 = ROcp18_46*s->dpt[2][6]+ROcp18_76*s->dpt[3][6];
    RLcp18_216 = ROcp18_56*s->dpt[2][6]+ROcp18_86*s->dpt[3][6];
    RLcp18_316 = ROcp18_66*s->dpt[2][6]+ROcp18_96*s->dpt[3][6];
    OMcp18_116 = OMcp18_16+ROcp18_46*qd[16];
    OMcp18_216 = OMcp18_26+ROcp18_56*qd[16];
    OMcp18_316 = OMcp18_36+ROcp18_66*qd[16];
    ORcp18_116 = OMcp18_26*RLcp18_316-OMcp18_36*RLcp18_216;
    ORcp18_216 = -(OMcp18_16*RLcp18_316-OMcp18_36*RLcp18_116);
    ORcp18_316 = OMcp18_16*RLcp18_216-OMcp18_26*RLcp18_116;
    OMcp18_117 = OMcp18_116+ROcp18_116*qd[17];
    OMcp18_217 = OMcp18_216+ROcp18_216*qd[17];
    OMcp18_317 = OMcp18_316+ROcp18_316*qd[17];
    OPcp18_117 = OPcp18_16+ROcp18_116*qdd[17]+ROcp18_46*qdd[16]+qd[16]*(OMcp18_26*ROcp18_66-OMcp18_36*ROcp18_56)+qd[17]*(
 OMcp18_216*ROcp18_316-OMcp18_316*ROcp18_216);
    OPcp18_217 = OPcp18_26+ROcp18_216*qdd[17]+ROcp18_56*qdd[16]-qd[16]*(OMcp18_16*ROcp18_66-OMcp18_36*ROcp18_46)-qd[17]*(
 OMcp18_116*ROcp18_316-OMcp18_316*ROcp18_116);
    OPcp18_317 = OPcp18_36+ROcp18_316*qdd[17]+ROcp18_66*qdd[16]+qd[16]*(OMcp18_16*ROcp18_56-OMcp18_26*ROcp18_46)+qd[17]*(
 OMcp18_116*ROcp18_216-OMcp18_216*ROcp18_116);
    RLcp18_118 = ROcp18_717*s->dpt[3][19];
    RLcp18_218 = ROcp18_817*s->dpt[3][19];
    RLcp18_318 = ROcp18_917*s->dpt[3][19];
    POcp18_118 = RLcp18_116+RLcp18_118+q[1];
    POcp18_218 = RLcp18_216+RLcp18_218+q[2];
    POcp18_318 = RLcp18_316+RLcp18_318+q[3];
    OMcp18_118 = OMcp18_117+ROcp18_717*qd[18];
    OMcp18_218 = OMcp18_217+ROcp18_817*qd[18];
    OMcp18_318 = OMcp18_317+ROcp18_917*qd[18];
    ORcp18_118 = OMcp18_217*RLcp18_318-OMcp18_317*RLcp18_218;
    ORcp18_218 = -(OMcp18_117*RLcp18_318-OMcp18_317*RLcp18_118);
    ORcp18_318 = OMcp18_117*RLcp18_218-OMcp18_217*RLcp18_118;
    VIcp18_118 = ORcp18_116+ORcp18_118+qd[1];
    VIcp18_218 = ORcp18_216+ORcp18_218+qd[2];
    VIcp18_318 = ORcp18_316+ORcp18_318+qd[3];
    ACcp18_118 = qdd[1]+OMcp18_217*ORcp18_318+OMcp18_26*ORcp18_316-OMcp18_317*ORcp18_218-OMcp18_36*ORcp18_216+OPcp18_217*
 RLcp18_318+OPcp18_26*RLcp18_316-OPcp18_317*RLcp18_218-OPcp18_36*RLcp18_216;
    ACcp18_218 = qdd[2]-OMcp18_117*ORcp18_318-OMcp18_16*ORcp18_316+OMcp18_317*ORcp18_118+OMcp18_36*ORcp18_116-OPcp18_117*
 RLcp18_318-OPcp18_16*RLcp18_316+OPcp18_317*RLcp18_118+OPcp18_36*RLcp18_116;
    ACcp18_318 = qdd[3]+OMcp18_117*ORcp18_218+OMcp18_16*ORcp18_216-OMcp18_217*ORcp18_118-OMcp18_26*ORcp18_116+OPcp18_117*
 RLcp18_218+OPcp18_16*RLcp18_216-OPcp18_217*RLcp18_118-OPcp18_26*RLcp18_116;
    OMcp18_119 = OMcp18_118+ROcp18_418*qd[19];
    OMcp18_219 = OMcp18_218+ROcp18_518*qd[19];
    OMcp18_319 = OMcp18_318+ROcp18_618*qd[19];
    OPcp18_119 = OPcp18_117+ROcp18_418*qdd[19]+ROcp18_717*qdd[18]+qd[18]*(OMcp18_217*ROcp18_917-OMcp18_317*ROcp18_817)+
 qd[19]*(OMcp18_218*ROcp18_618-OMcp18_318*ROcp18_518);
    OPcp18_219 = OPcp18_217+ROcp18_518*qdd[19]+ROcp18_817*qdd[18]-qd[18]*(OMcp18_117*ROcp18_917-OMcp18_317*ROcp18_717)-
 qd[19]*(OMcp18_118*ROcp18_618-OMcp18_318*ROcp18_418);
    OPcp18_319 = OPcp18_317+ROcp18_618*qdd[19]+ROcp18_917*qdd[18]+qd[18]*(OMcp18_117*ROcp18_817-OMcp18_217*ROcp18_717)+
 qd[19]*(OMcp18_118*ROcp18_518-OMcp18_218*ROcp18_418);

// = = Block_1_0_0_19_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp18_118;
    sens->P[2] = POcp18_218;
    sens->P[3] = POcp18_318;
    sens->R[1][1] = ROcp18_119;
    sens->R[1][2] = ROcp18_219;
    sens->R[1][3] = ROcp18_319;
    sens->R[2][1] = ROcp18_418;
    sens->R[2][2] = ROcp18_518;
    sens->R[2][3] = ROcp18_618;
    sens->R[3][1] = ROcp18_719;
    sens->R[3][2] = ROcp18_819;
    sens->R[3][3] = ROcp18_919;
    sens->V[1] = VIcp18_118;
    sens->V[2] = VIcp18_218;
    sens->V[3] = VIcp18_318;
    sens->OM[1] = OMcp18_119;
    sens->OM[2] = OMcp18_219;
    sens->OM[3] = OMcp18_319;
    sens->A[1] = ACcp18_118;
    sens->A[2] = ACcp18_218;
    sens->A[3] = ACcp18_318;
    sens->OMP[1] = OPcp18_119;
    sens->OMP[2] = OPcp18_219;
    sens->OMP[3] = OPcp18_319;
 
// 
break;
case 20:
 


// = = Block_1_0_0_20_0_1 = = 
 
// Sensor Kinematics 


    ROcp19_15 = C4*C5;
    ROcp19_25 = S4*C5;
    ROcp19_75 = C4*S5;
    ROcp19_85 = S4*S5;
    ROcp19_46 = ROcp19_75*S6-S4*C6;
    ROcp19_56 = ROcp19_85*S6+C4*C6;
    ROcp19_66 = C5*S6;
    ROcp19_76 = ROcp19_75*C6+S4*S6;
    ROcp19_86 = ROcp19_85*C6-C4*S6;
    ROcp19_96 = C5*C6;
    OMcp19_15 = -qd[5]*S4;
    OMcp19_25 = qd[5]*C4;
    OMcp19_16 = OMcp19_15+ROcp19_15*qd[6];
    OMcp19_26 = OMcp19_25+ROcp19_25*qd[6];
    OMcp19_36 = qd[4]-qd[6]*S5;
    OPcp19_16 = ROcp19_15*qdd[6]-qdd[5]*S4-qd[4]*qd[5]*C4-qd[6]*(OMcp19_25*S5+ROcp19_25*qd[4]);
    OPcp19_26 = ROcp19_25*qdd[6]+qdd[5]*C4-qd[4]*qd[5]*S4+qd[6]*(OMcp19_15*S5+ROcp19_15*qd[4]);
    OPcp19_36 = qdd[4]-qdd[6]*S5-qd[5]*qd[6]*C5;

// = = Block_1_0_0_20_0_6 = = 
 
// Sensor Kinematics 


    ROcp19_116 = ROcp19_15*C16-ROcp19_76*S16;
    ROcp19_216 = ROcp19_25*C16-ROcp19_86*S16;
    ROcp19_316 = -(ROcp19_96*S16+C16*S5);
    ROcp19_716 = ROcp19_15*S16+ROcp19_76*C16;
    ROcp19_816 = ROcp19_25*S16+ROcp19_86*C16;
    ROcp19_916 = ROcp19_96*C16-S16*S5;
    ROcp19_417 = ROcp19_46*C17+ROcp19_716*S17;
    ROcp19_517 = ROcp19_56*C17+ROcp19_816*S17;
    ROcp19_617 = ROcp19_66*C17+ROcp19_916*S17;
    ROcp19_717 = -(ROcp19_46*S17-ROcp19_716*C17);
    ROcp19_817 = -(ROcp19_56*S17-ROcp19_816*C17);
    ROcp19_917 = -(ROcp19_66*S17-ROcp19_916*C17);
    ROcp19_118 = ROcp19_116*C18+ROcp19_417*S18;
    ROcp19_218 = ROcp19_216*C18+ROcp19_517*S18;
    ROcp19_318 = ROcp19_316*C18+ROcp19_617*S18;
    ROcp19_418 = -(ROcp19_116*S18-ROcp19_417*C18);
    ROcp19_518 = -(ROcp19_216*S18-ROcp19_517*C18);
    ROcp19_618 = -(ROcp19_316*S18-ROcp19_617*C18);
    ROcp19_119 = ROcp19_118*C19-ROcp19_717*S19;
    ROcp19_219 = ROcp19_218*C19-ROcp19_817*S19;
    ROcp19_319 = ROcp19_318*C19-ROcp19_917*S19;
    ROcp19_719 = ROcp19_118*S19+ROcp19_717*C19;
    ROcp19_819 = ROcp19_218*S19+ROcp19_817*C19;
    ROcp19_919 = ROcp19_318*S19+ROcp19_917*C19;
    ROcp19_120 = ROcp19_119*C20+ROcp19_418*S20;
    ROcp19_220 = ROcp19_219*C20+ROcp19_518*S20;
    ROcp19_320 = ROcp19_319*C20+ROcp19_618*S20;
    ROcp19_420 = -(ROcp19_119*S20-ROcp19_418*C20);
    ROcp19_520 = -(ROcp19_219*S20-ROcp19_518*C20);
    ROcp19_620 = -(ROcp19_319*S20-ROcp19_618*C20);
    RLcp19_116 = ROcp19_46*s->dpt[2][6]+ROcp19_76*s->dpt[3][6];
    RLcp19_216 = ROcp19_56*s->dpt[2][6]+ROcp19_86*s->dpt[3][6];
    RLcp19_316 = ROcp19_66*s->dpt[2][6]+ROcp19_96*s->dpt[3][6];
    OMcp19_116 = OMcp19_16+ROcp19_46*qd[16];
    OMcp19_216 = OMcp19_26+ROcp19_56*qd[16];
    OMcp19_316 = OMcp19_36+ROcp19_66*qd[16];
    ORcp19_116 = OMcp19_26*RLcp19_316-OMcp19_36*RLcp19_216;
    ORcp19_216 = -(OMcp19_16*RLcp19_316-OMcp19_36*RLcp19_116);
    ORcp19_316 = OMcp19_16*RLcp19_216-OMcp19_26*RLcp19_116;
    OMcp19_117 = OMcp19_116+ROcp19_116*qd[17];
    OMcp19_217 = OMcp19_216+ROcp19_216*qd[17];
    OMcp19_317 = OMcp19_316+ROcp19_316*qd[17];
    OPcp19_117 = OPcp19_16+ROcp19_116*qdd[17]+ROcp19_46*qdd[16]+qd[16]*(OMcp19_26*ROcp19_66-OMcp19_36*ROcp19_56)+qd[17]*(
 OMcp19_216*ROcp19_316-OMcp19_316*ROcp19_216);
    OPcp19_217 = OPcp19_26+ROcp19_216*qdd[17]+ROcp19_56*qdd[16]-qd[16]*(OMcp19_16*ROcp19_66-OMcp19_36*ROcp19_46)-qd[17]*(
 OMcp19_116*ROcp19_316-OMcp19_316*ROcp19_116);
    OPcp19_317 = OPcp19_36+ROcp19_316*qdd[17]+ROcp19_66*qdd[16]+qd[16]*(OMcp19_16*ROcp19_56-OMcp19_26*ROcp19_46)+qd[17]*(
 OMcp19_116*ROcp19_216-OMcp19_216*ROcp19_116);
    RLcp19_118 = ROcp19_717*s->dpt[3][19];
    RLcp19_218 = ROcp19_817*s->dpt[3][19];
    RLcp19_318 = ROcp19_917*s->dpt[3][19];
    OMcp19_118 = OMcp19_117+ROcp19_717*qd[18];
    OMcp19_218 = OMcp19_217+ROcp19_817*qd[18];
    OMcp19_318 = OMcp19_317+ROcp19_917*qd[18];
    ORcp19_118 = OMcp19_217*RLcp19_318-OMcp19_317*RLcp19_218;
    ORcp19_218 = -(OMcp19_117*RLcp19_318-OMcp19_317*RLcp19_118);
    ORcp19_318 = OMcp19_117*RLcp19_218-OMcp19_217*RLcp19_118;
    OMcp19_119 = OMcp19_118+ROcp19_418*qd[19];
    OMcp19_219 = OMcp19_218+ROcp19_518*qd[19];
    OMcp19_319 = OMcp19_318+ROcp19_618*qd[19];
    OPcp19_119 = OPcp19_117+ROcp19_418*qdd[19]+ROcp19_717*qdd[18]+qd[18]*(OMcp19_217*ROcp19_917-OMcp19_317*ROcp19_817)+
 qd[19]*(OMcp19_218*ROcp19_618-OMcp19_318*ROcp19_518);
    OPcp19_219 = OPcp19_217+ROcp19_518*qdd[19]+ROcp19_817*qdd[18]-qd[18]*(OMcp19_117*ROcp19_917-OMcp19_317*ROcp19_717)-
 qd[19]*(OMcp19_118*ROcp19_618-OMcp19_318*ROcp19_418);
    OPcp19_319 = OPcp19_317+ROcp19_618*qdd[19]+ROcp19_917*qdd[18]+qd[18]*(OMcp19_117*ROcp19_817-OMcp19_217*ROcp19_717)+
 qd[19]*(OMcp19_118*ROcp19_518-OMcp19_218*ROcp19_418);
    RLcp19_120 = ROcp19_719*s->dpt[3][22];
    RLcp19_220 = ROcp19_819*s->dpt[3][22];
    RLcp19_320 = ROcp19_919*s->dpt[3][22];
    POcp19_120 = RLcp19_116+RLcp19_118+RLcp19_120+q[1];
    POcp19_220 = RLcp19_216+RLcp19_218+RLcp19_220+q[2];
    POcp19_320 = RLcp19_316+RLcp19_318+RLcp19_320+q[3];
    OMcp19_120 = OMcp19_119+ROcp19_719*qd[20];
    OMcp19_220 = OMcp19_219+ROcp19_819*qd[20];
    OMcp19_320 = OMcp19_319+ROcp19_919*qd[20];
    ORcp19_120 = OMcp19_219*RLcp19_320-OMcp19_319*RLcp19_220;
    ORcp19_220 = -(OMcp19_119*RLcp19_320-OMcp19_319*RLcp19_120);
    ORcp19_320 = OMcp19_119*RLcp19_220-OMcp19_219*RLcp19_120;
    VIcp19_120 = ORcp19_116+ORcp19_118+ORcp19_120+qd[1];
    VIcp19_220 = ORcp19_216+ORcp19_218+ORcp19_220+qd[2];
    VIcp19_320 = ORcp19_316+ORcp19_318+ORcp19_320+qd[3];
    OPcp19_120 = OPcp19_119+ROcp19_719*qdd[20]+qd[20]*(OMcp19_219*ROcp19_919-OMcp19_319*ROcp19_819);
    OPcp19_220 = OPcp19_219+ROcp19_819*qdd[20]-qd[20]*(OMcp19_119*ROcp19_919-OMcp19_319*ROcp19_719);
    OPcp19_320 = OPcp19_319+ROcp19_919*qdd[20]+qd[20]*(OMcp19_119*ROcp19_819-OMcp19_219*ROcp19_719);
    ACcp19_120 = qdd[1]+OMcp19_217*ORcp19_318+OMcp19_219*ORcp19_320+OMcp19_26*ORcp19_316-OMcp19_317*ORcp19_218-OMcp19_319*
 ORcp19_220-OMcp19_36*ORcp19_216+OPcp19_217*RLcp19_318+OPcp19_219*RLcp19_320+OPcp19_26*RLcp19_316-OPcp19_317*RLcp19_218-
 OPcp19_319*RLcp19_220-OPcp19_36*RLcp19_216;
    ACcp19_220 = qdd[2]-OMcp19_117*ORcp19_318-OMcp19_119*ORcp19_320-OMcp19_16*ORcp19_316+OMcp19_317*ORcp19_118+OMcp19_319*
 ORcp19_120+OMcp19_36*ORcp19_116-OPcp19_117*RLcp19_318-OPcp19_119*RLcp19_320-OPcp19_16*RLcp19_316+OPcp19_317*RLcp19_118+
 OPcp19_319*RLcp19_120+OPcp19_36*RLcp19_116;
    ACcp19_320 = qdd[3]+OMcp19_117*ORcp19_218+OMcp19_119*ORcp19_220+OMcp19_16*ORcp19_216-OMcp19_217*ORcp19_118-OMcp19_219*
 ORcp19_120-OMcp19_26*ORcp19_116+OPcp19_117*RLcp19_218+OPcp19_119*RLcp19_220+OPcp19_16*RLcp19_216-OPcp19_217*RLcp19_118-
 OPcp19_219*RLcp19_120-OPcp19_26*RLcp19_116;

// = = Block_1_0_0_20_1_0 = = 
 
// Symbolic Outputs  

    sens->P[1] = POcp19_120;
    sens->P[2] = POcp19_220;
    sens->P[3] = POcp19_320;
    sens->R[1][1] = ROcp19_120;
    sens->R[1][2] = ROcp19_220;
    sens->R[1][3] = ROcp19_320;
    sens->R[2][1] = ROcp19_420;
    sens->R[2][2] = ROcp19_520;
    sens->R[2][3] = ROcp19_620;
    sens->R[3][1] = ROcp19_719;
    sens->R[3][2] = ROcp19_819;
    sens->R[3][3] = ROcp19_919;
    sens->V[1] = VIcp19_120;
    sens->V[2] = VIcp19_220;
    sens->V[3] = VIcp19_320;
    sens->OM[1] = OMcp19_120;
    sens->OM[2] = OMcp19_220;
    sens->OM[3] = OMcp19_320;
    sens->A[1] = ACcp19_120;
    sens->A[2] = ACcp19_220;
    sens->A[3] = ACcp19_320;
    sens->OMP[1] = OPcp19_120;
    sens->OMP[2] = OPcp19_220;
    sens->OMP[3] = OPcp19_320;

break;
default:
break;
}


// ====== END Task 1 ====== 


}
 

