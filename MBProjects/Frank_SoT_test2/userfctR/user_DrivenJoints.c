//---------------------------
// UCL-CEREM-MBS
//
// @version MBsysLab_s 1.7.a
//
// Creation : 2006
// Last update : 01/10/2008
//---------------------------

#define _USE_MATH_DEFINES  // to use M_PI in Windows
#include "math.h"

#include "mbs_data.h"

#include "user_all_id.h"
#include "user_IO.h"

void user_DrivenJoints(MbsData *mbs_data,double tsim)
{
	// impose the q in right arm
   	mbs_data->q[Joint_9_id]  = mbs_data->user_IO->q_SoT[1];
   	mbs_data->q[Joint_10_id] = mbs_data->user_IO->q_SoT[2];
   	mbs_data->q[Joint_12_id] = mbs_data->user_IO->q_SoT[3];
   	mbs_data->q[Joint_7_id]  = mbs_data->user_IO->q_SoT[4];
   	mbs_data->q[Joint_11_id] = mbs_data->user_IO->q_SoT[5];
   
   	// impose q-nek and  q-head
   	mbs_data->q[Joint_21_id] = mbs_data->user_IO->q_SoT[6]; 
   	mbs_data->q[Joint_22_id] = mbs_data->user_IO->q_SoT[7];
   
   	// impose the q in left arm 
   	mbs_data->q[Joint_17_id] = mbs_data->user_IO->q_SoT[8];
   	mbs_data->q[Joint_18_id] = mbs_data->user_IO->q_SoT[9]; 
   	mbs_data->q[Joint_14_id] = mbs_data->user_IO->q_SoT[10];
   	mbs_data->q[Joint_8_id]  = mbs_data->user_IO->q_SoT[11];
   	mbs_data->q[Joint_6_id] = mbs_data->user_IO->q_SoT[12];
    
    // inpose pitch
    mbs_data->q[Joint_4_id] = mbs_data->user_IO->pitch_des;

}

 
