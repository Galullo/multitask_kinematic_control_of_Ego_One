% Frank LQR only base

%% Parameter
% Physical Constant
g = 9.81;						% gravity acceleration [m/sec^2]

% frank-2.0 Parameters
m = 1.6;						% wheel weight [kg]                                                     ok
R = 0.13;						% wheel radius [m]                                                      ok
Jw = 0.0057;                    % wheel inertia moment [kgm^2]                                          ok
M = 17.9;						% body weight [kg]  2.4;                                                ok
W = 0.496;						% body width [m]                                                        ok
L = 0.087 + 0.292;				% distance of the center of mass from the wheel axle [m]  0.1335        ok
Jpsi = 1.58;                    % body pitch inertia moment [kgm^2] 0.0377                              ok
Jphi = 0.39;                    % body yaw inertia moment [kgm^2]                                       ok
fm = 0.00059249;				% friction coefficient between body & DC motor                          ok
fw = 0;							% friction coefficient between wheel & floor                            NO

% DC Motor Parameters			
Jm = 0.00010667;				% DC motor inertia moment [kgm^2]                                       ok
Rm = 0.84278;					% DC motor resistance [Ohm]                                              ok
Kb = 0.048365;                  % DC motor back EMF constant [Vsec/rad]                                 ok
Kt = 0.015056;					% DC motor torque constant [Nm/A]                                       ok
n = 160;						% gear ratio                                                            OK



%% define system (wheel and pendulum - input i_r i_l)

E_11    = (2 * m + M) * R^2 + 2 * Jw + 2 * n^2 * Jm;
E_12    = M * L * R - 2 * n^2 * Jm;
E_21    = E_12;
E_22    = M * L^2 + Jpsi + 2 * n^2 * Jm;

E       = [E_11, E_12; E_21, E_22];             % Inertia matrix

F       = 2*[(fm + fw), -fm; -fm, fm];          % Corialis matrix

G       = [0 0; 0 -M * g * L];                  % Gravity matrix

H       = [n * Kt, n * Kt; -n * Kt, -n * Kt];   % Input matrix

%% state matrix

% sys1 (wheel and pendulum - input i_r i_l)
A1_32   = -g * M * L * E(1,2) / det(E);
A1_42   = g * M * L * E(1,1) / det(E);
A1_33   = -2 * ((fm + fw) * E(2,2) + fm * E(1,2)) / det(E);
A1_43   = 2 * ((fm + fw) * E(1,2) + fm * E(1,1)) / det(E);
A1_34   = 2 * fm * (E(2,2) + E(1,2)) / det(E);
A1_44   = -2 * fm * (E(1,1) + E(1,2)) / det(E);
B1_3    = (n * Kt) * (E(2,2) + E(1,2)) / det(E);
B1_4    = -(n * Kt) * (E(1,1) + E(1,2)) / det(E);

% define sys1
A1 = [0 0 1 0; 0 0 0 1; 0 A1_32 A1_33 A1_34; 0 A1_42 A1_43 A1_44];
B1 = [0 0; 0 0; B1_3 B1_3; B1_4 B1_4];
C1 = eye(4);
D1 = zeros(4, 2);


% sys2 (yaw - input i_r i_l)
I = m * W^2 / 2 + Jphi + (Jw + n^2 * Jm) * W^2 / (2 * R^2);
J = (fm + fw) * W^2 / (2 * R^2);
K = (n * Kt) * W / (2 * R);

% define sys2  
A2 = [0, 1; 0, -J/I];
B2 = [0, 0; -K/I, K/I];

C2 = eye(2);
D2 = zeros(2);


%% LQR design

A_BAR = [A1, zeros(4, 2); zeros(2, 4), A2];
B_BAR = [B1; B2];

% state th psi dth dpsi phi dphi
QQ = 1e0 * diag([1 7e6 1 1 1e3 1]);
RR = 0.15 * eye(2);
K_lqr = lqr(A_BAR, B_BAR, QQ, RR);



