% grafici per capire cosa succede

figure,
plot3(CoM_SoT(:,1),CoM_SoT(:,2),CoM_SoT(:,3),'k','LineWidth',2), grid on,
xlabel('x'), ylabel('y'), zlabel('z')
%xlim([-1.5 1.5]),
ylim([-0.5 0.5]),zlim([-0.5 1])
title('CoM w.r.t. Inertial Frame')

% task 1
Pose_sim = Pose_ee_right - Pose_shoul_right;

figure
hold on, grid on,
plot3(0, 0, 0, 's', 'MarkerSize', 10,'MarkerFaceColor',[0 1 0]),
plot3(position_arm.Data(1,:),position_arm.Data(2,:),position_arm.Data(3,:),'b','LineWidth', 2), 
plot3(position_arm.Data(1,1),position_arm.Data(2,1),position_arm.Data(3,1),'bo','MarkerSize', 9, 'LineWidth',2),
% plot3(position_arm.Data(3,:),-position_arm.Data(2,:),position_arm.Data(1,:),'b','LineWidth', 2), 
% plot3(position_arm.Data(3,1),-position_arm.Data(2,1),position_arm.Data(1,1),'bo','MarkerSize', 9, 'LineWidth',2),
plot3(Pose_sim(:,1), Pose_sim(:,2), Pose_sim(:,3),'r','LineWidth', 2),
plot3(Pose_sim(1,1), Pose_sim(1,2), Pose_sim(1,3),'ro','MarkerSize', 8, 'LineWidth',2),
grid on, legend('right shoulder','Desired','start', 'e-e','start')
xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]')
ylim([-0.5 0.5]),zlim([-0.5 0.3])
title('Task 1 : Trajectory for end-effector')
view([90, 90, 45])

%% plot the Joints's value
% figure
% plot(time, q_SoT(:,1), 'b', 'LineWidth', 2), hold on,
% plot(time, q_SoT(:,8),'r', 'LineWidth', 2), legend('right','left')
% title('Shulder R_y')
% xlabel('time [s]'), ylabel('q [rad]')
% 
% figure,
% plot(time, q_SoT(:,2),'b', 'LineWidth', 2), hold on,
% plot(time, q_SoT(:,9),'r', 'LineWidth', 2), legend('right','left')
% title('Arm R_x')
% xlabel('time [s]'), ylabel('q [rad]')
% 
% figure,
% plot(time, q_SoT(:,3),'b', 'LineWidth', 2), hold on,
% plot(time, q_SoT(:,10),'r', 'LineWidth', 2), legend('right','left')
% title('Elbow R_z')
% xlabel('time [s]'), ylabel('q [rad]')
% 
% figure,
% plot(time, q_SoT(:,4),'b', 'LineWidth', 2), hold on,
% plot(time, q_SoT(:,11),'r', 'LineWidth', 2), legend('right','left')
% title('Forarm R_y')
% xlabel('time [s]'), ylabel('q [rad]')
% 
% figure,
% plot(time, q_SoT(:,5),'b', 'LineWidth', 2), hold on,
% plot(time, q_SoT(:,12),'r', 'LineWidth', 2), legend('right','left')
% title('Hand R_z')
% xlabel('time [s]'), ylabel('q [rad]')
% 
% figure
% subplot(2,1,1)
% plot(time, q_SoT(:,6),'c', 'LineWidth', 2),
% xlabel('time [s]'), ylabel('q [rad]' )
% title('Nek')
% subplot(2,1,2)
% plot(time, q_SoT(:,7),'c', 'LineWidth', 2),
% xlabel('time [s]'), ylabel('q [rad]')
% title('Head')





