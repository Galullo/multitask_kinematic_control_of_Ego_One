function quat = rot2quat(R)
% this function compute the trasformation from rotation matrix to 
% quaternion (see Sciavicco formula 2.34)

tr = R(1,1) + R(2,2) + R(3,3);
% real part of unitary quaternion
eta = 0.5* sqrt(tr +1);

% immaginary components of unitary quaternion
eps1 = sign(R(3,2) - R(2,3))* sqrt(R(1,1) - R(2,2) - R(3,3) +1);
eps2 = sign(R(1,3) - R(3,1))* sqrt(R(2,2) - R(1,1) - R(3,3) +1);
eps3 = sign(R(2,1) - R(1,2))* sqrt(R(3,3) - R(1,1) - R(2,2) +1);

eps = 0.5*[eps1; eps2; eps3];

quat= [eta; eps];
end