%
%-------------------------------------------------------------
%
%	ROBOTRAN - Version 6.6 (build : february 22, 2008)
%
%	Copyright 
%	Universite catholique de Louvain 
%	Departement de Mecanique 
%	Unite de Production Mecanique et Machines 
%	2, Place du Levant 
%	1348 Louvain-la-Neuve 
%	http://www.robotran.be// 
%
%	==> Generation Date : Sun Oct 29 15:16:01 2017
%
%	==> Project name : Frank_SoT_test2
%	==> using XML input file 
%
%	==> Number of joints : 20
%
%	==> Function : F19 : External Forces
%	==> Flops complexity : 362
%
%	==> Generation Time :  0.010 seconds
%	==> Post-Processing :  0.010 seconds
%
%-------------------------------------------------------------
%
function [frc,trq] = extforces(s,tsim,usrfun)

 frc = zeros(3,20);
 trq = zeros(3,20);

q = s.q; 
qd = s.qd; 
qdd = s.qdd; 
frc = s.frc; 
trq = s.trq; 

% === begin imp_aux === 

% === end imp_aux === 

% ===== BEGIN task 0 ===== 
 
% Sensor Kinematics 



% = = Block_0_0_0_0_0_1 = = 
 
% Trigonometric Variables  

  C4 = cos(q(4));
  S4 = sin(q(4));
  C5 = cos(q(5));
  S5 = sin(q(5));
  C6 = cos(q(6));
  S6 = sin(q(6));

% = = Block_0_0_0_0_0_2 = = 
 
% Trigonometric Variables  

  C7 = cos(q(7));
  S7 = sin(q(7));

% = = Block_0_0_0_0_0_3 = = 
 
% Trigonometric Variables  

  C8 = cos(q(8));
  S8 = sin(q(8));

% = = Block_0_0_1_1_0_1 = = 
 
% Sensor Kinematics 


  ROcp14_15 = C4*C5;
  ROcp14_25 = S4*C5;
  ROcp14_75 = C4*S5;
  ROcp14_85 = S4*S5;
  ROcp14_46 = ROcp14_75*S6-S4*C6;
  ROcp14_56 = ROcp14_85*S6+C4*C6;
  ROcp14_66 = C5*S6;
  ROcp14_76 = ROcp14_75*C6+S4*S6;
  ROcp14_86 = ROcp14_85*C6-C4*S6;
  ROcp14_96 = C5*C6;
  OMcp14_15 = -qd(5)*S4;
  OMcp14_25 = qd(5)*C4;
  OMcp14_16 = OMcp14_15+qd(6)*ROcp14_15;
  OMcp14_26 = OMcp14_25+qd(6)*ROcp14_25;
  OMcp14_36 = qd(4)-qd(6)*S5;
  OPcp14_16 = -(qd(4)*qd(5)*C4+qd(6)*(qd(4)*ROcp14_25+OMcp14_25*S5)+qdd(5)*S4-qdd(6)*ROcp14_15);
  OPcp14_26 = -(qd(4)*qd(5)*S4-qd(6)*(qd(4)*ROcp14_15+OMcp14_15*S5)-qdd(5)*C4-qdd(6)*ROcp14_25);
  OPcp14_36 = qdd(4)-qd(5)*qd(6)*C5-qdd(6)*S5;

% = = Block_0_0_1_1_0_2 = = 
 
% Sensor Kinematics 


  ROcp14_17 = ROcp14_15*C7-ROcp14_76*S7;
  ROcp14_27 = ROcp14_25*C7-ROcp14_86*S7;
  ROcp14_37 = -(ROcp14_96*S7+S5*C7);
  ROcp14_77 = ROcp14_15*S7+ROcp14_76*C7;
  ROcp14_87 = ROcp14_25*S7+ROcp14_86*C7;
  ROcp14_97 = ROcp14_96*C7-S5*S7;
  RLcp14_17 = ROcp14_46*s.dpt(2,2);
  RLcp14_27 = ROcp14_56*s.dpt(2,2);
  RLcp14_37 = ROcp14_66*s.dpt(2,2);
  ORcp14_17 = OMcp14_26*RLcp14_37-OMcp14_36*RLcp14_27;
  ORcp14_27 = -(OMcp14_16*RLcp14_37-OMcp14_36*RLcp14_17);
  ORcp14_37 = OMcp14_16*RLcp14_27-OMcp14_26*RLcp14_17;
  PxF1(1) = q(1)+RLcp14_17;
  PxF1(2) = q(2)+RLcp14_27;
  PxF1(3) = q(3)+RLcp14_37;
  RxF1(1,1) = ROcp14_17;
  RxF1(1,2) = ROcp14_27;
  RxF1(1,3) = ROcp14_37;
  RxF1(2,1) = ROcp14_46;
  RxF1(2,2) = ROcp14_56;
  RxF1(2,3) = ROcp14_66;
  RxF1(3,1) = ROcp14_77;
  RxF1(3,2) = ROcp14_87;
  RxF1(3,3) = ROcp14_97;
  VxF1(1) = qd(1)+ORcp14_17;
  VxF1(2) = qd(2)+ORcp14_27;
  VxF1(3) = qd(3)+ORcp14_37;
  OMxF1(1) = OMcp14_16+qd(7)*ROcp14_46;
  OMxF1(2) = OMcp14_26+qd(7)*ROcp14_56;
  OMxF1(3) = OMcp14_36+qd(7)*ROcp14_66;
  AxF1(1) = qdd(1)+OMcp14_26*ORcp14_37-OMcp14_36*ORcp14_27+OPcp14_26*RLcp14_37-OPcp14_36*RLcp14_27;
  AxF1(2) = qdd(2)-OMcp14_16*ORcp14_37+OMcp14_36*ORcp14_17-OPcp14_16*RLcp14_37+OPcp14_36*RLcp14_17;
  AxF1(3) = qdd(3)+OMcp14_16*ORcp14_27-OMcp14_26*ORcp14_17+OPcp14_16*RLcp14_27-OPcp14_26*RLcp14_17;
  OMPxF1(1) = OPcp14_16+qd(7)*(OMcp14_26*ROcp14_66-OMcp14_36*ROcp14_56)+qdd(7)*ROcp14_46;
  OMPxF1(2) = OPcp14_26-qd(7)*(OMcp14_16*ROcp14_66-OMcp14_36*ROcp14_46)+qdd(7)*ROcp14_56;
  OMPxF1(3) = OPcp14_36+qd(7)*(OMcp14_16*ROcp14_56-OMcp14_26*ROcp14_46)+qdd(7)*ROcp14_66;
 
% Sensor Forces Computation 

  SWr1 = usrfun.fext(PxF1,RxF1,VxF1,OMxF1,AxF1,OMPxF1,s,tsim,1);
 
% Sensor Dynamics : Forces projection on body-fixed frames 

  xfrc115 = ROcp14_17*SWr1(1)+ROcp14_27*SWr1(2)+ROcp14_37*SWr1(3);
  xfrc215 = ROcp14_46*SWr1(1)+ROcp14_56*SWr1(2)+ROcp14_66*SWr1(3);
  xfrc315 = ROcp14_77*SWr1(1)+ROcp14_87*SWr1(2)+ROcp14_97*SWr1(3);
  frc(1,7) = s.frc(1,7)+xfrc115;
  frc(2,7) = s.frc(2,7)+xfrc215;
  frc(3,7) = s.frc(3,7)+xfrc315;
  xtrq115 = ROcp14_17*SWr1(4)+ROcp14_27*SWr1(5)+ROcp14_37*SWr1(6);
  xtrq215 = ROcp14_46*SWr1(4)+ROcp14_56*SWr1(5)+ROcp14_66*SWr1(6);
  xtrq315 = ROcp14_77*SWr1(4)+ROcp14_87*SWr1(5)+ROcp14_97*SWr1(6);
  trq(1,7) = s.trq(1,7)+xtrq115-xfrc215*SWr1(9)+xfrc315*SWr1(8);
  trq(2,7) = s.trq(2,7)+xtrq215+xfrc115*SWr1(9)-xfrc315*SWr1(7);
  trq(3,7) = s.trq(3,7)+xtrq315-xfrc115*SWr1(8)+xfrc215*SWr1(7);

% = = Block_0_0_1_2_0_1 = = 
 
% Sensor Kinematics 


  ROcp15_15 = C4*C5;
  ROcp15_25 = S4*C5;
  ROcp15_75 = C4*S5;
  ROcp15_85 = S4*S5;
  ROcp15_46 = ROcp15_75*S6-S4*C6;
  ROcp15_56 = ROcp15_85*S6+C4*C6;
  ROcp15_66 = C5*S6;
  ROcp15_76 = ROcp15_75*C6+S4*S6;
  ROcp15_86 = ROcp15_85*C6-C4*S6;
  ROcp15_96 = C5*C6;
  OMcp15_15 = -qd(5)*S4;
  OMcp15_25 = qd(5)*C4;
  OMcp15_16 = OMcp15_15+qd(6)*ROcp15_15;
  OMcp15_26 = OMcp15_25+qd(6)*ROcp15_25;
  OMcp15_36 = qd(4)-qd(6)*S5;
  OPcp15_16 = -(qd(4)*qd(5)*C4+qd(6)*(qd(4)*ROcp15_25+OMcp15_25*S5)+qdd(5)*S4-qdd(6)*ROcp15_15);
  OPcp15_26 = -(qd(4)*qd(5)*S4-qd(6)*(qd(4)*ROcp15_15+OMcp15_15*S5)-qdd(5)*C4-qdd(6)*ROcp15_25);
  OPcp15_36 = qdd(4)-qd(5)*qd(6)*C5-qdd(6)*S5;

% = = Block_0_0_1_2_0_3 = = 
 
% Sensor Kinematics 


  ROcp15_18 = ROcp15_15*C8-ROcp15_76*S8;
  ROcp15_28 = ROcp15_25*C8-ROcp15_86*S8;
  ROcp15_38 = -(ROcp15_96*S8+S5*C8);
  ROcp15_78 = ROcp15_15*S8+ROcp15_76*C8;
  ROcp15_88 = ROcp15_25*S8+ROcp15_86*C8;
  ROcp15_98 = ROcp15_96*C8-S5*S8;
  RLcp15_18 = ROcp15_46*s.dpt(2,3);
  RLcp15_28 = ROcp15_56*s.dpt(2,3);
  RLcp15_38 = ROcp15_66*s.dpt(2,3);
  ORcp15_18 = OMcp15_26*RLcp15_38-OMcp15_36*RLcp15_28;
  ORcp15_28 = -(OMcp15_16*RLcp15_38-OMcp15_36*RLcp15_18);
  ORcp15_38 = OMcp15_16*RLcp15_28-OMcp15_26*RLcp15_18;
  PxF2(1) = q(1)+RLcp15_18;
  PxF2(2) = q(2)+RLcp15_28;
  PxF2(3) = q(3)+RLcp15_38;
  RxF2(1,1) = ROcp15_18;
  RxF2(1,2) = ROcp15_28;
  RxF2(1,3) = ROcp15_38;
  RxF2(2,1) = ROcp15_46;
  RxF2(2,2) = ROcp15_56;
  RxF2(2,3) = ROcp15_66;
  RxF2(3,1) = ROcp15_78;
  RxF2(3,2) = ROcp15_88;
  RxF2(3,3) = ROcp15_98;
  VxF2(1) = qd(1)+ORcp15_18;
  VxF2(2) = qd(2)+ORcp15_28;
  VxF2(3) = qd(3)+ORcp15_38;
  OMxF2(1) = OMcp15_16+qd(8)*ROcp15_46;
  OMxF2(2) = OMcp15_26+qd(8)*ROcp15_56;
  OMxF2(3) = OMcp15_36+qd(8)*ROcp15_66;
  AxF2(1) = qdd(1)+OMcp15_26*ORcp15_38-OMcp15_36*ORcp15_28+OPcp15_26*RLcp15_38-OPcp15_36*RLcp15_28;
  AxF2(2) = qdd(2)-OMcp15_16*ORcp15_38+OMcp15_36*ORcp15_18-OPcp15_16*RLcp15_38+OPcp15_36*RLcp15_18;
  AxF2(3) = qdd(3)+OMcp15_16*ORcp15_28-OMcp15_26*ORcp15_18+OPcp15_16*RLcp15_28-OPcp15_26*RLcp15_18;
  OMPxF2(1) = OPcp15_16+qd(8)*(OMcp15_26*ROcp15_66-OMcp15_36*ROcp15_56)+qdd(8)*ROcp15_46;
  OMPxF2(2) = OPcp15_26-qd(8)*(OMcp15_16*ROcp15_66-OMcp15_36*ROcp15_46)+qdd(8)*ROcp15_56;
  OMPxF2(3) = OPcp15_36+qd(8)*(OMcp15_16*ROcp15_56-OMcp15_26*ROcp15_46)+qdd(8)*ROcp15_66;
 
% Sensor Forces Computation 

  SWr2 = usrfun.fext(PxF2,RxF2,VxF2,OMxF2,AxF2,OMPxF2,s,tsim,2);
 
% Sensor Dynamics : Forces projection on body-fixed frames 

  xfrc116 = ROcp15_18*SWr2(1)+ROcp15_28*SWr2(2)+ROcp15_38*SWr2(3);
  xfrc216 = ROcp15_46*SWr2(1)+ROcp15_56*SWr2(2)+ROcp15_66*SWr2(3);
  xfrc316 = ROcp15_78*SWr2(1)+ROcp15_88*SWr2(2)+ROcp15_98*SWr2(3);
  frc(1,8) = s.frc(1,8)+xfrc116;
  frc(2,8) = s.frc(2,8)+xfrc216;
  frc(3,8) = s.frc(3,8)+xfrc316;
  xtrq116 = ROcp15_18*SWr2(4)+ROcp15_28*SWr2(5)+ROcp15_38*SWr2(6);
  xtrq216 = ROcp15_46*SWr2(4)+ROcp15_56*SWr2(5)+ROcp15_66*SWr2(6);
  xtrq316 = ROcp15_78*SWr2(4)+ROcp15_88*SWr2(5)+ROcp15_98*SWr2(6);
  trq(1,8) = s.trq(1,8)+xtrq116-xfrc216*SWr2(9)+xfrc316*SWr2(8);
  trq(2,8) = s.trq(2,8)+xtrq216+xfrc116*SWr2(9)-xfrc316*SWr2(7);
  trq(3,8) = s.trq(3,8)+xtrq316-xfrc116*SWr2(8)+xfrc216*SWr2(7);

% = = Block_0_0_1_2_1_0 = = 
 
% Symbolic Outputs  

  frc(1,6) = s.frc(1,6);
  frc(2,6) = s.frc(2,6);
  frc(3,6) = s.frc(3,6);
  frc(1,9) = s.frc(1,9);
  frc(2,9) = s.frc(2,9);
  frc(3,9) = s.frc(3,9);
  frc(1,10) = s.frc(1,10);
  frc(2,10) = s.frc(2,10);
  frc(3,10) = s.frc(3,10);
  frc(1,11) = s.frc(1,11);
  frc(2,11) = s.frc(2,11);
  frc(3,11) = s.frc(3,11);
  frc(1,12) = s.frc(1,12);
  frc(2,12) = s.frc(2,12);
  frc(3,12) = s.frc(3,12);
  frc(1,13) = s.frc(1,13);
  frc(2,13) = s.frc(2,13);
  frc(3,13) = s.frc(3,13);
  frc(1,14) = s.frc(1,14);
  frc(2,14) = s.frc(2,14);
  frc(3,14) = s.frc(3,14);
  frc(1,15) = s.frc(1,15);
  frc(2,15) = s.frc(2,15);
  frc(3,15) = s.frc(3,15);
  frc(1,16) = s.frc(1,16);
  frc(2,16) = s.frc(2,16);
  frc(3,16) = s.frc(3,16);
  frc(1,17) = s.frc(1,17);
  frc(2,17) = s.frc(2,17);
  frc(3,17) = s.frc(3,17);
  frc(1,18) = s.frc(1,18);
  frc(2,18) = s.frc(2,18);
  frc(3,18) = s.frc(3,18);
  frc(1,19) = s.frc(1,19);
  frc(2,19) = s.frc(2,19);
  frc(3,19) = s.frc(3,19);
  frc(1,20) = s.frc(1,20);
  frc(2,20) = s.frc(2,20);
  frc(3,20) = s.frc(3,20);
  trq(1,6) = s.trq(1,6);
  trq(2,6) = s.trq(2,6);
  trq(3,6) = s.trq(3,6);
  trq(1,9) = s.trq(1,9);
  trq(2,9) = s.trq(2,9);
  trq(3,9) = s.trq(3,9);
  trq(1,10) = s.trq(1,10);
  trq(2,10) = s.trq(2,10);
  trq(3,10) = s.trq(3,10);
  trq(1,11) = s.trq(1,11);
  trq(2,11) = s.trq(2,11);
  trq(3,11) = s.trq(3,11);
  trq(1,12) = s.trq(1,12);
  trq(2,12) = s.trq(2,12);
  trq(3,12) = s.trq(3,12);
  trq(1,13) = s.trq(1,13);
  trq(2,13) = s.trq(2,13);
  trq(3,13) = s.trq(3,13);
  trq(1,14) = s.trq(1,14);
  trq(2,14) = s.trq(2,14);
  trq(3,14) = s.trq(3,14);
  trq(1,15) = s.trq(1,15);
  trq(2,15) = s.trq(2,15);
  trq(3,15) = s.trq(3,15);
  trq(1,16) = s.trq(1,16);
  trq(2,16) = s.trq(2,16);
  trq(3,16) = s.trq(3,16);
  trq(1,17) = s.trq(1,17);
  trq(2,17) = s.trq(2,17);
  trq(3,17) = s.trq(3,17);
  trq(1,18) = s.trq(1,18);
  trq(2,18) = s.trq(2,18);
  trq(3,18) = s.trq(3,18);
  trq(1,19) = s.trq(1,19);
  trq(2,19) = s.trq(2,19);
  trq(3,19) = s.trq(3,19);
  trq(1,20) = s.trq(1,20);
  trq(2,20) = s.trq(2,20);
  trq(3,20) = s.trq(3,20);

% ====== END Task 0 ====== 

  

