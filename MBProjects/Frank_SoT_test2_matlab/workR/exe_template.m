%--------------------------------------------------------------------------
%   Universite catholique de Louvain
%   CEREM : Centre for research in mechatronics
%   http://www.robotran.be  
%   Contact : robotran@prm.ucl.ac.be
%   Version : ROBOTRAN $Version$
%
%
%   MBsysLab main script template for simple model:
%   -----------------------------------------------
%    This template loads the data file *.mbs and execute the
%    direct dynamic modeuls (time integration of equations of
%    motion). It may be adapted and completed by the user.
%
%   Project : Frank_SoT_test2_matlab
%   Author : Team Robotran
%   Date : $Date$ 
%--------------------------------------------------------------------------

%% 1. Initialization and Project Loading [mbs_load]
%--------------------------------------------------------------------------
close all; clear variables; clc;                                            % Cleaning of the Matlab workspace
global MBS_user;                                                            % Declaration of the global user structure
MBS_user.process = '';                                                      % Initialisation of the user field "process"

% Project loading
prjname = 'Frank_SoT_test2_matlab';
[mbs_data, mbs_info] = mbs_load(prjname,'default');                         % Option 'default': automatic loading of "$project_name$.mbs" 
                                                                            % Have a look at the content of the mbs_data structure on www.robotran.be

%% 2. Direct dynamics [mbs_exe_dirdyn]
%--------------------------------------------------------------------------
MBS_user.process = 'dirdyn';

opt.dirdyn = {'time',0:0.01:5, 'odemethod','ode45'};                        % Help about options on www.robotran.be

[mbs_dirdyn,mbs_data] = mbs_exe_dirdyn(mbs_data,opt.dirdyn);                % Direct dynamics process (time simulation)

% Graphical Results
figure;
plot(mbs_dirdyn.tsim,mbs_dirdyn.q(:,1));                                    % Joint motion time history : joint n� 1 motion (example)


%% 3. Closing operations (optional)
%--------------------------------------------------------------------------
mbs_rm_allprjpath;                                                          % Cleaning of the Matlab project paths
mbs_del_glob('MBS_user','MBS_info','MBS_data');                             % Cleaning of the global MBS variables
clc;                                                                        % Cleaning of the Matlab command window
