% init_Frank
addpath('/home/parallels/.robotran/mbsysc/MBsysC/build/lib')

mbs_load('Frank_SoT_test2_whole_body');

prjname = 'Frank_SoT_test2_whole_body';

% selection task: 
%           - 0 all;
%           - 1 only task 1;
%           - 2 onliy task 2;
%           - otherwhise nothing;
task_chosen = 0;

% telecom chosen: 2 chosen default tasks, 1 chosen telecom signal;
telecom_chosen = 2;

%% Simulink parameter 

% I use this in controller function
rob_str.dpt = MBS_data.dpt;
rob_str.m = MBS_data.m;

% step of simulation
Ts = 0.02;

% compute the LQR gain
Frank_LQR_base;

% pitch desired for Frank [rad]
pitch_des = 0*0.0873;

% load trajectory 
load('timeseries_task_tot.mat')

% rotation matrix for Telecom operation between shoulder and world Robotran
% frame
if(telecom_chosen == 2)
    R_shoulder1 = eye(3);
elseif(telecom_chosen == 1)
    R_shoulder1 = [0, 0, 1; 0, -1, 0; 1, 0, 0];
end


% Task dimensions
dim1 = 6;
dim2 = 2;

% Feedback gains
% Gains Task 1
Kp = 10;
Ko = 5;

% Gain Task 2
Kc = 2.5;

% saturation on upper body joints
Upper_body_ub = [pi/2 pi/4 pi/2 pi/10 pi/2 inf inf pi/2 pi/2 pi/2 pi/10 pi/2];
Upper_body_lb = [-pi/2 -pi/2 -pi/2 -pi -pi/2 -inf -inf -pi/2 -pi/4 -pi/2 -pi -pi/2];

%% simulink
%open Frank_SoT.mdl
