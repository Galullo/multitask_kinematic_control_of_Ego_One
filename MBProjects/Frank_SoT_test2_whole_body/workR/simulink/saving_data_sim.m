% This file is used to save the data frmo simulation test
% Save the usefull data from simulation:
%
% - q                           : joints values from Robotran model;
% - q_dot                       : joints velocity from Robotran model;
% - q_dot _dot                  : joints acceleration from Robotran model;
% - q_dot_SoT_1                 : joint velocity from reverse priority task algorithm (task 1);
% - q_dot_SoT_2                 : joint velocity from reverse priority task algorithm (task 2);
% - q_SoT                       : joint values from reverse priority task algorithm;
% - CoM_SoT                     : CoM estimated by SoT algorithm;
% - CoM                         : CoM estimated in base controller w.r.t. world frame;
% - CoM_base                    : relative CoM estimated in base controller;
% - Psi                         : pitch angle etimated from relative CoM position;
% - Pose_shoul_right            : position of shoulder in world frame;
% - Pose_ee_right               : position of end-effector in world frame;
% - position_arm                : trajecotory desired in world frame;
% - pitch_des_from_ROS          : pitch ref in during test;
% - position_arm_from_ROS       : position arm during telecom;
% - orientation_arm_from_ROS    : orientation arm during telecom.

%% Saving data
% foldere where save data
folder = 'data_simulation_test/prova';

mkdir(folder)

% file.mat  name
name = 'usefull_data';

% all data
save([folder '/' name '.mat'], 'time','q','q_dot', 'q_dot_dot',...
    'q_dot_SoT_1','q_dot_SoT_2','q_SoT', 'CoM_SoT', 'CoM', 'CoM_base',...
    'Psi','Pose_ee_right', 'Pose_shoul_right','Pose_ee_right',...
    'position_arm','pitch_des_from_ROS', 'position_arm_from_ROS',...
    'orientation_arm_from_ROS')

% saving data from subsys selection q for inverse kineamtic algorithm
save([folder '/subsys_sel_q_inv_kin.mat'], 'q_SoT_subsys', 'dq_SoT_subsys', 'ddq_SoT_subsys')