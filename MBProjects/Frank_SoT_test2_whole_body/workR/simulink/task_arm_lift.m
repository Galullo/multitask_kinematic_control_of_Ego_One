
Tsim = 16;
t = 0:0.02:Tsim;


radius = 0.3360;
omega = 0.1;

n_sample = length(t);
% trajectory

% task 1
xt = radius*cos(omega*t - pi/2);
yt = 0*t;
zt = radius*sin(omega*t - pi/2);


% plot trajectory
figure,
hold on, grid on,
plot3(0, 0, 0, 's', 'MarkerSize', 10,'MarkerFaceColor',[0 1 0]),
plot3(xt, yt, zt,'r', 'LineWidth',2),
xlabel('x'), ylabel('y'), zlabel('z')
legend('right shoulder','trajectory')
title('Desired trajectory for end-effector')
xlim([-radius, radius]), ylim([-radius, radius]), zlim([-radius, radius])
view([90, 90, 45])

% orintation
quat_t = zeros(4, (length(t)));
 
 for i = 1:(length(t))
     Ry = [cos(omega*t(i)), 0, sin(omega*t(i)); 0, 1, 0; -sin(omega*t(i)), 0, cos(omega*t(i))];
     quat_t(:,i) = rot2quat(Ry);
 end
 
 % timeseries
 
 position_arm = timeseries([xt;yt;zt],t,'Name','position_arm');
 orientation_arm = timeseries(quat_t,t,'Name','orientation_arm');
 
 save('timeseries_task_tot', 'position_arm', 'orientation_arm')