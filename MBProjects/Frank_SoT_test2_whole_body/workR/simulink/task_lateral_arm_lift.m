
Tsim = 15;
t = 0:0.02:Tsim;


radius = 0.3360;
omega = 0.1;

% task 
xt = 0*t;
yt = radius*sin(omega*t - pi);
zt = radius*cos(omega*t - pi);

% plot trajectory

figure,
hold on, grid on,
plot3(0, 0, 0, 's', 'MarkerSize', 10,'MarkerFaceColor',[0 1 0]),
plot3(xt, yt, zt,'r', 'LineWidth',2),
xlabel('x [m]'), ylabel('y [m]'), zlabel('z [m]')
legend('right shoulder','trajectory')
title('Desired trajectory for end-effector')
xlim([-radius, radius]), ylim([-radius, radius]), zlim([-radius, radius])
view([80, 80, 90])
set(gca,'FontSize',10)


 
% orienattion deired
 quat_t = zeros(4, length(t));
 
 for i = 1:length(t)
     R_xt = [1 0 0; 0 cos(omega*t(i)) -sin(omega*t(i)); 0 sin(omega*t(i)) cos(omega*t(i))];
     quat_t(:,i) = rot2quat(R_xt);
 end
 
 
 % timeseries
 
 position_arm = timeseries([xt; yt; zt],t,'Name','position_arm');
 orientation_arm = timeseries(quat_t,t,'Name','orientation_arm');
 
 save('timeseries_task_tot', 'position_arm', 'orientation_arm')
